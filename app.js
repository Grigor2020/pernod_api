var createError = require('http-errors');
var express = require('express');
var cors = require('cors');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var authRouter = require('./routes/auth/auth');
var guestRouter = require('./routes/auth/guest');
var indexRouter = require('./routes/index');
var slideRouter = require('./routes/public/slide');
var translateRouter = require('./routes/public/translate');
var countryRouter = require('./routes/public/country');
var languageRouter = require('./routes/public/language');
var categoriesRouter = require('./routes/public/categories');
var productRouter = require('./routes/product/product');
var catalogRouter = require('./routes/catalog/catalog');
var brandRouter = require('./routes/brand/brand');
var categories_brandsRouter = require('./routes/public/categoriesBrands');
var cartRouter = require('./routes/cart/cart');
var favoriteRouter = require('./routes/favorite/favorite');
var checkoutRouter = require('./routes/checkout/checkout');
var ordersRouter = require('./routes/orders/orders');
var profileRouter = require('./routes/profile/profile');
var contactRouter = require('./routes/contact/contact');
var aboutRouter = require('./routes/about/about');
var historyRouter = require('./routes/history/history');
var deliveryRouter = require('./routes/delivery/delivery');
var newsRouter = require('./routes/news/news');
var settingsRouter = require('./routes/public/settings');
var servicePagesRouter = require('./routes/servicePages/servicePages');


var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(cors());



app.use(function (req, res, next) {
    if (req.headers['authorization'] == "484f55257f8bc1b213b7cae23a847831461418c1") {
        next();
    } else {
        res.json({error: true, msg: "Authorization wrong", data: null});
        res.end();
    }
});


app.use(function (req, res, next) {
    // var ip = req.connection.remoteAddress;
    // console.log('ip',ip)
    res.setHeader("Access-Control-Allow-Headers", "x-access-token,void,authorization,guest");
    //res.setHeader("Access-Control-Allow-Origin", "http://localhost:3000");
    //res.setHeader("Access-Control-Allow-Origin", "http://10.10.31.1");
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader("Access-Control-Allow-Credentials", true);
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT'); // OPTIONS, PATCH,
    res.setHeader('Cache-Control', 'no-cache'); // OPTIONS, PATCH,
    res.setHeader('X-Frame-Options', 'deny'); // OPTIONS, PATCH,
    res.removeHeader('Server');
    res.removeHeader('X-Powered-By');
    next()
})


app.use('/v1', indexRouter);
app.use('/v1/guest', guestRouter);
app.use('/v1/auth', authRouter);
app.use('/v1/language', languageRouter);
app.use('/v1/translate', translateRouter);
app.use('/v1/slide', slideRouter);
app.use('/v1/categories', categoriesRouter);
app.use('/v1/product', productRouter);
app.use('/v1/catalog', catalogRouter);
app.use('/v1/brand', brandRouter);
app.use('/v1/categories-brands', categories_brandsRouter);
app.use('/v1/cart', cartRouter);
app.use('/v1/favorite', favoriteRouter);
app.use('/v1/checkout', checkoutRouter);
app.use('/v1/orders', ordersRouter);
app.use('/v1/country', countryRouter);
app.use('/v1/profile', profileRouter);
app.use('/v1/contact', contactRouter);
app.use('/v1/about', aboutRouter);
app.use('/v1/history', historyRouter);
app.use('/v1/delivery', deliveryRouter);
app.use('/v1/news', newsRouter);
app.use('/v1/settings', settingsRouter);
app.use('/v1/service-pages', servicePagesRouter);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
    next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;
