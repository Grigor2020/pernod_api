let sha1 = require('sha1')
let profileQuery = require('../../model/dbQuerys/profile/profile');
let querys = require('../../model/dbQuerys/querys');
let log = require('../../logs/log')
let statics = require('../../static')
var Product = require('../../objects/product/Product')
var staticMethods = require('../../model/staticMethods')
let sendMail = require('../../mailer/v1/sendmail')

function Profile (userInfo,langId) {

    this.langId = typeof langId === "undefined" ? null : langId
    this.userInfo = typeof userInfo === "undefined" ? null : userInfo


    const self = this;

    this.defineQueryUserType = () => {
        // console.log('this.userInfo',this.userInfo)
        var queryUserInfo = {};
        if (this.userInfo.userType == 'guest') {
            queryUserInfo.field = 'guest_id';
            queryUserInfo.val = this.userInfo.userId;
        } else {
            queryUserInfo.field = 'user_id';
            queryUserInfo.val = this.userInfo.userId;
        }
        // console.log('queryUserInfo',queryUserInfo)
        return queryUserInfo
    }

     this.getProfileOrders = async (selectableOrdersCount) => {
         var queryUserInfo = await self.defineQueryUserType()
         return new Promise((resolve, reject) => {
             profileQuery.getUserOrders(selectableOrdersCount,queryUserInfo.val)
                 .then(function (data) {
                     // console.log('getProfileOrders',data)
                     for(var  i = 0; i < data.length; i++){
                         var orderDate = new Date(data[i].insert_data);
                         data[i].order_date = orderDate.getDate()+'/'+parseInt(orderDate.getMonth())+1+'/'+orderDate.getFullYear();
                         delete data[i].insert_data
                     }
                     resolve(data)
                 })
                 .catch(function (error) {
                     log.insertLog(JSON.stringify(error),"./logs/v1/profile.txt")
                     reject(new Error("error getProfileOrders"))
                 })
         })
    }

     this.removeOrder = async (orderId) => {
         var queryUserInfo = await self.defineQueryUserType()
         return new Promise((resolve, reject) => {
             querys.updatePromise('orders',{where:{id:orderId},values:{private : 1}})
                 .then(function (data) {
                     // console.log('getProfileOrders',data)
                     resolve(data)
                 })
                 .catch(function (error) {
                     log.insertLog(JSON.stringify(error),"./logs/v1/profile.txt")
                     reject(new Error("error getProfileOrders"))
                 })
         })
    }

     this.getProfileAllOrders = async () => {
         var queryUserInfo = await self.defineQueryUserType()
         return new Promise((resolve, reject) => {
             profileQuery.getUserAllOrders(queryUserInfo.val)
                 .then(function (data) {
                     // console.log('getProfileOrders',data)
                     for(var  i = 0; i < data.length; i++){
                         var orderDate = new Date(data[i].insert_data);
                         data[i].order_date = orderDate.getDate()+'/'+parseInt(orderDate.getMonth())+1+'/'+orderDate.getFullYear();
                         delete data[i].insert_data
                     }
                     resolve(data)
                 })
                 .catch(function (error) {
                     log.insertLog(JSON.stringify(error),"./logs/v1/profile.txt")
                     reject(new Error("error getProfileAllOrders"))
                 })
         })
    }

    this.generateRandonToken = function () {
        let dateObj = new Date();
        return sha1(sha1(staticMethods.generateString(10)) + "" + dateObj.getTime())
    }

    this.setNewEmail = async (newEmail) => {
        var token = await self.generateRandonToken();
        var url = statics.MAIN_URL+"/verify-email/change/"+token;

        const userId = self.userInfo.userId;
         return new Promise((resolve, reject) => {

             querys.findByMultyNamePromise('users_db',{email:newEmail}, ['id'])
                 .then(function (find) {
                     if(find.length === 0){
                         querys.findByMultyNamePromise('user_change_email',{user_id:userId}, ['id','email'])
                             .then(function (check) {
                                 console.log('check',check)
                                 if(check.length === 0){
                                     querys.insert2vPromise('user_change_email', {user_id : userId, email : newEmail, token : token})
                                         .then(function (data) {
                                             sendMail.sendEmailChangingEmail(newEmail,url)
                                             resolve({error:false, msg : 'ok'})
                                         })
                                         .catch(function () {
                                             resolve({error:true})
                                         })
                                 }else{
                                     if(check[0].email !== newEmail){
                                         querys.updatePromise('user_change_email',{where : {user_id : userId}, values : {email : newEmail, token : token}})
                                             .then(function (data) {
                                                 sendMail.sendEmailChangingEmail(newEmail,url)
                                                 resolve({error:false, msg : 'ok'})
                                             })
                                             .catch(function () {
                                                 resolve({error:true})
                                             })
                                     }else{
                                         resolve({error:true})
                                     }
                                 }
                             })
                             .catch(function (error) {
                                 resolve({error:true})
                             })

                     }else{
                         resolve({error:true,}) // registered_email
                     }
                 })
                 .catch(function () {
                     resolve({error:true})
                 })
         })
    }

    this.sendAuthenticationEmail = async function (userId,email) {
        var token = await self.generateRandonToken();
        var url = statics.MAIN_URL+"/verify-email/authentication/"+token;
        return new Promise((resolve, reject) => {
            querys.findByMultyNamePromise('email_tokens',{user_id : userId},['id'])
                .then(function (data) {
                    if(data.length > 0){
                        var id = data[0].id;
                        querys.updatePromise('email_tokens',{where : {id : id},values : {token : token}})
                            .then(function (data) {
                                sendMail.sendVerificationEMail(email,{url:url})
                                    .then(function (data) {
                                        resolve({error:false})
                                    })
                                    .catch(function (error) {
                                        resolve({error:false})
                                    })
                            })
                            .catch(function (error) {
                                resolve({error:false})
                            })
                    }else{
                        querys.insert2vPromise('email_tokens',{user_id : userId, token : token})
                            .then(function (data) {
                                sendMail.sendVerificationEMail(email,{url:url})
                                    .then(function (data) {
                                        resolve({error:false})
                                    })
                                    .catch(function (error) {
                                        resolve({error:false})
                                    })
                            })
                            .catch(function (error) {
                                resolve({error:false})
                            })
                    }
                })
                .catch(function (error) {
                    resolve({error:false})
                })
        })
    }

     this.getOrderById = async (publicId) => {
         var queryUserInfo = await self.defineQueryUserType()
         return new Promise((resolve, reject) => {
             profileQuery.getUserOrderByPublicId(queryUserInfo.field,queryUserInfo.val,publicId)
                 .then(function (data) {
                     for(var  i = 0; i < data.length; i++){
                         var orderDate = new Date(data[i].insert_data);
                         data[i].order_date = orderDate.getDate()+'/'+parseInt(orderDate.getMonth())+1+'/'+orderDate.getFullYear();
                         delete data[i].insert_data
                         data[i].total_price = staticMethods.formatMoney(data[i].total_price)
                     }
                     resolve(data)
                 })
                 .catch(function (error) {
                     log.insertLog(JSON.stringify(error),"./logs/v1/profile.txt")
                     reject(new Error("error getProfileAllOrders"))
                 })
         })
    }

     this.getOrderProducts = (langId,orderId) => {
         return new Promise((resolve, reject) => {
             profileQuery.getOrderProducts(orderId)
                 .then(function (data) {
                     var ids = [];
                     data.forEach(function (datas) {
                         ids.push(datas.product_id)
                     })
                     var mProduct = new Product(langId,{});
                     mProduct.getProductByIds(ids)
                         .then(function (products) {
                             // console.log('.......data.............',data)
                             // console.log('.......products.............',products)
                             let temp = [];
                             const count = data.length;
                             let tempId = 0;
                             for (let i = 0; i < count; i++) {
                                 tempId = data[i].product_id;
                                 temp[tempId] = {
                                     product_id: data[i].product_id,
                                     quantity: data[i].quantity,
                                 };
                             }
                             // console.log('.......temp1111.............',temp)
                             for (let i = 0; i < count; i++) {
                                 tempId = products[i].id;
                                 temp[tempId] = {
                                     product_id: temp[tempId].product_id,
                                     quantity: temp[tempId].quantity,
                                     ...products[i]
                                 };
                             }
                             const filtered = temp.filter(function (el) {
                                 return el != null;
                             });
                             // console.log('.......filtered.............',filtered)
                             // console.log('.......filtered.length.............',filtered.length)
                             resolve(filtered)
                         })
                         .catch(function (error) {
                             log.insertLog(JSON.stringify(error),"./logs/v1/profile.txt")
                             reject(new Error("error getOrderProducts"))
                         })



                 })
                 .catch(function (error) {
                     log.insertLog(JSON.stringify(error),"./logs/v1/profile.txt")
                     reject(new Error("error getOrderProducts"))
                 })
         })
    }

    this.getAccountLoyalityStatus = async () => {
        var queryUserInfo = await self.defineQueryUserType()
        return new Promise((resolve, reject) => {
            Promise.all([
                profileQuery.getAllLoyalityStatuses(self.langId),
                profileQuery.getUserLoyality(queryUserInfo.val)
            ])
                .then(function ([allLoyaltyStatuses,userLoyaltyStatus]) {
                    var loyalytResultArray = {
                        loyalty_info :allLoyaltyStatuses,
                        user_loyalty : {loyality_status_id : userLoyaltyStatus[0].loyality_status_id,spent_money : userLoyaltyStatus[0].spent_money}
                    }
                    resolve(loyalytResultArray)
                })
                .catch(function (error) {
                    // console.log('error',error)
                    log.insertLog(JSON.stringify(error),"./logs/v1/profile.txt")
                    reject(new Error("error getAccountLoyalityStatus"))
                })
        })
    }

    this.getUserAddresses = async () => {
        var queryUserInfo = await self.defineQueryUserType()
        return new Promise((resolve, reject) => {
            Promise.all([
                profileQuery.getUserAddresses(queryUserInfo.val)
            ])
                .then(function ([data]) {
                    // console.log('getUserAddresses',data)
                    resolve(data)
                })
                .catch(function (error) {
                    log.insertLog(JSON.stringify(error),"./logs/v1/profile.txt")
                    reject(new Error("error getUserAddresses"))
                })
        })
    }
    this.getUserAddressesLimits = async (limit) => {
        var queryUserInfo = await self.defineQueryUserType()
        return new Promise((resolve, reject) => {
            Promise.all([
                profileQuery.getUserAddressesLimit(limit,queryUserInfo.val)
            ])
                .then(function ([data]) {
                    // console.log('getUserAddresses',data)
                    resolve(data)
                })
                .catch(function (error) {
                    log.insertLog(JSON.stringify(error),"./logs/v1/profile.txt")
                    reject(new Error("error getUserAddresses"))
                })
        })
    }

    this.getUserAddressById = async (address_id) => {
        var queryUserInfo = await self.defineQueryUserType()
        return new Promise((resolve, reject) => {
            Promise.all([
                profileQuery.getUserAddressById(address_id)
            ])
                .then(function ([data]) {
                    // console.log('getUserAddresses',data)
                    resolve(data)
                })
                .catch(function (error) {
                    log.insertLog(JSON.stringify(error),"./logs/v1/profile.txt")
                    reject(new Error("error getUserAddresses"))
                })
        })
    }

    this.addAddress = async (body) => {
        var queryUserInfo = await self.defineQueryUserType()
        return new Promise((resolve, reject) => {
            let insertInfo = {
                user_id : queryUserInfo.val,
                name : body.name,
                last_name : body.last_name,
                phone : body.phone,
                email : body.email,
                city_id : body.city_id,
                text : body.text
            }
            querys.findByMultyNamePromise('user_address',{user_id : queryUserInfo.val},['id'])
                .then(function (addresses) {
                    if(addresses.length === 0){
                        insertInfo.def = '1'
                    }
                    querys.insert2vPromise('user_address',insertInfo)
                        .then(function (data) {
                            var addressId = data.insertId;
                            self.getUserAddressById(addressId)
                                .then(function (datas) {
                                    console.log('datas',datas)
                                    resolve(datas)
                                })
                                .catch(function (error) {
                                    resolve([])
                                })

                        })
                        .catch(function (error) {
                            log.insertLog(JSON.stringify(error)+" error Inserting address","./logs/v1/profile.txt")
                            reject(new Error("error addAddress"))
                        })
                })
                .catch(function (error) {
                    resolve([])
                })

        })
    }

    this.editAddress = async (body) => {
        var queryUserInfo = await self.defineQueryUserType()
        return new Promise((resolve, reject) => {
            querys.updatePromise('user_address',
                {
                    where:{id:body.address_id},
                    values:{
                        name : body.name,
                        last_name : body.last_name,
                        phone : body.phone,
                        email : body.email,
                        city_id : body.city_id,
                        text : body.text
                    }
                })
                .then(function (data) {
                    self.getUserAddressById(body.address_id)
                        .then(function (datas) {
                            resolve(datas)
                        })
                        .catch(function (error) {
                            resolve({})
                        })

                })
                .catch(function (error) {
                    log.insertLog(JSON.stringify(error)+" error Inserting address","./logs/v1/profile.txt")
                    reject(new Error("error editAddress"))
                })
        })
    }

    this.deleteAddress = async (addressId) => {
        var queryUserInfo = await self.defineQueryUserType()
        return new Promise((resolve, reject) => {
            querys.deletesPromise('user_address', {id:addressId})
                .then(function (data) {
                    resolve(data)
                })
                .catch(function (error) {
                    log.insertLog(JSON.stringify(error)+" error Inserting address","./logs/v1/profile.txt")
                    reject(new Error("error deleteAddress"))
                })
        })
    }

    this.setDefault = async (addressId) => {
        var queryUserInfo = await self.defineQueryUserType()
        return new Promise((resolve, reject) => {
            querys.updatePromise('user_address', {where : {user_id : queryUserInfo.val}, values : {def : '0'}})
                .then(function (data) {
                    querys.updatePromise('user_address', {where : {id : addressId}, values : {def : '1'}})
                        .then(function (data) {
                            resolve({error:false})
                        })
                        .catch(function (error) {
                            resolve({error:false})
                        })
                })
                .catch(function (error) {
                    resolve({error:false})
                })
        })
    }

    this.checkOldAndSetNewPassword = async (oldPass,newPass) => {
        return new Promise((resolve, reject) => {
            querys.findByMultyNamePromise('users_db',{id:self.userInfo.userId,password : oldPass},['id'])
                .then(function (data) {
                    if(data.length === 1){
                        querys.updatePromise('users_db',{where : {id:self.userInfo.userId},values : {password : newPass}})
                            .then(function () {
                                resolve({error:false})
                            })
                            .catch(function (error) {
                                resolve({error:true, errMsg : '999'})
                            })
                    }else{
                        resolve({error:true, errMsg : 'old password'})
                    }
                })
                .catch(function (error) {
                    resolve({error:true, errMsg : 'old password'})
                    console.log('error',error)
                })
        })
    }

    this.deleteProfile = () => {
        return new Promise((resolve, reject) => {
            querys.findByMultyNamePromise('users_db',{id:self.userInfo.userId},['id','name','last_name','email','birth_date','phone','status','password','token','email_verify','last_login_date'])
                .then(function (data) {
                    if(data.length === 1){
                        var insertInfo = {
                            user_id : data[0].id,
                            name : data[0].name,
                            last_name : data[0].last_name,
                            email : data[0].email,
                            birth_date : data[0].birth_date,
                            phone : data[0].phone,
                            status : data[0].status,
                            password : data[0].password,
                            token : data[0].token,
                            email_verify : data[0].email_verify,
                            last_login_date : data[0].last_login_date
                        };
                        querys.insert2vPromise('deleted_users', insertInfo)
                            .then(function () {
                                Promise.all([
                                    querys.deletesPromise('users_db', {id:data[0].id}),
                                    querys.deletesPromise('cart', {user_id:data[0].id})
                                ])
                                    .then(function ([data]) {
                                        resolve({error:false})
                                    })
                                    .catch(function (error) {
                                        log.insertLog('' +
                                            ' UserId - '+self.userInfo.userId+', ' +
                                            ' cannot delete from users or cart, ' +
                                            ' data - ' +JSON.stringify(insertInfo)+''+
                                            ' path - /api/objects/profile/Profile.js, ' +
                                            ' function findByMultyNamePromise(),error - '+JSON.stringify(error)+',  ' +
                                            ' date = '+new Date(),"./logs/v1/user-deleting.txt")
                                        resolve({error:true, errMsg : '58138468'})
                                    })
                            })
                            .catch(function (error) {
                                log.insertLog('' +
                                    ' UserId - '+self.userInfo.userId+', ' +
                                    ' cannot insert to deleted_users, ' +
                                    ' data - ' +JSON.stringify(insertInfo)+''+
                                    ' path - /api/objects/profile/Profile.js, ' +
                                    ' function insert2vPromise(),error - '+JSON.stringify(error)+',  ' +
                                    ' date = '+new Date(),"./logs/v1/user-deleting.txt")
                                resolve({error:true, errMsg : '1358465416'})
                            })
                    }else{
                        log.insertLog('' +
                            ' UserId - '+self.userInfo.userId+', ' +
                            ' cannot find from database, ' +
                            ' path - /api/objects/profile/Profile.js, ' +
                            ' function findByMultyNamePromise(),error - '+JSON.stringify(error)+',  ' +
                            ' date = '+new Date(),"./logs/v1/user-deleting.txt")
                        resolve({error:true, errMsg : 'no user'})
                        resolve({error:true, errMsg : 'no user'})
                    }
                })
                .catch(function (error) {
                    log.insertLog('' +
                        ' UserId - '+self.userInfo.userId+', ' +
                        ' cannot find from database, ' +
                        ' path - /api/objects/profile/Profile.js, ' +
                        ' function findByMultyNamePromise(),error - '+JSON.stringify(error)+',  ' +
                        ' date = '+new Date(),"./logs/v1/user-deleting.txt")
                    resolve({error:true, errMsg : 'no user'})
                    console.log('error',error)
                })
        })
    }

    // checkUpdateInformation -> check for  name,last_name, password errors
    this.checkUpdateInformation = function (body) {
        let namesTest = /[~!@#$?.%^&*()_+/|,]/g;
        let phoneTest = /^\d+$/g;

        let obj = {
            error : false,
            errorText : "",
            errorField : ""
        }

        // name
        if(body.name === null){
            obj.error = true;
            obj.errorText = "Name Error";
            obj.errorField = "name";
        }
        if(body.name.length > 25){
            obj.error = true;
            obj.errorText = "Name is long than 25 characters";
            obj.errorField = "name";
        }
        if(body.name.match(namesTest)){
            obj.error = true;
            obj.errorText = "Name have numbers or symbols";
            obj.errorField = "name";
        }

        // last_name
        if(body.last_name === null){
            obj.error = true;
            obj.errorText = "Last Name Error";
            obj.errorField = "last_name";
        }
        if(body.last_name.length > 25){
            obj.error = true;
            obj.errorText = "Last Name is long than 25 characters";
            obj.errorField = "last_name";
        }
        if(body.last_name.match(namesTest)){
            obj.error = true;
            obj.errorText = "Last Name have numbers or symbols";
            obj.errorField = "last_name";
        }

        // phone
        if(body.phone === null){
            obj.error = true;
            obj.errorText = "Phone Error";
            obj.errorField = "phone";
        }
        if(body.phone.length > 12){
            obj.error = true;
            obj.errorText = "Phone is longer than 12";
            obj.errorField = "phone";
        }
        if(!body.phone.match(phoneTest)){
            obj.error = true;
            obj.errorText = "Phone Validation error";
            obj.errorField = "phone";
        }

        // console.log('obj',obj)
        if(obj.error)
            log.insertLog(JSON.stringify(obj)+', date = '+new Date(),"./logs/v1/auth.txt")
        return obj
    }

    this.checkAdingAddressInformation = function (body) {
        let namesTest = /[~!@#$?.%^&*()_+/|,]/g;
        let phoneTest = /^\d+$/g;
        let checkEmail = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

        let obj = {
            error: false,
            errorText: "",
            errorField: ""
        }

        // name
        if (body.name === null) {
            obj.error = true;
            obj.errorText = "Name Error";
            obj.errorField = "name";
        }
        if (body.name.length > 150) {
            obj.error = true;
            obj.errorText = "Name is long than 150 characters";
            obj.errorField = "name";
        }
        if (body.name.match(namesTest)) {
            obj.error = true;
            obj.errorText = "Name have numbers or symbols";
            obj.errorField = "name";
        }

        // phone
        if (body.phone === null) {
            obj.error = true;
            obj.errorText = "Phone Error";
            obj.errorField = "phone";
        }
        if (body.phone.length > 12) {
            obj.error = true;
            obj.errorText = "Phone is longer than 12";
            obj.errorField = "phone";
        }
        if (!body.phone.match(phoneTest)) {
            obj.error = true;
            obj.errorText = "Phone Validation error";
            obj.errorField = "phone";
        }

        // email
        if (body.email === null) {
            obj.error = true;
            obj.errorText = "Email Error";
            obj.errorField = "email";
        }
        if (!checkEmail.test(body.email.toLowerCase())) {
            obj.error = true;
            obj.errorText = "Email Validation error";
            obj.errorField = "email";
        }
        if(obj.error)
            log.insertLog(JSON.stringify(obj)+', date = '+new Date(),"./logs/v1/auth.txt")
        return obj
    }

    this.checkEmail = function (email) {

        let checkEmail = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

        let obj = {
            error : false,
            errorText : "",
            errorField : ""
        }

        // email
        if(email === null){
            obj.error = true;
            obj.errorText = "Email Error";
            obj.errorField = "email";
        }
        if(!checkEmail.test(email.toLowerCase())){
            obj.error = true;
            obj.errorText = "Email Validation error";
            obj.errorField = "email";
        }


        // console.log('obj',obj)
        if(obj.error)
            log.insertLog(JSON.stringify(obj)+', date = '+new Date(),"./logs/v1/auth.txt")
        return obj
    }

    this.checkChangePassInfo = function (data) {
        let obj = {
            error : false,
            errorText : "",
            errorField : ""
        }

        // old_password
        if(data.old_password === null){
            obj.error = true;
            obj.errorText = "old_password Error";
            obj.errorField = "old_password";
        }
        if(data.old_password.length < 8 ){
            obj.error = true;
            obj.errorText = "old_password is short than 8";
            obj.errorField = "old_password";
        }
        // password
        if(data.password === null){
            obj.error = true;
            obj.errorText = "Password Error";
            obj.errorField = "password";
        }
        if(data.password.length < 8 ){
            obj.error = true;
            obj.errorText = "password is short than 8";
            obj.errorField = "password";
        }
        // re_password
        if(data.re_password === null){
            obj.error = true;
            obj.errorText = "re_password Error";
            obj.errorField = "re_password";
        }
        if(data.re_password.length < 8 ){
            obj.error = true;
            obj.errorText = "re_password is short than 8";
            obj.errorField = "re_password";
        }
        // password and re_password
        if(data.re_password !== data.password){
            obj.error = true;
            obj.errorText = "passwords are not same";
            obj.errorField = "";
        }
        if(obj.error)
            log.insertLog(JSON.stringify(obj)+', date = '+new Date(),"./logs/v1/profile.txt")
        return obj
    }
}

module.exports = Profile;