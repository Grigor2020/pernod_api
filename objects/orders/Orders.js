let querys = require('../../model/dbQuerys/querys');
let filterQuery = require('../../model/dbQuerys/filter/filter');
let productQuery = require('../../model/dbQuerys/product/product');
let logs = require('../../logs/log')
let sendMail = require('../../mailer/v1/sendmail')
let profileQuery = require('../../model/dbQuerys/profile/profile');
let ordersQuery = require('../../model/dbQuerys/orders/orders');
var staticMethods = require('../../model/staticMethods')
var statics = require('../../static.js')

function Orders (userInfo,langId) {

    this.userInfo = typeof userInfo === "undefined" ? null : userInfo
    this.langId = typeof langId === "undefined" ? null : langId
    this.generatedOrderId = ''
    this.result = {}
    // console.log('langIdlangIdlangIdlangIdlangIdlangIdlangIdlangId',langId)
    const self = this;

    this.defineQueryUserType = () => {
        // console.log('this.userInfo',this.userInfo)
        var queryUserInfo = {};
        if (this.userInfo.userType == 'guest') {
            queryUserInfo.field = 'guest_id';
            queryUserInfo.val = this.userInfo.userId;
        } else {
            queryUserInfo.field = 'user_id';
            queryUserInfo.val = this.userInfo.userId;
        }
        // console.log('queryUserInfo',queryUserInfo)
        return queryUserInfo
    }

    this.createOrderForUser = function (body,cartItems) {
        console.log('body', body)
        // console.log('body.address_id', body.address_id)
        // console.log('typeof body.address_id',typeof body.address_id)

        var self = this;
        return new Promise(function (resolve, reject) {
            if(body.address_id !== ''){
                querys.findByMultyNamePromise('user_address',{id:body.address_id},['city_id','text','name','last_name','phone','email'])
                    .then(function (addressInfos) {
                        var addressInfo = addressInfos[0]
                        // console.log('addressInfo',addressInfo)
                        var fInfo = {
                            delivery_date : body.delivery_date == '' ? null : body.delivery_date,
                            delivery_time : body.delivery_time == '' ? null : body.delivery_time,
                            comment : body.comment,
                            private : body.private,
                            subscribing : body.subscribing,
                            payment_type : parseInt(body.payment_type),
                            name : addressInfo.name,
                            send_email : body.send_email,
                            last_name : addressInfo.last_name,
                            email : addressInfo.email,
                            phone : addressInfo.phone,
                            cities_id : parseInt(addressInfo.city_id),
                            address : addressInfo.text,
                            address_id:body.address_id == '' ? null : body.address_id
                        }
                        self.userOrder(fInfo,cartItems)
                            .then(function (orderRes) {
                                resolve(orderRes)
                            })
                            .catch(function (error) {
                                resolve(error)
                            })
                    })
                    .catch(function (error) {
                        resolve(error)
                    })

            }else{
                var fInfo = {
                    delivery_date : body.delivery_date == '' ? null : body.delivery_date,
                    delivery_time : body.delivery_time == '' ? null : body.delivery_time,
                    comment : body.comment,
                    private : body.private,
                    subscribing : body.subscribing,
                    payment_type : parseInt(body.payment_type),
                    name : body.name,
                    send_email : body.send_email,
                    last_name : body.last_name,
                    email : body.email,
                    phone : body.phone,
                    cities_id : body.cities_id,
                    address : body.address,
                    address_id:null
                }
                // when need to add without user address
                self.userOrder(fInfo,cartItems)
                    .then(function (orderRes) {
                        if(body.save_to_my == 1){
                            var insertInfo = {
                                user_id : self.userInfo.userId,
                                name : body.name,
                                last_name : body.last_name,
                                email : body.email,
                                phone : body.phone,
                                city_id : parseInt(body.cities_id),
                                text : body.address,
                                def : '0'
                            }

                            querys.findByMultyNamePromise('user_address',{user_id :  self.userInfo.userId},['id'])
                                .then(function (addresses) {
                                    if(addresses.length === 0){
                                        insertInfo.def = '1'
                                    }
                                    querys.insert2vPromise('user_address',insertInfo)
                                        .then(function (data) {
                                            resolve(orderRes)
                                        })
                                        .catch(function (error) {
                                            log.insertLog(JSON.stringify(error)+" error Inserting address","./logs/v1/product.txt")
                                            reject(new Error("error addAddress"))
                                        })
                                })
                                .catch(function (error) {
                                    resolve(orderRes)
                                })
                        }else{
                            resolve(orderRes)
                        }
                    })
                    .catch(function (error) {
                        resolve(error)
                    })
            }
        });
    }

    this.insertOrder = function (orderInsertInfo, cb) {
        orderInsertInfo.public_id = staticMethods.generateRandomIntegers(10);
        self.generatedOrderId = orderInsertInfo.public_id
        // console.log('orderInsertInfo',orderInsertInfo)
        querys.insert2vPromise('orders',orderInsertInfo)
            .then(function (orderInsertData) {
                cb(orderInsertData)
            })
            .catch(function (error) {
                self.insertOrder(orderInsertInfo)
            })
    }

    this.userOrder = function (body,cartItems) {
        return new Promise(function (resolve, reject) {

            var catchNoInStock = false;
            let orderInsertInfo = {
                user_id : self.userInfo.userId,
                delivery_date : body.delivery_date,
                delivery_time : body.delivery_time,
                payment_type : body.payment_type,
                send_email : body.send_email,
                comment : body.comment,
                private : body.private,
                user_address_id : body.address_id,
                total_price : 0
            }
            for(let i = 0 ;  i < cartItems.length; i++){
                if(cartItems[i].in_stock == '0'){
                    orderInsertInfo.total_price += parseInt(cartItems[i].price.replace(/\s/g, '')) * parseInt(cartItems[i].quantity)
                }else{
                    catchNoInStock = true;
                    break;
                }
            }

            if(!catchNoInStock){
                // console.log('22orderInsertInfo',orderInsertInfo)
                self.insertOrder(orderInsertInfo,function (orderInsertData){
                    var allInserts = [];
                    // console.log('BODYYYYYYYYYY',body)
                    const orderId = orderInsertData.insertId

                    var fields = ['order_id','product_id','quantity']
                    var posts = [];
                    for(let i = 0 ;  i < cartItems.length; i++){
                        posts.push([orderId,cartItems[i].id,cartItems[i].quantity])
                    }
                    allInserts.push(querys.insertLoopPromise('order_product',fields,posts));

                    var orderInfoPost = {
                        order_id : orderId,
                        name : body.name,
                        last_name : body.last_name,
                        email : body.email,
                        phone : body.phone,
                        cities_id : parseInt(body.cities_id),
                        address : body.address
                    }
                    allInserts.push(querys.insert2vPromise('order_info',orderInfoPost))
                    // console.log('orderInfoPost',orderInfoPost)
                    var orderPaymentPost = {
                        order_id : orderId,
                        status : 0,
                    }


                    allInserts.push(querys.insert2vPromise('order_payment',orderPaymentPost))


                    // console.log('orderPaymentPost',orderPaymentPost)
                    Promise.all(allInserts)
                        .then(function ([orderProductRes,guestOrderInfoRes,orderPaymentRes]){
                            if(body.subscribing == '1'){
                                querys.insert2vPromise('subscribers',{user_id : orderInsertInfo.user_id, email :orderInfoPost.email })
                            }
                            // console.log('orderProductRes',orderProductRes)
                            // console.log('guestOrderInfoRes',guestOrderInfoRes)
                            // console.log('orderPaymentRes',orderPaymentRes)
                            resolve({error:false,data:{
                                    type : body.payment_type,
                                    order_payment_id : orderPaymentRes.insertId,
                                    // Amount : orderInsertInfo.total_price
                                    Amount : 10,
                                    // OrderID : 2360705
                                }})
                        })
                        .catch(function (error) {
                            // console.log('ERRRRRRROOOORRRRR :::: ',error)
                            resolve(error)
                        })
                })
            }else {
                resolve({error : true,errMsg : 'One of products is no in stock'})
            }
        })
    }

    this.createOrderForGuest = function (body,cartItems) {
        return new Promise(function (resolve, reject) {
            // console.log('cartItems',cartItems)

            let orderInsertInfo = {
                guest_id : userInfo.userId,
                delivery_date : body.delivery_date,
                delivery_time : body.delivery_time,
                payment_type : parseInt(body.payment_type),
                send_email : body.send_email,
                comment : body.comment,
                private : body.private,
                total_price : 0
            }

            var catchNoInStock = false;
            for(let i = 0 ;  i < cartItems.length; i++){
                if(cartItems[i].in_stock == '0'){
                    orderInsertInfo.total_price += parseInt(cartItems[i].price.replace(/\s/g, '')) * parseInt(cartItems[i].quantity)
                }else{
                    catchNoInStock = true;
                    break;
                }
            }

            // console.log('orderInsertInfo',orderInsertInfo)
            if(!catchNoInStock){
                self.insertOrder(orderInsertInfo,function (orderInsertData){
                    var allInserts = [];
                    // console.log('orderInsertData',orderInsertData)
                    const orderId = orderInsertData.insertId

                    var fields = ['order_id','product_id','quantity']
                    var posts = [];
                    for(let i = 0 ;  i < cartItems.length; i++){
                        posts.push([orderId,cartItems[i].id,cartItems[i].quantity])
                    }
                    allInserts.push(querys.insertLoopPromise('order_product',fields,posts));

                    var orderInfoPost = {
                        order_id : orderId,
                        name : body.name,
                        last_name : body.last_name,
                        email : body.email,
                        phone : body.phone,
                        cities_id : parseInt(body.cities_id),
                        address : body.address
                    }
                    allInserts.push(querys.insert2vPromise('order_info',orderInfoPost))
                    // console.log('orderInfoPost',orderInfoPost)
                    var orderPaymentPost = {
                        order_id : orderId,
                        status : 0,
                    }
                    allInserts.push(querys.insert2vPromise('order_payment',orderPaymentPost))
                    // console.log('orderPaymentPost',orderPaymentPost)
                    Promise.all(allInserts)
                        .then(function ([orderProductRes,guestOrderInfoRes,orderPaymentRes]){
                            if(body.subscribing == '1'){
                                querys.insert2vPromise('subscribers',{user_id : null, email :orderInfoPost.email })
                            }
                            // console.log('orderProductRes',orderProductRes)
                            // console.log('guestOrderInfoRes',guestOrderInfoRes)
                            // console.log('orderPaymentRes',orderPaymentRes)
                            resolve({error:false,data:{
                                    type : body.payment_type,
                                    order_payment_id : orderPaymentRes.insertId,
                                    // Amount : orderInsertInfo.total_price
                                    Amount : 10,
                                    // OrderID : 2360705
                                }})
                        })
                        .catch(function (error) {
                            // console.log('error',error)
                            reject(error)
                        })
                })

                // querys.insert2vPromise('orders',orderInsertInfo)
                //     .then(function (orderInsertData) {
                //
                //
                //     })
                //     .catch(function (error) {
                //         logs.insertLog("errorCode = 00129 "+ JSON.stringify(error)+', date = '+new Date(),"./logs/v1/auth.txt");
                //         resolve({error:true})
                //     })
            }else {
                resolve({error : true,errMsg : 'One of products is no in stock'})
            }
        });
    }

    this.addPaymentInitResponse = function (data) {
        // console.log('addPaymentInitResponse data',data)
        var PaymentID = data.PaymentID
        var paymentInitResponse = data.paymentInitResponse
        return new Promise((resolve, reject) => {
            querys.updatePromise('order_payment',
                {
                    where : {
                        id : data.order_payment_id
                    },
                    values : {
                        payment_id  :PaymentID,
                        paymentInitResponse  :paymentInitResponse
                    }
                }
                )
                .then(function (data) {
                    resolve({error:false})
                })
                .catch(function (error) {
                    // console.log('2222222222222222222')
                    reject({error:true})
                })
        })


    }
    this.addPaymentDetails = async  function (data) {
        var OrderID = data.OrderID
        var paymentDetailResponse = data.paymentDetails
        return new Promise((resolve, reject) => {
            Promise.all([
                querys.updatePromise('order_payment',
                    {
                        where : {
                            id  :OrderID,
                        },
                        values : {
                            paymentDetailResponse  :paymentDetailResponse,
                            status : parseInt(data.paymentStatus)
                        }
                    }
                ),
                querys.findByMultyNamePromise('order_payment', {id : OrderID},['order_id'])
            ])
                .then(function ([updateRes,orderInfo]) {
                    var dbOrderId = orderInfo[0].order_id;
                    Promise.all([
                        querys.findByMultyNamePromise('orders',{id : dbOrderId},['insert_data','delivery_date','delivery_time','payment_type','public_id','send_email','user_id','total_price']),
                        productQuery.getProductForOrderEmail(dbOrderId)
                    ])
                        .then(function ([orderDate,products]) {
                            // console.log('111111111111111111111orderDate',orderDate)
                            let pdfS = statics.MAIN_URL+'/pdf/131117068.pdf';
                            self.sendUserOrderEmail(dbOrderId,products,orderDate)
                            //
                            // console.log('products',products)
                            // console.log('public_id',orderDate[0].public_id)

                            resolve({error:false, data:{products:products,public_id : orderDate[0].public_id}})
                        })
                        .catch(function () {
                            // console.log('3444444444444')
                            reject({error:true})
                        })

                }) 
                .catch(function (error) {
                    // console.log('11111111111111111', error)
                    reject({error:true})
                })
        })
    }



    this.sendUserOrderEmail = async function (dbOrderId,products,orderDate) {
        // orderInfo -- te um hasceov a gnum
        // userInfo -- te um accountic a gnum katarvel
        var defineQueryUserType = await self.defineQueryUserType();
        if(defineQueryUserType.field == 'user_id'){
            if(orderDate[0].send_email == '1'){
                querys.getUserOrderInfo(dbOrderId)
                    .then(function (orderInfo) {

                        // console.log('products', products)
                        sendMail.sendOrderEmail(products,orderDate[0],orderInfo[0])
                        querys.findByMultyNamePromise('users_db', {id:orderDate[0].user_id}, ['name', 'last_name', 'phone', 'email'])
                            .then(function (userInfo) {
                                if(userInfo[0].email !== orderInfo[0].email){
                                    userInfo[0].address = ''
                                    userInfo[0].city = ''
                                    // console.log('userInfo',userInfo)
                                    sendMail.sendOrderEmail(products,orderDate[0],userInfo[0])
                                }
                            })
                            .catch(function (error) {
                                console.log('error',error)
                            })
                    })
                    .catch(function (error) {
                        console.log('error',error)
                    })
            }else{
                querys.getUserOrderInfo(dbOrderId)
                    .then(function (orderInfo) {
                        sendMail.sendOrderEmail(products,orderDate[0],orderInfo[0])
                    })
                    .catch(function (error) {
                        console.log('error',error)
                    })
            }
        }else{
            // self.sendOrderEmail(dbOrderId,products,orderDate)
            querys.getUserOrderInfo(dbOrderId)
                .then(function (orderInfo) {
                    sendMail.sendOrderEmail(products,orderDate[0],orderInfo[0])
                })
                .catch(function (error) {
                    console.log('error',error)
                })
        }
        // querys.findByMultyNamePromise('orders', {id : dbOrderId}, ['user_id', ])
        // Promise.all([
        //     querys.getUserOrderInfo(dbOrderId)
        // ])
        //     .then(function ([orderInfo]) {
        //         sendMail.sendOrderEmail(products,orderDate[0],orderInfo[0])
        //
        //     })
        //     .catch(function (error) {
        //         console.log('error',error)
        //     })
    }

    this.getOrderByPublicId = async function (body) {
        var defineQueryUserType = await this.defineQueryUserType();
        return new Promise((resolve, reject) => {
            // console.log('body.public_id',body.public_id)
            // console.log('defineQueryUserType',defineQueryUserType)
            Promise.all([
                ordersQuery.getOrderByPublicId(body.public_id,defineQueryUserType),
                ordersQuery.getOrderInfoByPublicId(body.public_id,defineQueryUserType)
            ])
                .then(function ([orderProducts,orderInfo]) {
                    // console.log('}}}}}}}}}}}}}}}}}}}}}}}}}}}}}orderProducts',orderProducts)
                    // console.log('}}}}}}}}}}}}}}}}}}}}}}}}}}}}}orderInfo',orderInfo)
                    for(var  i = 0; i < orderProducts.length; i++){
                        var orderDate = new Date(orderProducts[i].insert_data);
                        orderProducts[i].order_date = orderDate.getDate()+'/'+parseInt(orderDate.getMonth())+1+'/'+orderDate.getFullYear();
                        delete orderProducts[i].insert_data
                    }
                    var orderDetaling = orderInfo[0];
                    if(orderDetaling.payment_type == '1'){
                        var payDetail = JSON.parse(orderInfo[0].paymentDetailResponse)
                        orderDetaling.CardNumber = payDetail.CardNumber
                        // orderDetaling.Amount = payDetail.Amount
                        delete orderDetaling.paymentDetailResponse
                    }
                    resolve({orderProducts : orderProducts,orderDetaling : orderDetaling})
                })
                .catch(function (error) {
                    console.log('error',error)
                    resolve({orderProducts : [],orderDetaling : []})
                })
        })
    }
}

module.exports = Orders