let sha1 = require('sha1')
let querys = require('../../model/dbQuerys/querys');
let logs = require('../../logs/log')
let sendMail = require('../../mailer/v1/sendmail')
let statics = require('../../static');
let staticMethods = require('../../model/staticMethods');
var Cart = require('../../objects/cart/Cart')

function Auth (userInfo,userType) {
    // console.log('userInfo',userInfo)
    // console.log('userType',userType)
    this.userId = typeof userInfo.userId === "undefined" ? null : userInfo.userId
    this.name = typeof userInfo.name === "undefined" ? null : userInfo.name
    this.last_name = typeof userInfo.last_name === "undefined" ? null : userInfo.last_name
    this.email = typeof userInfo.email === "undefined" ? null : userInfo.email
    this.birth_date = typeof userInfo.birth_date === "undefined" ? null : userInfo.birth_date
    this.phone = typeof userInfo.phone === "undefined" ? null : userInfo.phone
    this.password = typeof userInfo.password === "undefined" ? null : userInfo.password
    this.passwordHashed = typeof userInfo.password === "undefined" ? null : sha1(userInfo.password)
    this.re_password = typeof userInfo.re_password === "undefined" ? null : userInfo.re_password
    this.token = typeof userInfo.token === "undefined" ? null : userInfo.token
    this.userType = typeof userType === "undefined" ? null : userType

    this.result = {}

    const self = this;

    // console.log('userType...........')

    this.setToken = function () {
        let dateObj = new Date();
        self.token =  sha1(sha1(self.phone) + "" + dateObj.getTime())
        return true
    }
    this.generateRandonToken = function () {
        let dateObj = new Date();
        return sha1(sha1(staticMethods.generateString(10)) + "" + dateObj.getTime())
    }



    // checkSignUpInformation -> check for name, last_name, main, phone errors
    this.checkSignUpInformation = function () {
        let namesTest = /[~!@#$?.%^&*()_+/|,]/g;
        let phoneTest = /^\d+$/g;
        let checkEmail = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

        let obj = {
            error : false,
            errorText : "",
            errorField : ""
        }
        // name
        if(self.name === null){
            obj.error = true;
            obj.errorText = "Name Error";
            obj.errorField = "name";
        }
        if(self.name.length > 25){
            obj.error = true;
            obj.errorText = "Name is long than 25 characters";
            obj.errorField = "name";
        }
        if(self.name.match(namesTest)){
            obj.error = true;
            obj.errorText = "Name have numbers or symbols";
            obj.errorField = "name";
        }
        // last_name
        if(self.last_name === null){
            obj.error = true;
            obj.errorText = "Last Name Error";
            obj.errorField = "last_name";
        }
        if(self.last_name.length > 25){
            obj.error = true;
            obj.errorText = "Last Name is long than 25 characters";
            obj.errorField = "last_name";
        }
        if(self.last_name.match(namesTest)){
            obj.error = true;
            obj.errorText = "Last Name have numbers or symbols";
            obj.errorField = "last_name";
        }
        // email
        if(self.email === null){
            obj.error = true;
            obj.errorText = "Email Error";
            obj.errorField = "email";
        }
        if(!checkEmail.test(self.email.toLowerCase())){
            obj.error = true;
            obj.errorText = "Email Validation error";
            obj.errorField = "email";
        }
        // phone
        if(self.phone === null){
            obj.error = true;
            obj.errorText = "Phone Error";
            obj.errorField = "phone";
        }
        if(self.phone.length > 12){
            obj.error = true;
            obj.errorText = "Phone is longer than 12";
            obj.errorField = "phone";
        }
        if(!self.phone.match(phoneTest)){
            obj.error = true;
            obj.errorText = "Phone Validation error";
            obj.errorField = "phone";
        }
        // password
        if(self.password === null){
            obj.error = true;
            obj.errorText = "Password Error";
            obj.errorField = "password";
        }
        if(self.password.length < 8 ){
            obj.error = true;
            obj.errorText = "password is short than 8";
            obj.errorField = "password";
        }
        // re_password
        if(self.re_password === null){
            obj.error = true;
            obj.errorText = "re_password Error";
            obj.errorField = "re_password";
        }
        if(self.re_password.length < 8 ){
            obj.error = true;
            obj.errorText = "re_password is short than 8";
            obj.errorField = "re_password";
        }
        // password and re_password
        if(self.re_password !== self.password){
            obj.error = true;
            obj.errorText = "passwords are not same";
            obj.errorField = "password and re_password";
        }
        if(obj.error)
            logs.insertLog(JSON.stringify(obj)+', date = '+new Date(),"./logs/v1/auth.txt")
        return obj
    }
    // check Forgot Passwors -> check for password and repassword errors
    this.checkForgotInfo = function () {

        let obj = {
            error : false,
            errorText : "",
            errorField : ""
        }
        // password
        if(self.password === null){
            obj.error = true;
            obj.errorText = "Password Error";
            obj.errorField = "password";
        }
        if(self.password.length < 8 ){
            obj.error = true;
            obj.errorText = "password is short than 8";
            obj.errorField = "password";
        }
        // re_password
        if(self.re_password === null){
            obj.error = true;
            obj.errorText = "re_password Error";
            obj.errorField = "re_password";
        }
        if(self.re_password.length < 8 ){
            obj.error = true;
            obj.errorText = "re_password is short than 8";
            obj.errorField = "re_password";
        }
        // password and re_password
        if(self.re_password !== self.password){
            obj.error = true;
            obj.errorText = "passwords are not same";
            obj.errorField = "password and re_password";
        }

        if(obj.error)
            logs.insertLog(JSON.stringify(obj)+', date = '+new Date(),"./logs/v1/auth.txt")
        console.log('obj',obj)
        return obj
    }

    // checkSignInInformation -> check for  mail, password errors
    this.checkSignInInformation = function () {
        let checkEmail = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        let obj = {
            error : false,
            errorText : "",
            errorField : ""
        }

        // email
        if(self.email === null){
            obj.error = true;
            obj.errorText = "Email Error";
            obj.errorField = "email";
        }
        if(!checkEmail.test(self.email.toLowerCase())){
            obj.error = true;
            obj.errorText = "Email Validation error";
            obj.errorField = "email";
        }
        // password
        if(self.password === null){
            obj.error = true;
            obj.errorText = "Password Error";
            obj.errorField = "password";
        }
        if(self.password.length < 8 ){
            obj.error = true;
            obj.errorText = "password is short than 8";
            obj.errorField = "password";
        }
        if(obj.error)
            logs.insertLog(JSON.stringify(obj)+', date = '+new Date(),"./logs/v1/auth.txt")
        return obj
    }

    this.checkEmailExists = function (){
        let obj = {
            error : false,
            errorText : ""
        }
        return  new Promise((resolve) => {
            querys.findByMultyNamePromise('users_db',{email : self.email},['id'])
                .then(function (data) {
                    if(data.length === 0){
                        obj.error = false;
                        obj.errorText = "";
                        obj.data = {}
                    }else{
                        obj.error = true
                        obj.errorText = "user Exists"
                        obj.data = data
                        logs.insertLog("errorCode = 00107, user Exists"+', date = '+new Date(),"./logs/v1/auth.txt")
                    }
                    resolve(obj)
                })
                .catch(function (error) {
                    obj.error = true
                    obj.errorText = "check user Exists SQL"
                    logs.insertLog("errorCode = 00106 "+ JSON.stringify(error)+', date = '+new Date(),"./logs/v1/auth.txt");
                    resolve(obj)
                })
        })
    }

    this.updateFavoriteGuestToUser = function (userid) {
        let guestId = self.userType.userId;
        return new Promise((resolve) => {
            Promise.all([
                querys.findByMultyNamePromise('favorite',{guest_id:guestId},['product_id','id']),
                querys.findByMultyNamePromise('favorite',{user_id:userid},['product_id','id'])
            ])

                .then(function ([guestItems,userItems]) {
                    console.log('guestItems',guestItems)
                    console.log('.......................')
                    console.log('userItems',userItems)

                    var newItems = [];
                    // var newProds = [];
                    for(var i = 0; i < userItems.length; i++){
                        if(newItems.indexOf(userItems[i].product_id) === -1 ){
                            newItems.push(userItems[i].product_id)
                            // newProds.push({id : userItems[i].id, product_id : userItems[i].product_id})
                        }
                    }
                    for(var i = 0; i < guestItems.length; i++){
                        if(newItems.indexOf(guestItems[i].product_id) === -1 ){
                            newItems.push(guestItems[i].product_id)
                            // newProds.push({id : guestItems[i].id, product_id : guestItems[i].product_id})
                        }
                    }
                    if(newItems.length > 0){
                        Promise.all([
                            querys.deletesPromise('favorite',{user_id : userid}),
                            querys.deletesPromise('favorite',{guest_id : guestId})
                        ])
                            .then(function ([delRes]) {
                                console.log('newItems',newItems)
                                var fields = ['guest_id','user_id','product_id']
                                var post = []
                                for(var i = 0; i < newItems.length; i++){
                                    post.push([null,userid,newItems[i]])
                                }
                                console.log('fields',fields)
                                console.log('post',post)
                                querys.insertLoopPromise('favorite', fields, post)
                                    .then(function (data) {
                                        resolve({error : false,errorText : ""})
                                    })
                                    .catch(function (error) {
                                        logs.insertLog("errorCode = 00111 "+ JSON.stringify(error)+', date = '+new Date(),"./logs/v1/auth.txt");
                                        resolve({error : true,errorText : "Update favorite guest to user"})
                                    })
                            })
                            .catch(function (error) {
                                logs.insertLog("errorCode = 00110 "+ JSON.stringify(error)+', date = '+new Date(),"./logs/v1/auth.txt");
                                resolve({error : true,errorText : "Update favorite guest to user"})
                            })
                    }else{
                        resolve({error : false,errorText : ""})
                    }

                })
                .catch(function (error) {
                    logs.insertLog("errorCode = 00109 "+ JSON.stringify(error)+', date = '+new Date(),"./logs/v1/auth.txt");
                    resolve({error : true,errorText : "Update favorite guest to user"})
                })
        })
    }

    this.updateCartGuestToUser = function (userid) {
        let guestId = self.userType.userId;
        console.log('userid',userid)
        console.log('guestId',guestId)
        return new Promise((resolve) => {
            Promise.all([
                querys.findByMultyNamePromise('cart',{guest_id  : guestId},['id','product_id','guest_id','quantity','insert_date']),
                querys.findByMultyNamePromise('cart',{user_id  : userid},['id','product_id','user_id','quantity','insert_date'])
            ])
                .then(([guestItems,userItems])=>{
                    // console.log('guestItems',guestItems)
                    // console.log('userItems',userItems)
                    if(guestItems.length > 0){
                        var userProdIds = []
                        for(let i = 0 ;  i < userItems.length; i++){
                            userProdIds.push(userItems[i].id)
                        }
                        for(let j = 0 ;  j < guestItems.length; j++){
                            userProdIds.push(guestItems[j].id)
                        }
                        querys.deletesMultyRowPromise('cart','id',userProdIds)
                            .then(function () {
                                var fields = ['user_id','guest_id','product_id','quantity'];
                                var post = [];
                                for(let j = 0 ;  j < guestItems.length; j++){
                                    post.push([userid,null,guestItems[j].product_id, guestItems[j].quantity])
                                }
                                querys.insertLoopPromise('cart',fields,post)
                                    .then(function () {
                                        resolve({error : false,errorText : ""})
                                    })
                                    .catch(function () {
                                        resolve({error : true,errorText : "Update favorite guest to user"})
                                    })
                            })
                            .catch(function () {
                                resolve({error : true,errorText : "Update favorite guest to user"})
                            })
                    }else{
                        resolve({error : false,errorText : ""})
                    }
                })
                .catch(()=>{
                    resolve({error : true,errorText : "Update favorite guest to user"})
                })
        })
    }

    this.setHashedPassword = function () {
        this.passwordHashed = sha1(this.password);
    }

    this.setEmail = function (email) {
        self.email = email;
    }

    this.signUp = function () {
        let obj = {
            error : false,
            errorText : "",
            result : {}
        }
        let insertInfo = {
            name : self.name,
            last_name : self.last_name,
            email : self.email,
            birth_date : self.birth_date,
            phone : self.phone,
            password : self.passwordHashed,
            token : self.token,
            email_verify : '0'
        }
        console.log('insertInfo',insertInfo)
        return new Promise((resolve) => {
            querys.insert2vPromise('users_db',insertInfo)
                .then(function (data) {
                    self.setEmail(insertInfo.email)
                    let mailParams = {
                        userName : insertInfo.name,
                        lastName : insertInfo.last_name
                    }
                    self.sendAuthenticationEmail(data.insertId)
                        .then(function (smsResult) {
                            obj.result.userInfo = {
                                id : data.insertId,
                                name : self.name,
                                last_name : self.last_name,
                                token : insertInfo.token
                            }
                            obj.result.emailInfo = {
                                // success : smsResult.error
                                success : false
                            }
                            resolve(obj)
                        })
                        .catch(function (error) {
                            obj.result.userInfo = {
                                id : data.insertId,
                                name : self.name,
                                last_name : self.last_name,
                                token : insertInfo.token
                            }
                            obj.result.emailInfo = {
                                // success : smsResult.error
                                success : true
                            }
                            resolve(obj)
                        })
                })
                .catch(function (error) {
                    obj.error = true;
                    obj.errorText = 'Insert users_db SQL';
                    logs.insertLog("errorCode = 00108 "+ JSON.stringify(error)+', date = '+new Date(),"./logs/v1/auth.txt");
                    resolve(obj)
                })
        })
    }

    this.signIn = function () {
        var obj = {
            error : false,
            errorText : "",
            result : {}
        }
        return new Promise((resolve) => {
            // console.log('self.password',self.password)
            // console.log('self.passwordHashed',self.passwordHashed)
            querys.findByMultyNamePromise('users_db',{email:self.email,password : self.passwordHashed},['id','token','name','last_name','email_verify'])
                .then(function (data) {
                    if(data.length === 0){
                        obj.error = true;
                        obj.errorText = 'No User';
                        resolve(obj)
                    }else{
                        if(data[0].email_verify === '0'){
                            obj.error = true;
                            obj.errorText = 'No Verifyed Email';
                            obj.result.userInfo = {
                                id : data[0].id,
                                token : data[0].token,
                                email_verify : data[0].email_verify
                            }
                            resolve(obj)
                        }else{
                            obj.result.userInfo = {
                                id : data[0].id,
                                token : data[0].token,
                                email_verify : data[0].email_verify
                            }
                            resolve(obj)
                        }
                    }

                })
                .catch(function (error) {
                    obj.error = true;
                    obj.errorText = 'Insert users_db SQL';
                    logs.insertLog("errorCode = 00120 "+ JSON.stringify(error)+', date = '+new Date(),"./logs/v1/auth.txt");
                    resolve(obj)
                })
        })
    }

    this.updatePassword = function () {
        var obj = {
            error : false,
            errorText : "",
            result : {}
        }
        console.log('self.userId',self.userId)
        console.log('self.passwordHashed',self.passwordHashed)
        return new Promise((resolve) => {
            querys.updatePromise('users_db',{where : {id : self.userId},values : {password : self.passwordHashed}})
                .then(function () {
                    querys.deletesPromise('forgot_tokens',{user_id: self.userId})
                        .then(function () {
                            resolve(obj)
                        })
                        .catch(function () {
                            obj.error = true;
                            obj.errorText = "can't delete token row SQL";
                            resolve(obj)
                        })
                })
                .catch(function () {
                    obj.error = true;
                    obj.errorText = "can't update password SQL";
                    resolve(obj)
                })
        })
    }

    this.setStatus = function () {
        return new Promise((resolve, reject) => {

        })
    }


    this.checkUser = function (langId) {
        var obj = {
            error : false,
            errorText : "",
            data : {}
        }
        return new Promise((resolve, reject) => {
            var mCart = new Cart(userType,langId);
            if(self.userType.userType == 'guest'){
                Promise.all([
                    // querys.getCartInfoByUserOrGuest({field : 'guest_id',id : self.userType.userId},langId),
                    mCart.getCartInfo(),
                    querys.getFavouriteCount({field : 'guest_id',id : self.userType.userId})
                ])
                    .then(function ([cart,favouriteCount]) {
                        // console.log('GUESTTTTTTTTTTTTT......cart',cart)
                        obj.data.favouriteCount = favouriteCount[0].fav_count
                        obj.data.cart = cart;
                        resolve(obj)
                    })
                    .catch(function (error) {
                        console.log('error',error)
                        obj.error = true;
                        obj.errorText = 'Check User SQL';
                        logs.insertLog("errorCode = 00129 "+ JSON.stringify(error)+', date = '+new Date(),"./logs/v1/auth.txt");
                        resolve(obj)
                    })
            }else{
                querys.findUserInfo(self.userType.userId)
                // querys.findByMultyNamePromise('users_db',{id : self.userType.userId},['name','last_name','status','token','phone','email','email_verify'])
                    .then((data)=>{
                        // console.log(':::::::::::data::::::::::',data)
                        if(data.length === 0){
                            obj.error = true;
                            obj.errorText = 'No User';
                            resolve(obj)
                        }else{
                            Promise.all([
                                mCart.getCartInfo(),
                                querys.getFavouriteCount({field : 'user_id',id : self.userType.userId})
                            ])
                                .then(function ([cart,favouriteCount]) {
                                    obj.data.favouriteCount = favouriteCount[0].fav_count
                                    obj.data.cart = cart;
                                    resolve(obj)
                                    obj.data.userInfo = {
                                        id : data[0].id,
                                        name : data[0].name,
                                        last_name : data[0].last_name,
                                        phone : data[0].phone,
                                        email : data[0].email,
                                        changing_email : data[0].changing_email,
                                        email_verify : data[0].email_verify,
                                        token : data[0].token
                                    }
                                    resolve(obj)
                                })
                                .catch(function (error) {
                                    obj.error = true;
                                    obj.errorText = 'Check User SQL';
                                    logs.insertLog("errorCode = 00130 "+ JSON.stringify(error)+', date = '+new Date(),"./logs/v1/auth.txt");
                                    resolve(obj)
                                })
                        }
                    })
                    .catch((error)=>{
                        obj.error = true;
                        obj.errorText = 'Check User SQL';
                        logs.insertLog("errorCode = 00129 "+ JSON.stringify(error)+', date = '+new Date(),"./logs/v1/auth.txt");
                        resolve(obj)
                    })
            }
        })
    }

    this.sendForgotEmail = async function () {
        var obj = {
            error:false,
            errorTxt : ""
        }
        var token = await this.generateRandonToken();
        return new Promise((resolve, reject) => {
            var url = statics.MAIN_URL+"/password-recovery/verify/"+token;
            sendMail.sendMailForgotUrl(self.email,{url : url})
                .then(function (smsResult) {
                    querys.findByMultyNamePromise('forgot_tokens',{user_id : self.userId},['id'])
                        .then(function (data) {
                            if(data.length !== 0){
                                querys.updatePromise('forgot_tokens',{where : {id : data[0].id},values : {token : token}})
                                    .then(function (data) {
                                        resolve(data)
                                    })
                                    .catch(function () {
                                        obj.error = true;
                                        obj.errorTxt = "Can't Update Token"
                                        resolve(obj)
                                    })
                            }else{
                                querys.insert2vPromise('forgot_tokens',{user_id : self.userId,token : token})
                                    .then(function (data) {
                                        resolve(data)
                                    })
                                    .catch(function () {
                                        obj.error = true;
                                        obj.errorTxt = "Can't Insert Token"
                                        resolve(obj)
                                    })
                            }
                        })
                        .catch(function () {
                            obj.error = true;
                            obj.errorTxt = "Find token SQL"
                            resolve(obj)
                        })
                })
                .catch(function () {
                    obj.error = true;
                    obj.errorTxt = "Sms Not Sent"
                    resolve(obj)
                })
        })
    }

    this.checkForgotToken = async function (token) {
        var obj = {
            error:false,
            errorTxt : ""
        }
        return new Promise((resolve, reject) => {
            querys.findByMultyNamePromise('forgot_tokens',{token : token},['user_id'])
                .then(function (data) {
                    if(data.length  > 0){
                        self.setUserId(data[0].user_id)
                        obj.error = false;
                    }else {
                        obj.error = true;
                        obj.errorTxt = "No Token"
                    }
                    resolve(obj)
                })
                .catch(function () {
                    obj.error = true;
                    obj.errorTxt = "Not Valid Token"
                    resolve(obj)
                })
        })
    }

    this.sendAuthenticationEmail = async function (userId) {
        var token = await this.generateRandonToken();
        var url = statics.MAIN_URL+"/verify-email/authentication/"+token;
        return new Promise((resolve, reject) => {
            querys.findByMultyNamePromise('email_tokens',{user_id : userId},['id'])
                .then(function (data) {
                    console.log('....................data',data)
                    if(data.length > 0){
                        var id = data[0].id;
                        console.log('....................id',id)
                        querys.updatePromise('email_tokens',{where : {id : id},values : {token : token}})
                            .then(function (data) {

                                console.log('self.email',self.email)
                                console.log('url',url)
                                sendMail.sendVerificationEMail(self.email,{url:url})
                                    .then(function (data) {
                                        resolve({error:false})
                                    })
                                    .catch(function (error) {
                                        resolve({error:false})
                                    })
                            })
                            .catch(function (error) {
                                resolve({error:false})
                            })
                    }else{
                        querys.insert2vPromise('email_tokens',{user_id : userId, token : token})
                            .then(function (data) {
                                sendMail.sendVerificationEMail(self.email,{url:url})
                                    .then(function (data) {
                                        resolve({error:false})
                                    })
                                    .catch(function (error) {
                                        resolve({error:false})
                                    })
                            })
                            .catch(function (error) {
                                resolve({error:false})
                            })
                    }
                })
                .catch(function (error) {
                    resolve({error:false})
                })
        })
    }

    this.checkEmailAuthenticationToken = async function (token) {
        return new Promise((resolve, reject) => {
            querys.findByMultyNamePromise('email_tokens',{token : token},['id','user_id'])
                .then(function (data) {
                    console.log('data',data)
                    if(data.length > 0){
                        var id = data[0].id;
                        var user_id = data[0].user_id;
                        Promise.all([
                            querys.updatePromise('users_db',{where : {id : user_id},values : {email_verify : '1'}}),
                            querys.deletesPromise('email_tokens',{id:id})
                        ])
                            .then(function ([data]) {
                                resolve({error:false})
                            })
                            .catch(function (error) {
                                resolve({error:true, errMsg : "update or del SQL"})
                            })
                    }else{
                        resolve({error:true, errMsg : "not valid token"})
                    }
                })
                .catch(function (error) {
                    resolve({error:true, errMsg : "Not Valid Token"})
                })
        })
    }
    this.checkEmailChangeToken = async function (token) {
        return new Promise((resolve, reject) => {
            querys.findByMultyNamePromise('user_change_email',{token : token},['id','user_id','email'])
                .then(function (data) {
                    console.log('data',data)
                    if(data.length > 0){
                        var id = data[0].id;
                        var user_id = data[0].user_id;
                        var email = data[0].email;
                        Promise.all([
                            querys.updatePromise('users_db',{where : {id : user_id},values : {email_verify : '1',email : email}}),
                            querys.deletesPromise('user_change_email',{id:id})
                        ])
                            .then(function ([data]) {
                                resolve({error:false})
                            })
                            .catch(function (error) {
                                resolve({error:true, errMsg : "update or del SQL"})
                            })
                    }else{
                        resolve({error:true, errMsg : "not valid token"})
                    }
                })
                .catch(function (error) {
                    resolve({error:true, errMsg : "Not Valid Token"})
                })
        })
    }
    // this.getUserInfoById = async function (userId) {
    //     return new Promise((resolve, reject) => {
    //         querys.findByMultyNamePromise('users_db',{id : userId},['id','user_id'])
    //             .then(function (data) {
    //                 console.log('data',data)
    //                 if(data.length > 0){
    //                     var id = data[0].id;
    //                     var user_id = data[0].user_id;
    //                     Promise.all([
    //                         querys.updatePromise('users_db',{where : {id : user_id},values : {email_verify : '1'}}),
    //                         querys.deletesPromise('email_tokens',{id:id})
    //                     ])
    //                         .then(function ([data]) {
    //                             resolve({error:false})
    //                         })
    //                         .catch(function (error) {
    //                             resolve({error:true, errMsg : "update or del SQL"})
    //                         })
    //                 }else{
    //                     resolve({error:true, errMsg : "not valid token"})
    //                 }
    //             })
    //             .catch(function (error) {
    //                 resolve({error:true, errMsg : "Not Valid Token"})
    //             })
    //     })
    // }

    this.getUserInfoByEmail = async function () {
        return new Promise((resolve, reject) => {
            querys.findByMultyNamePromise('users_db',{email : self.email},['id','email_verify'])
                .then(function (data) {
                    resolve(data)
                })
                .catch(function (error) {
                    resolve({error:true, errMsg : "update or del SQL"})
                })
        })
    }

    this.updateUserLastLogin = async function (user_id) {
        return new Promise((resolve, reject) => {
            querys.updateLastLogin(user_id)
                .then(function (data) {
                    resolve(data)
                })
                .catch(function (error) {
                    resolve({error:true, errMsg : "update or del SQL"})
                })
        })
    }

    this.setUserId = function (userId) {
        self.userId = userId;
        return userId
    }
}

module.exports = Auth;





// {
//     "error": false,
//     "errorText": "",
//     "data": {
//     "favouriteCount": 0,
//         "cart": {
//         "items": [
//             {
//                 "quantity": 2,
//                 "id": 42,
//                 "price": "47 500",
//                 "in_stock": "0",
//                 "price_before_sale": null,
//                 "name": "test 777",
//                 "catName": "Գինի",
//                 "image": "http://127.0.0.1:3091/images/product/1139944-1608372611621.png",
//                 "item_total_price": 94
//             },
//             {
//                 "quantity": 2,
//                 "id": 40,
//                 "price": "8 000",
//                 "in_stock": "0",
//                 "price_before_sale": null,
//                 "name": "ghghg",
//                 "catName": "Լիկյորներ",
//                 "image": "http://127.0.0.1:3091/images/product/258173-1607772589473.png",
//                 "item_total_price": 16
//             }
//         ],
//             "allProdQuantity": 4,
//             "total": "111 000",
//             "beforeDiscount": "111 000"
//     }
// }
// }

// {
//     "error": false,
//     "errorText": "",
//     "data": {
//     "favouriteCount": 0,
//         "cart": {
//         "items": [
//             {
//                 "quantity": 2,
//                 "id": 40,
//                 "price": "8 000",
//                 "in_stock": "0",
//                 "price_before_sale": null,
//                 "name": "ghghg",
//                 "catName": "Լիկյորներ",
//                 "image": "http://127.0.0.1:3091/images/product/258173-1607772589473.png",
//                 "item_total_price": 16
//             }
//         ],
//             "allProdQuantity": 2,
//             "total": "16 000",
//             "beforeDiscount": "16 000"
//     },
//     "userInfo": {
//         "name": "Grigor",
//             "last_name": "Sadoyan",
//             "phone": "37494337837",
//             "email": "sadoyangrigor@gmail.com",
//             "email_verify": "1",
//             "token": "ef07d1c29d111f3c4e461e87d1f1b436a126d8dc"
//     }
// }
// }