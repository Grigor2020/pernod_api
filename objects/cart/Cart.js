let sha1 = require('sha1')
let querys = require('../../model/dbQuerys/querys');
let catalogQuery = require('../../model/dbQuerys/catalog/catalog');
let logs = require('../../logs/log')
let sendMail = require('../../mailer/v1/sendmail')
let cartQuery = require('../../model/dbQuerys/cart/cart');
let productQuery = require('../../model/dbQuerys/product/product');
let staticMethods = require('../../model/staticMethods');
var Filter = require('../filters/Filter')

function Cart (userInfo,langId) {
    this.userInfo = typeof userInfo === "undefined" ? null : userInfo
    this.langId = typeof langId === "undefined" ? null : langId
    this.result = {}
    // console.log('langIdlangIdlangIdlangIdlangIdlangIdlangIdlangId',langId)
    const self = this;

    this.addToCart = (productInfo,langId) => {
        return new Promise((resolve, reject) => {
            // console.log('productInfo.quantity',productInfo.quantity)
            var insertData = {
                product_id:productInfo.productId,
                quantity : productInfo.quantity == "" || productInfo.quantity < 0 || typeof productInfo.quantity == "undefined" ? 1 : productInfo.quantity
            }
            if(self.userInfo.userType == 'user'){
                insertData.user_id = self.userInfo.userId
            }
            if(self.userInfo.userType == 'guest'){
                insertData.guest_id = self.userInfo.userId
            }
            // console.log('insertData',insertData)

            querys.insert2vPromise('cart',insertData)
                .then(function (data) {
                    resolve( data )
                })
                .catch(function (error) {
                    console.log('error',error)
                })
        })
    }

    this.updateCartCount = (productInfo) => {
        return new Promise((resolve, reject) => {
            // console.log('productInfo',productInfo)
            var updateData = {
                product_id:productInfo.id,
            }
            if(self.userInfo.userType == 'user'){
                updateData.user_id = self.userInfo.userId
            }
            if(self.userInfo.userType == 'guest'){
                updateData.guest_id = self.userInfo.userId
            }
            var uData = {
                where : updateData,
                values : {quantity : productInfo.quantity}
            }
            Promise.all([
                querys.findByMultyNamePromise('product',{id:productInfo.id},['price','price_before_sale']),
                querys.updatePromise('cart',uData)
            ])

                .then(function ([product,data]) {
                    resolve( product )
                })
                .catch(function (error) {
                    console.log('error',error)
                })
        })
    }

    this.updateCart = function (productInfo) {
        return new Promise((resolve, reject) => {
            // console.log('productInfo',productInfo)
            var uData = {
                where : {id:productInfo.id},
                values : {quantity : productInfo.quantity}
            }
            querys.updatePromise('cart',uData)
                .then(function (data) {
                    resolve( data )
                })
                .catch(function (error) {
                    console.log('error',error)
                })
        })
    }

    this.checkAvailability = function (productInfo) {
        return new Promise((resolve, reject) => {
            // console.log('productId',productInfo.productId)
            var params = { product_id:productInfo.productId}
            var params2V = {
                product_id:productInfo.productId,
                val : self.userInfo.userId
            }
            if(self.userInfo.userType == 'user'){
                params.user_id = self.userInfo.userId
                params2V.field = 'user_id'
            }
            if(self.userInfo.userType == 'guest'){
                params.guest_id = self.userInfo.userId
                params2V.field = 'guest_id'
            }
            Promise.all([
                productQuery.checkProductInStock(params2V.product_id),
                cartQuery.findCartItems(params2V,1)
            ])
                .then( ( [product,data] )=> {
                    if(product[0].in_stock == '0') {
                        // console.log('product IFFFFFFFFFFFFproduct', product)
                        // console.log('BEFORE IFFFFFFFFFFFFdata', data)
                        if (data.length > 0) {
                            // update
                            // console.log('update...update....update...update....update...update....', data)
                            self.updateCart({
                                id: data[0].id,
                                quantity: parseInt(productInfo.quantity)

                            })
                                .then(function (result) {
                                    self.getCartInfo()
                                        .then(function (datass) {
                                            // console.log('datass', datass)
                                            resolve(datass)
                                        })
                                        .catch(function (datass) {
                                            resolve(datass)
                                        })
                                })
                                .catch(function (error) {
                                    console.log('error updateCart', error)
                                })
                        } else {
                            // insert
                            // console.log('insert...insert....insert...insert....insert...insert....', data)
                            self.addToCart({productId: productInfo.productId, quantity: productInfo.quantity})
                                .then(function (result) {
                                    self.getCartInfo()
                                        .then(function (datass) {
                                            resolve(datass)
                                        })
                                        .catch(function (datass) {
                                            resolve(datass)
                                        })
                                })
                                .catch(function (error) {
                                    console.log('error addToCart', error)
                                })
                        }
                    }else{
                        reject({error:true,msg:"NoInStock"})
                    }
                })
                .catch((error)=>{
                    console.log('error findByMultyNamePromise',error)
                })
        })
    }


    this.removeFromCart = function (productInfo) {
        return new Promise((resolve, reject) => {
            var reqs = [];
            // console.log('productId',productInfo.productId)
            if(self.userInfo.userType == 'user'){
                reqs.push(querys.deletesPromise('cart',{user_id : self.userInfo.userId,product_id : productInfo.productId}))
                // params.user_id = self.userInfo.userId
            }
            if(self.userInfo.userType == 'guest'){
                reqs.push(querys.deletesPromise('cart',{guest_id : self.userInfo.userId,product_id : productInfo.productId}))
                // params.guest_id = self.userInfo.userId
            }
            Promise.all(reqs)
                .then(function ([data]){
                    // console.log('data',data)
                    self.getCartInfo()
                        .then(function (datas) {
                            // console.log('datas',datas)
                            resolve( datas )
                        })
                        .catch(function (error) {
                            resolve( [] )
                        })
                })
                .catch(function (error){
                    console.log('error',error)
                })
        })
    }

    this.removeAllFromCart = function () {
        return new Promise((resolve, reject) => {
            var reqs = [];
            if(self.userInfo.userType == 'user'){
                reqs.push(querys.deletesPromise('cart',{user_id : self.userInfo.userId}))
            }
            if(self.userInfo.userType == 'guest'){
                reqs.push(querys.deletesPromise('cart',{guest_id : self.userInfo.userId}))
            }
            Promise.all(reqs)
                .then(function ([data]){
                    self.getCartInfo()
                        .then(function (datas) {
                            // console.log('datas',datas)
                            resolve( datas )
                        })
                        .catch(function (error) {
                            resolve( [] )
                        })
                })
                .catch(function (error){
                    console.log('error',error)
                })
        })
    }


    this.getCartInfo = function (){
        return new Promise((resolve, reject) => {
            //var resultCart = [];
            // console.log('self.langId',self.langId)
            var requests = [];
            if(self.userInfo.userType == 'guest'){
                requests.push(querys.getCartInfoByUserOrGuest({field : 'guest_id',id : self.userInfo.userId},self.langId))
            }else{
                requests.push(querys.getCartInfoByUserOrGuest({field : 'user_id',id : self.userInfo.userId},self.langId))
            }
            Promise.all(requests)
                .then(function ([cart]) {
                    // console.log('11cart...cart...cart...cart..cart',cart)
                    var allCart = [];
                    var allDiscountPrice = 0;
                    var allPrice = 0;
                    var allProdQuantity = 0;
                    var num = 0;
                    cart.forEach(function (item) {
                        if(item.in_stock == '0') {

                            item.selfTotal = staticMethods.formatMoney(parseInt(item.quantity) * parseFloat(item.price))
                            allProdQuantity += parseInt(item.quantity)
                            allPrice += parseInt(item.quantity) * parseInt(item.price)
                            if (item.price_before_sale !== null) {
                                allDiscountPrice = allDiscountPrice + (parseInt(item.quantity) * parseInt(item.price_before_sale))
                            } else {
                                allDiscountPrice = allDiscountPrice + (parseInt(item.quantity) * parseInt(item.price))
                            }
                            item.price = staticMethods.formatMoney(item.price);
                            item.price_before_sale = item.price_before_sale === null ? null : staticMethods.formatMoney(item.price_before_sale)
                            item.item_total_price = parseFloat(item.price) * parseInt(item.quantity)
                            // price_before_sale
                            allCart.push(item)
                            num++;
                        }

                    })
                    var obj = {
                        items : cart,
                        allProdQuantity : allProdQuantity,
                        total : staticMethods.formatMoney(allPrice),
                        beforeDiscount : allDiscountPrice === 0 ? null : staticMethods.formatMoney(allDiscountPrice)
                    };

                    //resultCart.push(obj)
                    resolve(obj);
                })
                .catch(function (error) {
                    console.log('error',error)
                    logs.insertLog("errorCode = 00999 "+ JSON.stringify(error)+', date = '+new Date(),"./logs/v1/all.txt");
                    resolve(obj)
                    resolve([]);
                })
        })
    }

    this.getCartCheckoutInfo = function (){
        return new Promise((resolve, reject) => {
            //var resultCart = [];
            // console.log('self.langId',self.langId)
            var requests = [];
            if(self.userInfo.userType == 'guest'){
                requests.push(querys.getCartCheckoutInfoByUserOrGuest({field : 'guest_id',id : self.userInfo.userId},self.langId))
            }else{
                requests.push(querys.getCartCheckoutInfoByUserOrGuest({field : 'user_id',id : self.userInfo.userId},self.langId))
            }
            Promise.all(requests)
                .then(function ([cart]) {
                    // console.log('cart...cart...cart...cart..cart',cart)
                    // var allCart = [];
                    var allDiscountPrice = 0;
                    var allPrice = 0;
                    var allProdQuantity = 0;
                    cart.forEach(function (item) {
                        // if(item.in_stock == '0'){

                            item.selfTotal = staticMethods.formatMoney(parseInt(item.quantity) * parseFloat(item.price))
                            allProdQuantity+= parseInt(item.quantity)
                            allPrice+= parseInt(item.quantity) * parseInt(item.price)
                            if(item.price_before_sale !== null){
                                allDiscountPrice = allDiscountPrice + (parseInt(item.quantity) * parseInt(item.price_before_sale))
                            }else{
                                allDiscountPrice = allDiscountPrice + (parseInt(item.quantity) * parseInt(item.price))
                            }
                            item.price = staticMethods.formatMoney(item.price);
                            item.price_before_sale = item.price_before_sale === null ? null : staticMethods.formatMoney(item.price_before_sale)
                            item.item_total_price = parseFloat(item.price) * parseInt(item.quantity)
                            // price_before_sale
                            // allCart.push(item)
                        // }


                    })
                    var obj = {
                        items : cart,
                        allProdQuantity : allProdQuantity,
                        total : staticMethods.formatMoney(allPrice),
                        beforeDiscount : allDiscountPrice === 0 ? null : staticMethods.formatMoney(allDiscountPrice)
                    };

                    //resultCart.push(obj)
                    resolve(obj);
                })
                .catch(function (error) {
                    console.log('error',error)
                    logs.insertLog("errorCode = 00999 "+ JSON.stringify(error)+', date = '+new Date(),"./logs/v1/all.txt");
                    resolve(obj)
                    resolve([]);
                })
        })
    }
}



module.exports = Cart;