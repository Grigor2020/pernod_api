let sha1 = require('sha1')
let querys = require('../../model/dbQuerys/querys');
let filterQuery = require('../../model/dbQuerys/filter/filter');
let logs = require('../../logs/log')
let sendMail = require('../../mailer/v1/sendmail')


function Filter (langId) {

    this.langId = typeof langId === "undefined" ? null : langId

    this.result = {}

    const self = this;


    this.getGropedFilter = function (filters) {
        return new Promise(function (resolve, reject) {
            if(filters.length === 0){
                resolve([])
            }else{
                filterQuery.getGrouped(filters)
                    .then(function (data) {
                        // console.log('data',data)
                        var arr = [];
                        for(let i = 0; i < data.length; i++){
                            let arr2 = []
                            let sSpl = data[i].fGroup.split('||')

                            for(let j = 0; j < sSpl.length; j++){
                                arr2.push(sSpl[j])
                            }
                            arr.push(arr2)
                        }
                        resolve(arr)
                    })
                    .catch(function (error) {
                        console.log('error',error)
                    })
            }

        });
    }

    this.getAllFilters = function () {
        return new Promise(function (resolve, reject) {
            filterQuery.getAllFilters(self.langId)
                .then(function (data) {
                    var mainArr = [];

                    for(let i = 0; i < data.length; i++){
                        var arr = {
                            filterName : data[i].filterName,
                            filters : []
                        };
                        var splitedIds = data[i].filterPropId.split('||');
                        var splitedNames = data[i].filterPropName.split('||');
                        // console.log('splitedIds',splitedIds)
                        for(let j = 0;  j < splitedIds.length; j++){
                            arr.filters.push({
                                filterPropId : splitedIds[j],
                                filterPropName : splitedNames[j]
                            })
                        }
                        mainArr.push(arr)
                    }

                    resolve(mainArr)
                })
                .catch(function (error) {
                    console.log('error',error)
                })
        });
    }

    this.getFiltersByProductIds = function (selectedProducts) {
        return new Promise(function (resolve, reject) {
            filterQuery.getAllFiltersByProdIds(selectedProducts,self.langId)
                .then(function (data) {
                    // console.log('data',data)
                    var mainArr = [];
                    for(let i = 0; i < data.length; i++){
                        var splitedIds = data[i].filterPropId.split('||');
                        for(let j = 0; j < splitedIds.length; j++){
                            var x = mainArr.indexOf(parseInt(splitedIds[j]))
                            if(x === -1) {
                                mainArr.push(parseInt(splitedIds[j]))
                            }
                        }
                        var splitedBrands = data[i].brandSlug.split('||');
                        for(let j = 0; j < splitedBrands.length; j++){
                            var x = mainArr.indexOf(splitedBrands[j])
                            if(x === -1) {
                                mainArr.push(splitedBrands[j])
                            }
                        }
                        // var splitedCats = data[i].categoriesSlug.split('||');
                        // for(let j = 0; j < splitedCats.length; j++){
                        //     var x = mainArr.indexOf(splitedCats[j])
                        //     if(x === -1) {
                        //         mainArr.push(splitedCats[j])
                        //     }
                        // }
                    }
                    // console.log('mainArr',mainArr)
                    resolve(mainArr)
                })
                .catch(function (error) {
                    console.log('error',error)
                })
        });
    }
    this.getFIlterPropNames = function (avFilterProps) {
        return new Promise(function (resolve, reject) {
            console.log('avFilterProps',avFilterProps)
            filterQuery.getAllFiltersByPropIds(avFilterProps,self.langId)
            // filterQuery.getAllFiltersByPropIds([ 2, 16, 17],self.langId)
                .then(function (data) {
                    var mainArr = [];
                    // console.log('data',data)
                    for(let i = 0; i < data.length; i++){
                        var arr = {
                            filterName : data[i].filterName,
                            filters : []
                        };
                        var splitedIds = data[i].filterPropId.split('||');
                        var splitedNames = data[i].filterPropName.split('||');
                        // console.log('splitedIds',splitedIds)
                        for(let j = 0;  j < splitedIds.length; j++){
                            arr.filters.push({
                                filterPropId : splitedIds[j],
                                filterPropName : splitedNames[j]
                            })
                        }
                        mainArr.push(arr)
                    }

                    // console.log('mainArr',mainArr)
                    resolve(mainArr)
                })
                .catch(function (error) {
                    console.log('error',error)
                })
        });
    }

}

module.exports = Filter;