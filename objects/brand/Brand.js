let brandQuery = require('../../model/dbQuerys/brand/brand');

function Brand (langId) {

    this.langId = typeof langId === "undefined" ? 2 : langId
    this.brandId = null

    this.result = {}

    const self = this;


    this.defineBrandId = (brandSlug) => {
        return new Promise(function (resolve, reject){
            let slug = brandSlug.split('-');
            if(slug.length == 1){
                brandQuery.findBrandIdByslug(slug[0])
                    .then(function (data) {
                        self.brandId = data[0].id;
                        resolve(data[0].id)
                    })
            }else{
                self.brandId = slug[slug.length - 1];
                resolve(slug[slug.length - 1])
            }
        })
    }

    this.getBrandInfo = function (id) {
        return new Promise((resolve, reject) => {
            console.log('..........id',id)
            brandQuery.findBrand(id,self.langId)
                .then(function (data) {
                    console.log('findBrand',data)
                    resolve(data[0])
                })
                .catch(function (error) {
                    console.log('findBrand',data)
                    resolve([])
                })
        })

    }
}

module.exports = Brand;