let sha1 = require('sha1')
let querys = require('../../model/dbQuerys/querys');
let logs = require('../../logs/log')
let sendMail = require('../../mailer/v1/sendmail')


function Loyalty (data) {
    this.user_id = typeof data.user_id === "undefined" ? null : data.user_id
    this.money = typeof data.money === "undefined" ? null : data.money
    this.status_id = typeof data.status_id === "undefined" ? null : data.status_id

    this.result = {}

    const self = this;



    this.setStatus = function () {
        return new Promise((resolve, reject) => {
            self.getAllLoyality()
                .then(function (data) {
                    let insertPost = {
                        user_id : self.user_id,
                        money : self.money,
                        loyality_status_id : null
                    };
                    // console.log('insertPost',insertPost)
                    // console.log('data',data)
                    if(self.money === 0){
                        for(let i = 0; i < data.result.length; i++){
                            if(data.result[i].from_money === 0){
                                insertPost.loyality_status_id = data.result[i].id
                                break
                            }
                        }
                    }else{
                        for(let i = 0; i < data.result.length; i++){
                            if(self.money >= data.result[i].from_money && self.money <= data.result[i+1].from_money){
                                insertPost.loyality_status_id = data.result[i].id
                                break
                            }
                        }
                    }
                    // console.log('insertPost',insertPost)
                    querys.insert2vPromise('user_loyality',insertPost)
                        .then(function (data) {
                            resolve ({error : false, errMsg : ""})
                        })
                        .catch(function (error) {
                            resolve ({error : true,errMsg : "loyalti error"})
                        })
                })
        })
    }

    this.getAllLoyality = function () {
        let obj = {
            error : false,
            errorText : "",
            result : {}
        }
        return new Promise((resolve) => {
            querys.findAllPromise('loyality_status')
                .then(function (data) {
                    // console.log('..........getAllLoyality data',data)
                    obj.result = data
                    resolve (obj)
                })
                .catch(function (error) {
                    obj.error = true;
                    obj.errorText = 'getAllLoyality SQL';
                    logs.insertLog("errorCode = 00151 "+ JSON.stringify(error)+', date = '+new Date(),"./logs/v1/loyalty.txt");
                    resolve (obj)
                })
        })
    }
}

module.exports = Loyalty;