const Rectangle = class {
    constructor(height, width) {
        this.height = height;
        this.width = width;
    }

    area() {
        return this.height * this.width;
    }
};

console.log(new Rectangle(6, 10).area());