let sha1 = require('sha1')
let querys = require('../../model/dbQuerys/querys');
let catalogQuery = require('../../model/dbQuerys/catalog/catalog');
let logs = require('../../logs/log')
let sendMail = require('../../mailer/v1/sendmail')
let productQuery = require('../../model/dbQuerys/product/product');
let staticMethods = require('../../model/staticMethods');
var Filter = require('../filters/Filter')

function Catalog (langId,page,info,userInfo) {

    this.userInfo = typeof userInfo === "undefined" ? null : userInfo

    this.price = {
        min: typeof info.price !== 'undefined' ? info.price.min : null,
        max: typeof info.price !== 'undefined' ? info.price.max : null
    }
    this.cat  = typeof info.cat !== 'undefined' ? info.cat : null
    this.brand  = typeof info.brand !== 'undefined' ? info.brand : null
    this.litrage  = typeof info.litrage !== 'undefined' ? info.litrage : null
    this.filters  = typeof info.filters !== 'undefined' ? info.filters : null
    this.langId = typeof langId === "undefined" ? null : langId
    this.page = typeof page === "undefined" ? null : page

    this.result = {}

    const self = this;


    this.defineQueryUserType = () =>{
        var queryUserInfo = {};
        if(this.userInfo.userType == 'guest'){
            queryUserInfo.field = 'guest_id';
            queryUserInfo.val = this.userInfo.userId;
        }else{
            queryUserInfo.field = 'user_id';
            queryUserInfo.val = this.userInfo.userId;
        }
        return queryUserInfo
    }


    this.getCatalog = async () => {
        var defineQueryUserType = await this.defineQueryUserType();
        return new Promise((resolve, reject) => {
            catalogQuery.getAllCatalog(self.price,self.langId,self.page,defineQueryUserType)
                .then(function (data) {
                    resolve (data)
                })
                .catch(function (error) {
                    resolve ([])
                })
        })

    }

    this.getCatalogByFilters = async () => {
        var defineQueryUserType = await this.defineQueryUserType();
        return new Promise((resolve, reject) => {
            // console.log('price',self.price)
            // console.log('langId',self.langId)
            var mFilter = new Filter(self.langId);
                //
                // console.log('self.page',self.page)
                // console.log('self.cat',self.cat)
                // console.log('self.brand',self.brand)
                // console.log('self.litrage',self.litrage)
                // console.log('self.price',self.price)
                // console.log('self.langId',self.langId)
            mFilter.getGropedFilter(self.filters)
                .then((resGropued) => {
                    console.log('resGropued',resGropued)
                    catalogQuery.findCatalog2V(
                        self.page,
                        self.cat,
                        self.brand,
                        self.litrage,
                        resGropued,
                        self.price,
                        self.langId,
                        defineQueryUserType
                    )
                        .then(function (selectedProducts) {
                            if(selectedProducts.length !== 0){
                                var productIds = [];
                                for(var i = 0 ;  i< selectedProducts.length; i++){
                                    productIds.push(selectedProducts[i].product_id)
                                }

                                var mFilter = new Filter(self.langId);

                                Promise.all([
                                    // productQuery.getMultipleProducts(self.page,self.langId,productIds),
                                    mFilter.getFiltersByProductIds(productIds)
                                ])
                                    .then(([filters])=>{

                                        // mFilter.getFIlterPropNames(filters)
                                        //     .then((allFilter)=>{
                                                resolve({
                                                    filters : filters,
                                                    products : selectedProducts,
                                                })
                                            // })
                                            // .catch((error)=>{
                                            //     console.log('error', error)
                                            // })

                                    })
                                    .catch((error)=>{
                                        console.log('error', error)
                                        resolve({
                                            filters : [],
                                            products : [],
                                        })
                                    })
                            }else{
                                resolve({
                                    filters : [],
                                    products : [],
                                })
                            }
                        })
                        .catch(function (error) {

                        })
                })
                .catch((error) => {
                    console.log('error', error)
                })

        })
    }

    this.getCatalogWithoutFilters = async () => {
        var defineQueryUserType = await this.defineQueryUserType();
        return new Promise((resolve, reject) => {
            // console.log('price',self.price)
            // console.log('langId',self.langId)
                //
                console.log('::self.page',self.page)
                console.log('::self.cat',self.cat)
                console.log('::self.brand',self.brand)
                console.log('::self.litrage',self.litrage)
                console.log('::self.price',self.price)
                console.log('::self.langId',self.langId)

                    catalogQuery.findCatalogWithoutFilters(
                        self.page,
                        self.cat,
                        self.brand,
                        self.litrage,
                        self.price,
                        self.langId,
                        defineQueryUserType
                    )
                        .then(function (selectedProducts) {
                            if(selectedProducts.length !== 0){
                                resolve({
                                    filters : [],
                                    products : selectedProducts,
                                })
                            }else{
                                resolve({
                                    filters : [],
                                    products : [],
                                })
                            }
                        })
                        .catch(function (error) {
                            resolve({
                                filters : [],
                                products : [],
                            })
                        })
        })
    }

    this.checkFilterObj = () => {
        var obj = {
            cat : self.cat,
            brand : self.brand,
            litrage : []
        }
        // console.log('self.cat',self.cat)
        // for(let i = 0; i < self.cat.length; i++ ){
        //     let spl = self.cat[i].split('-')
        //     obj.cat.push(spl[spl.length - 1])
        // }
        // for(let i = 0; i < self.brand.length; i++ ){
        //     let spl = self.brand[i].split('-')
        //     obj.brand.push(spl[spl.length - 1])
        // }
        for(let i = 0; i < self.litrage.length; i++ ){
            let spl = self.litrage[i].split('-')
            obj.litrage.push(spl[spl.length - 1])
        }
        // self.cat = obj.cat
        // self.brand = obj.brand
        self.litrage = obj.litrage
        return true
    }

    this.checkPagesAndCount = () => {
        return new Promise((resolve, reject) => {
            productQuery.getAllProductCount()
                .then(function (productCount) {
                    // console.log('productCount............................',productCount)
                    // console.log('page............................',staticMethods.pageProductCount)
                    var pagesCount = parseInt(productCount) / staticMethods.pageProductCount;
                    // console.log('pagesCount............................',pagesCount)
                    resolve(Math.ceil(pagesCount))
                })
                .catch(function (error) {
                    console.log('error',error)
                })
        })
    }
}

module.exports = Catalog;