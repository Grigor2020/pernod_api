let sha1 = require('sha1')
let querys = require('../../model/dbQuerys/querys');
let productQuery = require('../../model/dbQuerys/product/product');
let logs = require('../../logs/log')
let sendMail = require('../../mailer/v1/sendmail')
let staticMethods = require('../../model/staticMethods');

function Product (langId,userInfo) {

    this.userInfo = typeof userInfo === "undefined" ? null : userInfo
    this.langId = typeof langId === "undefined" ? 2 : langId
    this.productId = null


    const self = this;

    this.defineQueryUserType = () =>{
        // console.log('this.userInfo',this.userInfo)
        var queryUserInfo = {};
        if(this.userInfo.userType == 'guest'){
            queryUserInfo.field = 'guest_id';
            queryUserInfo.val = this.userInfo.userId;
        }else{
            queryUserInfo.field = 'user_id';
            queryUserInfo.val = this.userInfo.userId;
        }
        // console.log('queryUserInfo',queryUserInfo)
        return queryUserInfo
    }

    this.defineProductId = (slug) => {
        let sSlug = slug.split('-');
        self.productId = sSlug[sSlug.length - 1];
    }


    this.getProductsByBrandId = async (brandId) => {
        var defineQueryUserType = await this.defineQueryUserType();
        return new Promise((resolve, reject) => {
            // console.log('brandId',brandId)
            productQuery.getBroductByBrandId(this.langId,brandId,defineQueryUserType)
                .then((data)=>{
                    resolve(data)
                })
                .catch((error)=>{
                    console.log('error.............',error)
                })
        })
    }

    this.search = async (vals) => {
        var defineQueryUserType = await this.defineQueryUserType();
        return new Promise((resolve, reject) => {
            productQuery.getSearch(vals,defineQueryUserType)
                .then((data)=>{
                    for(var i = 0; i < data.length; i++){
                        if(data[i].price_before_sale !== null){
                            data[i].price_before_sale = staticMethods.formatMoney(data[i].price_before_sale);
                        }
                        data[i].price = staticMethods.formatMoney(data[i].price);
                    }
                    resolve(data)
                })
                .catch((error)=>{
                    console.log('error.............',error)
                })
        })
    }

    this.getProductMinAndMaxPrices = () => {
        return new Promise((resolve, reject) => {
            productQuery.findMinMaxPrice()
                .then((data)=>{
                    var prices = {
                        min_price : 0,
                        max_price : Math.ceil(data.max_price/1000)*1000
                    }
                    resolve(prices)
                })
                .catch((error)=>{
                    console.log('error.............',error)
                })
        })
    }


    this.getProductById = async () => {
        var defineQueryUserType = await this.defineQueryUserType();
        return new Promise((resolve, reject) => {
            productQuery.getOneProduct(self.productId,self.langId,defineQueryUserType)
                .then((data)=>{
                    // console.log('getProductById.............',data)
                    resolve(data)
                })
                .catch((error)=>{
                    console.log('error.............',error)
                })
        })
    }

    this.getRelativeProducts = async (catId) => {
        var defineQueryUserType = await this.defineQueryUserType();
        return new Promise((resolve, reject) => {
            productQuery.getRelativeProducts(catId,self.productId,self.langId,defineQueryUserType)
                .then((data)=>{
                    if(data.length < 5){
                        let selectableCount = 5 - data.length;
                        let notSelectableProducts = []
                        data.forEach(function (item) { notSelectableProducts.push(item.id) })
                        productQuery.getAlternativeRelativeProducts(self.langId,selectableCount,notSelectableProducts,defineQueryUserType)
                            .then(function (alternativeRelatedProducts) {
                                var result = data.concat(alternativeRelatedProducts)
                                resolve(result)
                            })
                            .catch(function (error) {
                                console.log('error.............',error)
                            })
                    }else{
                        resolve(data)
                    }
                })
                .catch((error)=>{
                    console.log('error.............',error)
                })
        })
    }

    this.getProductByIds = async (ids) => {
        return new Promise((resolve, reject) => {
            productQuery.getProductsByIds(self.langId,ids)
                .then((data)=>{
                    resolve(data)
                })
                .catch((error)=>{
                    console.log('error.............',error)
                })
        })
    }

    this.getProductFilters = () => {
        return new Promise((resolve, reject) => {
            productQuery.getOneProductFilters(self.productId,self.langId)
                .then((data)=>{
                    // console.log('getProductFilters.............',data)
                    resolve(data)
                })
                .catch((error)=>{
                    console.log('error.............',error)
                })
        })
    }

    this.getProductAdditionalFilters = () => {
        return new Promise((resolve, reject) => {
            productQuery.getOneProductAddFilters(self.productId,self.langId)
                .then((data)=>{
                    // console.log('getProductAdditionalFilters.............',data)
                    resolve(data)
                })
                .catch((error)=>{
                    console.log('error.............',error)
                })
        })

    }

    this.getPopular = async () => {
        var defineQueryUserType = await this.defineQueryUserType();
        return new Promise((resolve, reject) => {
            productQuery.getPopularProduct(self.langId,defineQueryUserType)
                .then((data)=>{
                    // console.log('getPopularProduct.............',data)
                    for(var i = 0; i < data.length; i++){
                        // if(data[i].price_before_sale !== null){
                        //     data[i].price_before_sale = staticMethods.formatMoney(data[i].price_before_sale);
                        // }
                        // data[i].price = staticMethods.formatMoney(data[i].price);
                    }
                    // console.log('.............getPopularProduct',data)
                    resolve(data)
                })
                .catch((error)=>{
                    console.log('error.............',error)
                })
        })

    }

    this.getProductInShopSection = async () => {
        return new Promise((resolve, reject) => {
            productQuery.getProductInShopSection(self.langId)
                .then((data)=>{
                    resolve(data)
                })
                .catch((error)=>{
                    console.log('error.............',error)
                })
        })

    }

    this.getSpecialOffers = async () => {
        var defineQueryUserType = await this.defineQueryUserType();
        return new Promise((resolve, reject) => {
            productQuery.getSpecialOffers(self.langId,defineQueryUserType)
                .then((data)=>{
                    // console.log('getPopularProduct.............',data)
                    resolve(data)
                })
                .catch((error)=>{
                    console.log('error.............',error)
                })
        })

    }

}

module.exports = Product;