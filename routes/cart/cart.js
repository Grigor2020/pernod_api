var express = require('express');
var router = express.Router();
var Cart = require('../../objects/cart/Cart')
var checkRequestUser = require('../checkRequestUser');

router.post('/all',checkRequestUser.checkUserOrGuest, function(req, res, next) {
    let userInfo = {userType :req.user_type,userId : req.user_id, userStatus : req.user_status};
    var mCart = new Cart(userInfo, req.body.lang_id);

    mCart.getCartInfo()
        .then(function (data) {
            // console.log('getCartInfo data',data)
            res.json({error : false,data:data})
        })
        .catch((error)=>{
            console.log('error checkAvailability',error)
        })
});
router.post('/add',checkRequestUser.checkUserOrGuest, function(req, res, next) {
    // console.log('req.body...........',req.body)
    let userInfo = {userType :req.user_type,userId : req.user_id, userStatus : req.user_status};
    var mCart = new Cart(userInfo, req.body.lang_id);

    mCart.checkAvailability(
        {productId : req.body.productId,quantity : req.body.quantity}
    )
        .then(function (data) {
            res.json({error : false,data:data})
        })
        .catch((error)=>{
            res.json(error)
        })
});

router.post('/remove',checkRequestUser.checkUserOrGuest, function(req, res, next) {
    let userInfo = {userType :req.user_type,userId : req.user_id, userStatus : req.user_status};
    var mCart = new Cart(userInfo, req.body.lang);
    mCart.removeFromCart({productId : req.body.productId})
        .then((data)=>{
            res.json({error : false,data:data})
        })
        .catch((error)=>{
            console.log('error',error)
        })
});

router.post('/remove-all',checkRequestUser.checkUserOrGuest, function(req, res, next) {
    let userInfo = {userType :req.user_type,userId : req.user_id, userStatus : req.user_status};
    var mCart = new Cart(userInfo, req.body.lang);
    mCart.removeAllFromCart()
        .then((data)=>{
            res.json({error : false,data:data})
        })
        .catch((error)=>{
            console.log('error',error)
        })
});

router.post('/update-quantity',checkRequestUser.checkUserOrGuest, function(req, res, next) {
    let userInfo = {userType :req.user_type,userId : req.user_id, userStatus : req.user_status};
    var mCart = new Cart(userInfo);
    let productInfo = {
        id : req.body.productId,
        quantity : parseInt(req.body.quantity)
    }
    mCart.updateCartCount(productInfo)
        .then(function (product) {
            res.json({error : false,data : product})
        })
        .catch(function (error) {
            console.log('error',error)
        })
});


module.exports = router;
