var query2v = require('../model/dbQuerys/querys')

function checkUserOrGuest(req, res, next) {
    var userToken = "";
    var guestToken = "";
    if(typeof req.headers['void'] !== 'undefined') {
        userToken = req.headers['void'];
    }

    if(typeof req.headers['guest'] !== 'undefined') {
        guestToken = req.headers['guest'];
    }
    if(userToken != "" || guestToken != "") {
        var allrequest = [query2v.findByToken(userToken), query2v.findByGuestToken(guestToken)];
        return Promise.all(allrequest)
            .then(function ([user, guest]) {
                if (user.length == 1) {
                    req.user_id = user[0]['id'];
                    req.user_status = user[0]['status'];
                    req.user_type = "user";
                    next();
                } else if (guest.length == 1) {
                    req.user_id = guest[0]['id'];
                    req.user_status = null;
                    req.user_type = "guest";
                    next();
                } else {
                    res.json({error: true, msg: "104", data: null});
                    res.end();
                }
            })
            .catch(function (error) {
                res.json({error: true,msgText:error.toString(), msg: "9999-2", text:"checkUserOrGuest", data: null});
                res.end();
            })
    }else{
        res.json({error: true, msg: "9999-2", text:"GG checkUserOrGuest token", data: null});
        res.end();
    }
}

function checkUser(req, res, next) {
    var userToken = "";
    if(typeof req.headers['void'] !== 'undefined') {
        userToken = req.headers['void'];
    }
    if(userToken != "") {
        query2v.findByToken(userToken)
            .then(function (user) {
                if (user.length == 1) {
                    req.user_id = user[0]['id'];
                    req.user_status = user[0]['user_status'];
                    next();
                } else {
                    res.json({error: true, msg: "104", data: null});
                    res.end();
                }
            })
            .catch(function (error) {
                res.json({error: true, msg: "9999", text:"checkUser", data: null});
                res.end();
            })
    }else{
        res.json({error: true, msg: "9999", text:"checkUserOrGuest token", data: null});
        res.end();
    }
}

module.exports.checkUserOrGuest = checkUserOrGuest;
module.exports.checkUser = checkUser;
