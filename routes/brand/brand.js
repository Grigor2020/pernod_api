var express = require('express');
var router = express.Router();
var Brand = require('../../objects/brand/Brand')
var Product = require('../../objects/product/Product')
var checkRequestUser = require('../checkRequestUser')

router.post('/getOneBrand',checkRequestUser.checkUserOrGuest, function(req, res, next) {
    let userInfo = {userType :req.user_type,userId : req.user_id, userStatus : req.user_status};
    var mBrand = new Brand(req.body.langId);
    var mProduct = new Product(req.body.langId,userInfo);
    // console.log('req.body.brand',req.body.brand)
    // mBrand.defineBrandId(req.body.brand)
    //     .then(function (datas) {
    //         console.log('..........datas',datas)
            mBrand.getBrandInfo(req.body.brand)
                .then(function (data) {
                    // console.log('datadatadatadatadatadatadatadatadata',data)
                    mProduct.getProductsByBrandId(data.id)
                        .then(function (products) {
                            res.json({error : false,data : {brand : data,products : products}})
                        })
                        .catch(function (error) {
                            console.log('error 3',error)
                            res.json({error:true, msg:"3333"})
                        })
                })
                .catch(function (error) {
                    console.log('error 2',error)
                    res.json({error:true, msg:"222"})
                })
        // })
        // .catch(function (error) {
        //     console.log('error 1',error)
        //     res.json({error:true, msg:"111"})
        // })


});


module.exports = router;
