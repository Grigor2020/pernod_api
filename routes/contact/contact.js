var express = require('express');
var router = express.Router();
var checkRequestUser = require('../checkRequestUser');
var Catalog = require('../../objects/catalog/Catalog')
var Product = require('../../objects/product/Product')
var querys = require('../../model/dbQuerys/querys')
let sendMail = require('../../mailer/v1/sendmail')

router.post('/msg', function(req, res, next) {
    if
    (
        req.body.name !== "" &&
        req.body.email !== "" &&
        req.body.subject !== "" &&
        req.body.message !== ""
    ){
        var insertInfo = {
            name : req.body.name,
            email : req.body.email,
            subject : req.body.subject,
            message : req.body.message
        }
        querys.insert2vPromise('contact',insertInfo)
            .then(function () {
                querys.findByMultyNamePromise('settings',{id:1},['admin_email'])
                    .then(function (data) {
                        var admin_email = data[0].admin_email;
                        Promise.all([
                            sendMail.sendContactMessageToAdmin(admin_email,insertInfo),
                            sendMail.sendContactMessageToUser(insertInfo)
                        ])
                            .then(function () {
                                res.json({error:false})
                            })
                            .catch(function () {
                                res.json({error:true})
                            })
                    })
                    .catch(function (eror) {
                        res.json({error:true})
                    })
            })
            .catch(function () {
                res.json({error:true})
            })
    }else {
        res.json({error:true})
    }
});

module.exports = router;
