var express = require('express');
var router = express.Router();
var querys = require('../../model/dbQuerys/querys');
let logs = require('../../logs/log')
var statics = require('../../static');
var Product = require('../../objects/product/Product')
var checkRequestUser = require('../checkRequestUser');

router.post('/get-one-product',checkRequestUser.checkUserOrGuest, function(req, res, next) {
    let userInfo = {userType :req.user_type,userId : req.user_id, userStatus : req.user_status};
    const slug = req.body.slug;
    var mProduct = new Product(req.body.langId,userInfo);
    mProduct.defineProductId(slug);
    Promise.all([
        mProduct.getProductById(),
        mProduct.getProductFilters(),
        mProduct.getProductAdditionalFilters()
    ])
        .then(([data,staticFilters,addFilters])=>{
            mProduct.getRelativeProducts(data.catId)
                .then(function (relativeProduct) {
                    res.json({error : false,data : {product : data,staticFilters : staticFilters,addFilters : addFilters,relativeProduct : relativeProduct}})
                })
                .catch(function () {
                    res.json({error : true,data : {}})
                })
        })
        .catch((error)=>{
            res.json({error : true,data : {}})
        })

    // console.log('mProduct.productId',mProduct.productId)

});

router.get('/get-popular/:langId',checkRequestUser.checkUserOrGuest, function(req, res, next) {
    // console.log('.......')
    let userInfo = {userType :req.user_type,userId : req.user_id, userStatus : req.user_status};
    var mProduct = new Product(req.params.langId,userInfo);
    Promise.all([
        mProduct.getProductInShopSection(),
        mProduct.getPopular()
    ])
        .then(([ssProduct,data])=>{
            // console.log('ssProduct',ssProduct)
            res.json({error : false,data : {popularProducts : data, ssProduct:ssProduct}})
        })
        .catch((error)=>{
            res.json({error : false,data : []})
        })

    // console.log('mProduct.productId',mProduct.productId)

});

router.post('/search',checkRequestUser.checkUserOrGuest, function(req, res, next) {
    var langId = 2
    if(typeof req.body.langId !== "undefined"){
        langId = req.body.langId
    }
    let userInfo = {userType :req.user_type,userId : req.user_id, userStatus : req.user_status};
    let vals = req.body.vals;
    var mProduct = new Product(langId,userInfo);
    Promise.all([
        mProduct.search(vals)
    ])
        .then(([products])=>{
            res.json({error : false,data : products})
        })
        .catch((error)=>{
            res.json({error : false,data : []})
        })

    // console.log('mProduct.productId',mProduct.productId)

});


module.exports = router;
