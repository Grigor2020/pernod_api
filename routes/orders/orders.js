var express = require('express');
var router = express.Router();
var Cart = require('../../objects/cart/Cart')
var Orders = require('../../objects/orders/Orders')
var querys = require('../../model/dbQuerys/querys')
var ordersQuerys = require('../../model/dbQuerys/orders/orders')
var checkRequestUser = require('../checkRequestUser');
let productQuery = require('../../model/dbQuerys/product/product');

router.post('/create',checkRequestUser.checkUserOrGuest, function(req, res, next) {
    let userInfo = {userType :req.user_type,userId : req.user_id, userStatus : req.user_status};
    var mOrders = new Orders(userInfo, 2);
    var mCart = new Cart(userInfo, 2);

    mCart.getCartCheckoutInfo()
        .then(function (cart) {
            var promiseQuery = [];
            if(userInfo.userType == 'guest'){
                promiseQuery.push(mOrders.createOrderForGuest(req.body,cart.items))
            }else{
                promiseQuery.push(mOrders.createOrderForUser(req.body,cart.items))
            }
            Promise.all(promiseQuery)
                .then(function ([data]) {
                    // console.log('data',data)
                    res.json(data)
                })
                .catch(function (error){
                    console.log('............. error promiseQuery',error)
                    res.json({error :true})
                })

        })
        .catch(function (error) {
            console.log('.............cart error',error)
        })
});

router.post('/add-payment-init-response',checkRequestUser.checkUserOrGuest, function(req, res, next) {
    let userInfo = {userType :req.user_type,userId : req.user_id, userStatus : req.user_status};
    var mOrders = new Orders(userInfo, 2);
    mOrders.addPaymentInitResponse(req.body)
        .then(function (data) {
            res.json(data)
        })
        .catch(function (error) {
            res.json(error)
        })
});
router.post('/add-payment-details',checkRequestUser.checkUserOrGuest, function(req, res, next) {
    let userInfo = {userType :req.user_type,userId : req.user_id, userStatus : req.user_status};
    var mOrders = new Orders(userInfo, 2);
    mOrders.addPaymentDetails(req.body)
        .then(function (data) {
            res.json(data)
        })
        .catch(function (error) {
            res.json(error)
        })
});


router.post('/get-order-by-public-id',checkRequestUser.checkUserOrGuest, function(req, res, next) {
    let userInfo = {userType :req.user_type,userId : req.user_id, userStatus : req.user_status};
    var mOrders = new Orders(userInfo, 2);
    mOrders.getOrderByPublicId(req.body)
        .then(function (data) {
            // console.log('asdadsadsadsa',data)
            res.json({error:false,data:data})
        })
        .catch(function (error) {
            res.json({error:true,data:{}})
        })
});



module.exports = router;
