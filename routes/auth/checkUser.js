var express = require('express');
var router = express.Router();
var querys2v = require('../../model/dbQuerys/querys');
var checkRequestUser = require('../checkRequestUser');
var log = require("../../logs/log");
var static = require('../../static');

router.post('/',checkRequestUser.checkUser, function(req, res, next) {
   if(typeof req.user_id != "undefined" && !isNaN(req.user_id)){
       var filds = ['f_name','l_name','email','status','phone','token'];
       querys2v.findByMultyNamePromise("users_db",{id:req.user_id},filds)
           .then(function (user) {
               if (user.length == 1) {
                   res.json({error: false, msg: "1000", data: {user:user}});
                   res.end();
               }else{
                   res.json({error: true, msg: "104", data: null});
                   res.end();
               }
           })
           .catch(function (error) {
               // console.log("9999-1",error)
               var errorLog = {
                   promiseError:error,
                   msg:"9999-1",
                   route:"/checkUser"
               }
               log.insertLog(JSON.stringify(errorLog),"./logs/v1/promiseError.txt");
               res.json({error: true, msg: "9999-1",text:error, data: null});
               res.end();
           })
   }
})

module.exports = router;