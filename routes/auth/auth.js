var express = require('express');
var router = express.Router();
var checkRequestUser = require('../checkRequestUser');
var Auth = require('../../objects/auth/Auth')
var Loyalty = require('../../objects/user/Loyalty')
// var Test = require('../../objects/user/Test')
var Cart = require('../../objects/cart/Cart')

router.post('/sign-up',checkRequestUser.checkUserOrGuest, function(req, res, next) {
    let userInfo = {userType :req.user_type,userId : req.user_id, userStatus : req.user_status};
    let mAuth = new Auth(req.body,userInfo);
    let checkSignUpInformation = mAuth.checkSignUpInformation();
    if(!checkSignUpInformation.error){
        mAuth.checkEmailExists()
            .then(function (data) {
                if(!data.error){
                    mAuth.setToken();
                    mAuth.setHashedPassword();
                    mAuth.signUp()
                        .then(function (data) {
                            // console.log('data,',data)
                            let emailResult = data.result.emailInfo.success;
                            let loyalData = {user_id : data.result.userInfo.id,money :0 }
                            let mLoyalty = new Loyalty(loyalData);
                            mLoyalty.setStatus()
                                .then(function () {
                                    if(!data.error){
                                        //     mAuth.updateFavoriteGuestToUser(data.result.userInfo.id)
                                        //         .then(function (updateFavoriteResult) {
                                        //             if(!updateFavoriteResult.error){
                                        //                 res.json({
                                        //                     error : false,
                                        //                     data : {
                                        //                         user : {
                                        //                             token:data.result.userInfo.token
                                        //                         },
                                        //                         emailError : emailResult
                                        //                     }
                                        //                 })
                                        // res.end();
                                        //             }else{
                                        //                 res.json(data)
                                        //                 res.end();
                                        //             }
                                        //         })
                                        res.json({error:false})
                                    }else{
                                        res.json(data)
                                        res.end();
                                    }
                                })
                                .catch(function () {
                                    res.json({error:true})
                                    res.end();
                                })
                        })
                }else{
                    res.json(data)
                    res.end();
                }
            })
    }else{
        res.json(checkSignUpInformation)
        res.end();
    }
});

router.post('/sign-in',checkRequestUser.checkUserOrGuest, function(req, res, next) {
    let userInfo = {userType :req.user_type,userId : req.user_id, userStatus : req.user_status};
    let mAuth = new Auth(req.body,userInfo);
    let checkSignInInformation = mAuth.checkSignInInformation();
    if(!checkSignInInformation.error){
        mAuth.setHashedPassword();
        mAuth.signIn()
            .then(function (data) {
                if(!data.error){
                    console.log('}}}}}}}}}}}}}}}}}data',data)
                    Promise.all([
                        mAuth.updateFavoriteGuestToUser(data.result.userInfo.id),
                        mAuth.updateCartGuestToUser(data.result.userInfo.id),
                        mAuth.updateUserLastLogin(data.result.userInfo.id)
                    ])
                        .then(function ([updateFavoriteResult,updateCartResult, updateLastLogin]) {
                            if(!updateFavoriteResult.error && !updateCartResult.error){
                                var resData = {user : {
                                        name:data.result.userInfo.name,
                                        last_name:data.result.userInfo.last_name,
                                        email_verify:data.result.userInfo.email_verify,
                                        token:data.result.userInfo.token
                                    }};
                                console.log('{{{{{{{{{{{{{{resData',resData)
                                res.json({error : false,data : resData})
                                res.end();
                            }else{
                                res.json(data)
                                res.end();
                            }
                        })
                        .catch(function (error) {
                            console.log('}}}}}}}}}}}}}}}}}error',error)
                        })
                }else{
                    if(data.errorText === "No Verifyed Email"){
                        res.json({error : false,data : {user : {
                                    email_verify: data.result.userInfo.email_verify
                                }}});
                    }else{
                        res.json(data)
                        res.end();
                    }

                }
            });
    }else{
        res.json(checkSignInInformation)
        res.end();
    }
});

router.post('/check-user',checkRequestUser.checkUserOrGuest, function(req, res, next) {
    let userInfo = {userType :req.user_type,userId : req.user_id, userStatus : req.user_status};
    // console.log('req.user_type',req.user_type)
    // console.log('req.user_id',req.user_id)
    // console.log('req.user_status',req.user_status)
    var langId = '2'
    if(typeof req.body.lang_id !== "undefined" )
        langId = req.body.lang_id
    let mAuth = new Auth({},userInfo);
    // new Cart(userInfo, langId);
    mAuth.checkUser(req.body.langId)
        .then((data) => {
            res.json(data)
        })
        .catch((error) => {
            res.json(error)
            res.end();
        })
});

router.post('/forgot/check-email',function(req, res, next) {
    const email = req.body.email;
    // console.log('req.body,',req.body)
    let mAuth = new Auth({email:email},{});
    mAuth.checkEmailExists()
        .then((data) => {
            var userId = mAuth.setUserId(data.data[0].id)
            if(data.errorText == "user Exists"){
                mAuth.sendForgotEmail()
                    .then(function (data) {
                        res.json({error : false})
                    })
                    .catch(function (error) {
                        res.json({error : true, msg:"02"})
                    })
            }else{
                res.json({error : true,msg:"no User"})
            }
        })
        .catch((error) => {
            res.json({error : true,msg:"no User"})
            res.end();
        })
});

router.post('/forgot/check-token',function(req, res, next) {
    const token = req.body.token;
    let mAuth = new Auth({},{});
    mAuth.checkForgotToken(token)
        .then((data) => {
            res.json(data)
        })
        .catch((error) => {
            res.json({error : true,msg:"no User"})
            res.end();
        })
});

router.post('/forgot/update-password',function(req, res, next) {
    const token = req.body.token;
    let mAuth = new Auth({password : req.body.password,re_password : req.body.repassword},{});
    mAuth.checkForgotToken(token)
        .then((data) => {
            var infoErrors = mAuth.checkForgotInfo();
            if(!infoErrors.error){
                mAuth.setHashedPassword();
                mAuth.updatePassword()
                    .then((data) => {
                        res.json(data)
                        res.end();
                    })
                    .catch((error) => {
                        res.json(data)
                        res.end();
                    })
            }else{
                res.json(infoErrors)
                res.end();
            }
        })
        .catch((error) => {
            res.json({error : true,msg:"no User"})
            res.end();
        })
});


router.post('/email/verify',function(req, res, next) {
    var email = req.body.email;
    let mAuth = new Auth({email:email},{});
    mAuth.getUserInfoByEmail()
        .then(function (userData) {
            if(userData.length === 0){
                res.json({error:true, msg:"Email not defined"})
            }else{
                var userInfo = userData[0];
                if(userInfo.email_verify == '1'){
                    res.json({error:true, msg:"Email is validated"})
                }else{
                    mAuth.sendAuthenticationEmail(userInfo.id)
                        .then((data) => {
                            res.json(data)
                            res.end();
                        })
                }
            }
        })
});
router.post('/email/verify-token',function(req, res, next) {
    var token = req.body.token;
    let mAuth = new Auth({},{});
    mAuth.checkEmailAuthenticationToken(token)
        .then((data) => {
            res.json(data)
            res.end();
        })
});


router.post('/email/change/verify-token',function(req, res, next) {
    var token = req.body.token;
    let mAuth = new Auth({},{});
    mAuth.checkEmailChangeToken(token)
        .then((data) => {
            res.json(data)
            res.end();
        })
});


module.exports = router;
