var express = require('express');
var router = express.Router();
var querys = require('../../model/dbQuerys/querys');
let logs = require('../../logs/log')
var statics = require('../../static');
var staticMethods = require('../../model/staticMethods');


router.post('/all', function(req, res, next) {
    var langId = 2;
    if(typeof req.body.langId !== "undefined")
        langId = req.body.langId
    var page = 1;
    if(typeof req.body.page !== "undefined")
        page = req.body.page

        Promise.all([
            querys.findAllNewsCount(),
            querys.findNews(page,langId)
        ])
            .then(function ([itemsCount,data]) {
                var pagesCount = Math.ceil(parseInt(itemsCount) / staticMethods.pageNewsCount);
                res.json({error: false, data: {items : data, pagesCount : pagesCount}})
            })
            .catch(function (error) {
                console.log('error',error)
                logs.insertLog("errorCode = 0213 " + JSON.stringify(error) + ', date = ' + new Date(), "./logs/v1/all.txt");
                res.json({error: true, msg: '0213'})
            })
});

router.post('/item', function(req, res, next) {
    var langId = 2;
    if(typeof req.body.langId !== "undefined")
        langId = req.body.langId
    if(typeof req.body.slug !== "undefined"){
        Promise.all([
            querys.findNewsById(req.body.slug,langId)
        ])
            .then(function ([data]) {
                res.json({error: false, data: data})
            })
            .catch(function (error) {
                logs.insertLog("errorCode = 0212 " + JSON.stringify(error) + ', date = ' + new Date(), "./logs/v1/all.txt");
                res.json({error: true, msg: '0212'})
            })
    }else{
        logs.insertLog("errorCode = 0211 " + JSON.stringify(error) + ', date = ' + new Date(), "./logs/v1/all.txt");
        res.json({error: true, msg: '0211'})
    }
});

router.get('/get-latest/:lang_id', function(req, res, next) {
    var langId = 2;
    if(typeof req.params.langId !== "undefined")
        langId = req.params.langId

        Promise.all([
            querys.findLatestNews(langId)
        ])
            .then(function ([data]) {
                data.forEach(function (item) {
                    item.publicUrl = statics.MAIN_URL+"/news/item/"+item.slug
                })
                res.json({error: false, data: data})
            })
            .catch(function (error) {
                logs.insertLog("errorCode = 0212 " + JSON.stringify(error) + ', date = ' + new Date(), "./logs/v1/all.txt");
                res.json({error: true, msg: '0212'})
            })
});


module.exports = router;
