var express = require('express');
var router = express.Router();
var checkRequestUser = require('../checkRequestUser');
var Catalog = require('../../objects/catalog/Catalog')
var Filter = require('../../objects/filters/Filter')
var Product = require('../../objects/product/Product')

router.post('/all',checkRequestUser.checkUserOrGuest, function(req, res, next) {
    const userInfo = {userType :req.user_type,userId : req.user_id, userStatus : req.user_status};
    // console.log('....',userInfo)
    const langId = req.body.langId;
    const page = req.body.page;
    var mCatalog = new Catalog(langId,page,{},userInfo);
    var mFilter = new Filter(langId);
    var mProduct = new Product(langId,userInfo);
    // var mProduct = new Product();
    Promise.all([
        mCatalog.checkPagesAndCount(),
        mCatalog.getCatalog(),
        mFilter.getAllFilters(),
        mProduct.getProductMinAndMaxPrices()
    ])
        .then(function ([count,catalog,filters,priceRange]) {
            // console.log('1111..count',count)
            // console.log('1111..catalog',catalog)
            // console.log('1111..filters',filters)
            res.json({error:false,data:{
                    priceRange : priceRange,
                    pagesCount : count,
                    catalog : catalog,
                    filters : filters
                }})
        })
        .catch(function (error) {
            res.json({error:true,data:[]})
        })
});

router.post('/get-catalog', function(req, res, next) {
    // console.log('req.body', req.body);


    res.json()
    // res.end();
});


router.post('/get-by-filters',checkRequestUser.checkUserOrGuest, function(req, res, next) {
    const userInfo = {userType :req.user_type,userId : req.user_id, userStatus : req.user_status};
    var paresedBody = JSON.parse(req.body.data)
    // var paresedBody = req.body
    console.log('}}}}}}}}}}',paresedBody)
    var mCatalog = new Catalog(2,paresedBody.page,paresedBody,userInfo);

    var mProduct = new Product(2,userInfo);

    mCatalog.checkFilterObj();

    var allQuerys = [
        mCatalog.checkPagesAndCount(),
        mProduct.getProductMinAndMaxPrices()
    ]
    console.log('req..................paresedBody.filters.length', paresedBody.filters.length);
    if(paresedBody.cat.length === 0 && paresedBody.brand.length === 0 && paresedBody.litrage.length === 0 && paresedBody.filters.length === 0){
        allQuerys.push(mCatalog.getCatalog())
    }else {
        if (paresedBody.filters.length === 0) {
            // var mCatalog = new Catalog(2,paresedBody.page,{},userInfo);
            allQuerys.push(mCatalog.getCatalogWithoutFilters())
        } else {
            allQuerys.push(mCatalog.getCatalogByFilters())
        }
    }
    Promise.all(allQuerys)
        .then(([pageCount,minMaxPrices,data])=>{
            var result = {};
            result.priceRange = minMaxPrices
            result.pagesCount = pageCount
            if(paresedBody.cat.length === 0 && paresedBody.brand.length === 0 && paresedBody.litrage.length === 0 && paresedBody.filters.length === 0 ){
                result.filters = []
                result.products = data
            }else{
                result.filters = data.filters
                result.products = data.products
            }

            res.json({error:false,data:result})
        })
});

router.post('/get-all-filters',checkRequestUser.checkUserOrGuest, function(req, res, next) {
    var mFilter = new Filter(req.body.langId);
    mFilter.getAllFilters()
        .then(function (filters) {
            res.json({error:false,data:filters})
        })
        .catch(function (error) {
            res.json({error:true,data:[]})
        })
});

router.post('/get-special-offers',checkRequestUser.checkUserOrGuest, function(req, res, next) {
    const userInfo = {userType :req.user_type,userId : req.user_id, userStatus : req.user_status};
    var mProduct = new Product(2,userInfo);
    mProduct.getSpecialOffers()
        .then(function (data) {
            res.json({error:false,data:data})
        })
        .catch(function (error) {
            res.json({error:true,data:[]})
        })
});

module.exports = router;
