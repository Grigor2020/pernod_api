var express = require('express');
var router = express.Router();
var querys = require('../../model/dbQuerys/querys');
let logs = require('../../logs/log')
var statics = require('../../static');

router.get('/all/:lang_id', function(req, res, next) {
    if(typeof req.params.lang_id !== "undefined" && req.params.lang_id !== '') {
        querys.findAllSlide(req.params.lang_id,statics.API_URL)
            .then(function (data) {
                res.json({error: false, data: {slide: data}})
            })
            .catch(function (error) {
                logs.insertLog("errorCode = 0202 " + JSON.stringify(error) + ', date = ' + new Date(), "./logs/v1/slide.txt");
                res.json({error: true, msg: '0202'})
            })
    }else{
        logs.insertLog("errorCode = 0201 , date = " + new Date(), "./logs/v1/slide.txt");
        res.json({error: true, msg: '0201'})
    }
});


router.get('/get-login-slide/:lang_id', function(req, res, next) {
    if(typeof req.params.lang_id !== "undefined" && req.params.lang_id !== '') {
        querys.findAllLoginSlide(req.params.lang_id,statics.API_URL)
            .then(function (data) {
                res.json({error: false, data: {login_slide: data}})
            })
            .catch(function (error) {
                logs.insertLog("errorCode = 0202 " + JSON.stringify(error) + ', date = ' + new Date(), "./logs/v1/slide.txt");
                res.json({error: true, msg: '0202'})
            })
    }else{
        logs.insertLog("errorCode = 0201 , date = " + new Date(), "./logs/v1/slide.txt");
        res.json({error: true, msg: '0201'})
    }
});


module.exports = router;
