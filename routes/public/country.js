var express = require('express');
var router = express.Router();
var querys = require('../../model/dbQuerys/querys');
let logs = require('../../logs/log')
var statics = require('../../static');

router.get('/all', function(req, res, next) {
    querys.findAllCountries(1)
        .then(function (data) {
            // console.log('...............data',data)
            res.json({error: false, data: data})
        })
        .catch(function (error) {
            logs.insertLog("errorCode = 0210 " + JSON.stringify(error) + ', date = ' + new Date(), "./logs/v1/all.txt");
            res.json({error: true, msg: '0210'})
        })
});


module.exports = router;
