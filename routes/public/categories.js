var express = require('express');
var router = express.Router();
var querys = require('../../model/dbQuerys/querys');
let logs = require('../../logs/log')
var statics = require('../../static');

router.get('/all/:lang_id', function(req, res, next) {
    if(typeof req.params.lang_id !== "undefined" && req.params.lang_id !== '') {
        querys.findAllCategories(req.params.lang_id,statics.API_URL)
            .then(function (data) {
                // console.log('...............data',data)
                res.json({error: false, data: {categories: data}})
            })
            .catch(function (error) {
                logs.insertLog("errorCode = 0210 " + JSON.stringify(error) + ', date = ' + new Date(), "./logs/v1/slide.txt");
                res.json({error: true, msg: '0210'})
            })
    }else{
        logs.insertLog("errorCode = 0211 , date = " + new Date(), "./logs/v1/slide.txt");
        res.json({error: true, msg: '0211'})
    }
});


module.exports = router;
