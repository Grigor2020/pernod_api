var express = require('express');
var router = express.Router();
var querys = require('../../model/dbQuerys/querys');
let logs = require('../../logs/log')

router.get('/all/:lang_id', function(req, res, next) {
    if(typeof req.params.lang_id !== "undefined" && req.params.lang_id !== '') {
        querys.findAllTranslate(req.params.lang_id)
            .then(function (data) {
                // console.log('data',data)
                var obj = {};
                for(let i = 0; i < data.length; i++){
                    // console.log('data[i].key_name',data[i].word_key)
                    obj[data[i].word_key] =  data[i].word
                }
                res.json({error: false, data: {translates: obj}})
            })
            .catch(function (error) {
                logs.insertLog("errorCode = 0008 " + JSON.stringify(error) + ', date = ' + new Date(), "./logs/v1/translate.txt");
                res.json({error: true, msg: '0008'})
            })
    }else{
        logs.insertLog("errorCode = 0007 , date = " + new Date(), "./logs/v1/translate.txt");
        res.json({error: true, msg: '0007'})
    }
});


module.exports = router;
