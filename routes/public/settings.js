var express = require('express');
var router = express.Router();
var querys = require('../../model/dbQuerys/querys');
let logs = require('../../logs/log')

router.get('/', function(req, res, next) {
    querys.findAllPromise('settings')
        .then(function (data) {
            res.json({error: false, data: data[0]})
        })
        .catch(function (error) {
            logs.insertLog("errorCode = 0210 " + JSON.stringify(error) + ', date = ' + new Date(), "./logs/v1/all.txt");
            res.json({error: true, msg: '0210'})
        })
});


module.exports = router;