var express = require('express');
var router = express.Router();
var querys = require('../../model/dbQuerys/querys');
var productQuerys = require('../../model/dbQuerys/product/product');
let logs = require('../../logs/log')
var statics = require('../../static');

router.get('/all/:lang_id', function(req, res, next) {
    if(typeof req.params.lang_id !== "undefined" && req.params.lang_id !== '') {
        Promise.all([
            productQuerys.findMinMaxPrice(),
            querys.findAllCategoriesWithBrands(req.params.lang_id,statics.API_URL)
        ])
            .then(function ([price,data]) {
                // console.log('.............price........',price)
                var categoriesWithBrands = [];
                var brands = [];
                for (var i = 0;  i < data.length; i++){
                    var objCB =
                        {
                            image : data[i].catImage,
                            effect_image : data[i].catEffectImage,
                            name : data[i].catName,
                            slug : data[i].catSlug,
                            catalogUrl : statics.MAIN_URL+"/catalog?page=1&cat="+data[i].catSlug+"&brand=&litrage=&filters=&price=0,"+Math.ceil(price.max_price/1000)*1000+""
                        };
                    // console.log('objCB',objCB)
                    let sArr = [];
                    if(data[i].brandName !== null){
                        var brandName = data[i].brandName.split('||');
                        var brandLogo = data[i].brandLogo.split('||');
                        var brandSlug = data[i].brandSlug.split('||');
                        for(let j = 0; j < brandName.length; j++){
                            sArr.push(
                                {
                                    name : brandName[j],
                                    slug :  brandSlug[j],
                                    image  : brandLogo[j],
                                    catalogUrl : statics.MAIN_URL+"/catalog?page=1&cat=&brand="+brandSlug[j]+"&litrage=&filters=&price=0,"+Math.ceil(price.max_price/1000)*1000+""
                                }
                            )
                            brands.push(
                                {
                                    name : brandName[j],
                                    image : brandLogo[j],
                                    slug :  brandSlug[j],
                                    catalogUrl : statics.MAIN_URL+"/catalog?page=1&cat=&brand="+brandSlug[j]+"&litrage=&filters=&price=0,"+Math.ceil(price.max_price/1000)*1000+""
                                }
                            )
                        }
                    }
                    objCB.brands = sArr
                    categoriesWithBrands.push(objCB)
                }
                // console.log('categoriesWithBrands',categoriesWithBrands)
                res.json({error: false, data: {categoriesWithBrands:categoriesWithBrands,brands:brands}})
            })
            .catch(function (error) {
                logs.insertLog("errorCode = 0270 " + JSON.stringify(error) + ', date = ' + new Date(), "./logs/v1/categories.txt");
                res.json({error: true, msg: '0270'})
            })
    }else{
        logs.insertLog("errorCode = 0271 , date = " + new Date(), "./logs/v1/categories.txt");
        res.json({error: true, msg: '0271'})
    }
});


module.exports = router;
