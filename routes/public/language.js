var express = require('express');
var router = express.Router();
var querys = require('../../model/dbQuerys/querys');
let logs = require('../../logs/log')

router.get('/all', function(req, res, next) {

    querys.findAllPromise('language')
        .then(function (data) {
            // console.log('/////////////////////////////////////data',data)
            res.json({error:false,data:{languages : data}})
        })
        .catch(function (error) {
            logs.insertLog("errorCode = 0007 "+ JSON.stringify(error)+', date = '+new Date(),"./logs/v1/language.txt");
            res.json({error :true,msg:'0007'})
        })
});


module.exports = router;
