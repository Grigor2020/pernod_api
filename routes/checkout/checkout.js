var express = require('express');
var router = express.Router();
var Cart = require('../../objects/cart/Cart')
var Profile = require('../../objects/profile/Profile')
var querys = require('../../model/dbQuerys/querys')
var checkRequestUser = require('../checkRequestUser');


router.post('/start',checkRequestUser.checkUserOrGuest, function(req, res, next) {
    let userInfo = {userType :req.user_type,userId : req.user_id, userStatus : req.user_status};
    var mCart = new Cart(userInfo, req.body.lang_id);
    var mProfile = new Profile(userInfo,req.body.lang_id);
// in our DB Country -> State -> City are only in English
    Promise.all([
        // mCart.getCartInfo()
        mCart.getCartCheckoutInfo(),
        querys.findAllCountries(1),
        mProfile.getUserAddresses()
    ])
        .then(function ([cart,countries, addresses]) {
            // console.log('getCartInfo data',cart)
            res.json({error : false, data : {cart : cart, countries : countries, addresses : addresses} })
        })
        .catch(function (error) {
            res.json({error : true, data : {} })
        })
});


router.post('/get-countries', function(req, res, next) {
    querys.findAllCountries(1)
        .then(function (countries) {
            res.json({error : false, data : { countries : countries} })
        })
        .catch(function (error) {
            res.json({error : true, data : {} })
        })
});

router.post('/get-states', function(req, res, next) {
    querys.findAllStates(req.body.countryId,1)
        .then(function (states) {
            res.json({error : false, data : { states : states} })
        })
        .catch(function (error) {
            res.json({error : true, data : {} })
        })
});


router.post('/get-cities', function(req, res, next) {
    querys.findAllCities(req.body.stateId,1)
        .then(function (cities) {
            res.json({error : false, data : { cities : cities} })
        })
        .catch(function (error) {
            res.json({error : true, data : {} })
        })
});


module.exports = router;
