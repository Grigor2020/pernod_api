var express = require('express');
var router = express.Router();
var querys = require('../../model/dbQuerys/querys');
let logs = require('../../logs/log')
var statics = require('../../static');



router.post('/', function(req, res, next) {
    if(typeof req.body.lang_id !== "undefined" && req.body.lang_id !== '') {
        // console.log('raboutaboutaboutaboutaboutaboutaboutaboutabout,',req.body.lang_id )
        querys.findByMultyNamePromise('about',{lang_id : req.body.lang_id}, ['text'])
            .then(function (data) {
                // console.log('...............data',data)
                res.json({error: false, data: data})
            })
            .catch(function (error) {
                logs.insertLog("errorCode = 0210 " + JSON.stringify(error) + ', date = ' + new Date(), "./logs/v1/all.txt");
                res.json({error: true, msg: '0210'})
            })
    }else{
        logs.insertLog("errorCode = 0211 , date = " + new Date(), "./logs/v1/all.txt");
        res.json({error: true, msg: '0211'})
    }
});


module.exports = router;