var express = require('express');
var router = express.Router();
var querys = require('../../model/dbQuerys/querys')
var checkRequestUser = require('../checkRequestUser');


router.post('/all',checkRequestUser.checkUserOrGuest, function(req, res, next) {
    // console.log('....................................req.body',req.body)
    var field = ''
    var vals = req.user_id
    if(req.user_type == 'guest'){
        field = 'guest_id';
    }else{
        field = 'user_id';
    }
    querys.findFavourite(field,vals,req.body.langId)
        .then(function (data) {
            // console.log('data',data)
            res.json({error :false, data : {favourite : data}})
        })
        .catch(function (error) {
            res.json({error :true})
        })
});
router.post('/add',checkRequestUser.checkUserOrGuest, function(req, res, next) {
    // console.log('req.body',req.body)
    // console.log('user_type',req.user_type)
    // console.log('user_type',req.user_id)
    console.log('req.body',req.body)
    var params = {}
    if(req.user_type == 'guest'){
        params = {guest_id : req.user_id,product_id : req.body.productId}
    }else{
        params = {user_id : req.user_id,product_id : req.body.productId}
    }
    querys.findByMultyNamePromise('favorite',params,['id'])
        .then(function (find) {
            // console.log('find',find)
            if(find.length > 0){
                querys.deletesPromise('favorite',params)
                    .then(function (data) {
                        console.log("deleted")
                        res.json({error : false , data : {action : "deleted"}})
                    })
                    .catch(function (error) {
                        res.json({error : true})
                    })
            }else{

                console.log('"added"',params)
                querys.insert2vPromise('favorite',params)
                    .then(function (data) {
                        console.log("added")
                        res.json({error : false, data : {action : "added"}})
                    })
                    .catch(function (error) {
                        res.json({error : true})
                    })
            }
        })
        .catch(function (error) {
            res.json({error : true})
        })
});



module.exports = router;
