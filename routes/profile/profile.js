var express = require('express');
var router = express.Router();
var querys = require('../../model/dbQuerys/querys');
let logs = require('../../logs/log')
var statics = require('../../static');
var checkRequestUser = require('../checkRequestUser');
var sha1 = require('sha1');
var Profile = require('../../objects/profile/Profile')

router.post('/get-info',checkRequestUser.checkUserOrGuest, function(req, res, next) {
    if(req.user_type === 'user'){
        var langId = 2;
        if(typeof req.body.langId !== "undefined")
            langId = req.body.langId
        const userInfo = {userType :req.user_type,userId : req.user_id, userStatus : req.user_status};
        var mProfile = new Profile(userInfo,langId);
        Promise.all([
            mProfile.getProfileOrders(3),
            mProfile.getAccountLoyalityStatus(),
            mProfile.getUserAddressesLimits(3)
        ])
            .then(function ([orders,loyalty,addresses]) {
                // console.log('.........orders..........data',orders)
                // console.log('.........loyalty..........data',loyalty)
                // console.log('.........addresses..........data',addresses)
                res.json({error : false,data : {
                        orders :orders,
                        loyalty :loyalty,
                        addresses :addresses
                    }})
            })
            .catch(function (error) {
                // console.log('.........error..........data',error)
                res.json({error : true})
                res.end();
            })
    }else{
        res.json({error:true,errorMsg : 'Authentication Failed'})
        res.end();
    }
});
router.post('/get-orders',checkRequestUser.checkUserOrGuest, function(req, res, next) {
    if(req.user_type === 'user'){
        var langId = 2;
        if(typeof req.body.langId !== "undefined")
            langId = req.body.langId
        const userInfo = {userType :req.user_type,userId : req.user_id, userStatus : req.user_status};
        var mProfile = new Profile(userInfo,langId);
        Promise.all([
            mProfile.getProfileAllOrders()
        ])
            .then(function ([orders]) {
                res.json({error : false,data : {
                        orders :orders
                    }})
            })
            .catch(function (error) {
                res.json({error : true})
                res.end();
            })
    }else{
        res.json({error:true,errorMsg : 'Authentication Failed'})
        res.end();
    }
});

router.post('/get-order-by-id',checkRequestUser.checkUserOrGuest, function(req, res, next) {
    if(req.user_type === 'user'){
        var publicId = req.body.id;
        var langId = 2;
        if(typeof req.body.langId !== "undefined")
            langId = req.body.langId
        const userInfo = {userType :req.user_type,userId : req.user_id, userStatus : req.user_status};
        var mProfile = new Profile(userInfo,langId);

        Promise.all([
            mProfile.getOrderById(publicId),
        ])
            .then(function ([orderInfo]) {

                mProfile.getOrderProducts(langId,orderInfo[0].order_id)
                    .then(function (data) {

                        // console.log('orderInfo', orderInfo)

                        var orderDetaling = orderInfo[0];
                        console.log('orderDetaling', orderDetaling)
                        if(orderDetaling.payment_type == '1'){
                            if(orderInfo[0].paymentDetailResponse !== null){
                                var payDetail = JSON.parse(orderInfo[0].paymentDetailResponse)
                                orderDetaling.CardNumber = payDetail.CardNumber
                                // orderDetaling.Amount = payDetail.Amount
                                delete orderDetaling.paymentDetailResponse
                            }
                        }
                        console.log('res ord', orderDetaling)
                        res.json({error : false,data : {
                                orderInfo :orderDetaling,
                                orderProducts : data
                            }})
                    })
                    .catch(function (error) {
                        res.json({error : true})
                        res.end();
                    })
            })
            .catch(function (error) {
                res.json({error : true})
                res.end();
            })
    }else{
        res.json({error:true,errorMsg : 'Authentication Failed'})
        res.end();
    }
});

router.post('/remove-order',checkRequestUser.checkUserOrGuest, function(req, res, next) {
    if(req.user_type === 'user'){
        var orderId = req.body.orderId;
        var langId = 2;
        if(typeof req.body.langId !== "undefined")
            langId = req.body.langId
        const userInfo = {userType :req.user_type,userId : req.user_id, userStatus : req.user_status};
        var mProfile = new Profile(userInfo,langId);
        Promise.all([
            mProfile.removeOrder(orderId),
        ])
            .then(function ([orderInfo]) {
                res.json({error : false})
            })
            .catch(function (error) {
                res.json({error : true})
                res.end();
            })
    }else{
        res.json({error:true,errorMsg : 'Authentication Failed'})
        res.end();
    }
});

router.post('/get-user-addresses',checkRequestUser.checkUserOrGuest, function(req, res, next) {
    if(req.user_type === 'user'){
        var langId = 2;
        if(typeof req.body.langId !== "undefined")
            langId = req.body.langId
        const userInfo = {userType :req.user_type,userId : req.user_id, userStatus : req.user_status};
        var mProfile = new Profile(userInfo,langId);
        Promise.all([
            mProfile.getUserAddresses()
        ])
            .then(function ([addresses]) {
                res.json({error : false,data : {
                        addresses :addresses
                    }})
            })
            .catch(function (error) {
                res.json({error : true})
                res.end();
            })
    }else{
        res.json({error:true,errorMsg : 'Authentication Failed'})
        res.end();
    }
});

router.post('/user-info/update',checkRequestUser.checkUserOrGuest, function(req, res, next) {
    if(req.user_type === 'user'){
        var langId = 2;
        if(typeof req.body.langId !== "undefined")
            langId = req.body.langId
        const userInfo = {userType :req.user_type,userId : req.user_id, userStatus : req.user_status};

        var mProfile = new Profile(userInfo,langId);
        var errorInData = mProfile.checkUpdateInformation(req.body);
        if(!errorInData.error){
            querys.updatePromise('users_db',{where : {id : userInfo.userId},values : {name : req.body.name,last_name : req.body.last_name,phone : req.body.phone,} })
                .then(function (data) {
                    res.json({error : false})
                    res.end();
                })
                .catch(function (error) {
                    res.json({error : true})
                    res.end();
                })
        }else{
            res.json(errorInData)
            res.end();
        }
    }else{
        res.json({error:true,errorMsg : 'Authentication Failed'})
        res.end();
    }
});

router.post('/address/add',checkRequestUser.checkUserOrGuest, function(req, res, next) {
    if(req.user_type === 'user'){
        var langId = 2;
        if(typeof req.body.langId !== "undefined")
            langId = req.body.langId
        const userInfo = {userType :req.user_type,userId : req.user_id, userStatus : req.user_status};

        var mProfile = new Profile(userInfo,langId);
        var errorInData = mProfile.checkAdingAddressInformation(req.body);
        // console.log('errorInData',errorInData)
        if(!errorInData.error){
            // console.log('req.body',req.body)
            mProfile.addAddress(req.body)
                .then(function (data) {
                    res.json({error:false,data:data})
                })
                .catch(function (error) {
                    res.json({error:true,data:{}})
                })
        }else{
            res.json(errorInData)
            res.end();
        }
    }else{
        res.json({error:true,errorMsg : 'Authentication Failed'})
        res.end();
    }
});

router.post('/address/edit',checkRequestUser.checkUserOrGuest, function(req, res, next) {
    if(req.user_type === 'user'){
        var langId = 2;
        if(typeof req.body.langId !== "undefined")
            langId = req.body.langId
        const userInfo = {userType :req.user_type,userId : req.user_id, userStatus : req.user_status};

        var mProfile = new Profile(userInfo,langId);
        var errorInData = mProfile.checkAdingAddressInformation(req.body);
        if(!errorInData.error){
            mProfile.editAddress(req.body)
                .then(function (data) {
                    res.json({error:false,data:data})
                })
                .catch(function (error) {
                    res.json({error:true})
                })
        }else{
            res.json(errorInData)
            res.end();
        }
    }else{
        res.json({error:true,errorMsg : 'Authentication Failed'})
        res.end();
    }
});

router.post('/address/remove',checkRequestUser.checkUserOrGuest, function(req, res, next) {
    if(req.user_type === 'user'){
        var langId = 2;
        if(typeof req.body.langId !== "undefined")
            langId = req.body.langId
        const userInfo = {userType :req.user_type,userId : req.user_id, userStatus : req.user_status};

        var mProfile = new Profile(userInfo,langId);
            mProfile.deleteAddress(req.body.id)
                .then(function (data) {
                    res.json({error:false})
                })
                .catch(function (error) {
                    res.json({error:true})
                })
    }else{
        res.json({error:true,errorMsg : 'Authentication Failed'})
        res.end();
    }
});

router.post('/address/set-default',checkRequestUser.checkUserOrGuest, function(req, res, next) {
    if(req.user_type === 'user'){
        var langId = 2;
        if(typeof req.body.langId !== "undefined")
            langId = req.body.langId
        const userInfo = {userType :req.user_type,userId : req.user_id, userStatus : req.user_status};

        var mProfile = new Profile(userInfo,langId);
            mProfile.setDefault(req.body.id)
                .then(function (data) {
                    res.json({error:false})
                })
                .catch(function (error) {
                    res.json({error:true})
                })
    }else{
        res.json({error:true,errorMsg : 'Authentication Failed'})
        res.end();
    }
});

router.post('/change-password',checkRequestUser.checkUserOrGuest, function(req, res, next) {
    if(req.user_type === 'user'){
        var langId = 2;
        if(typeof req.body.langId !== "undefined")
            langId = req.body.langId
        const userInfo = {userType :req.user_type,userId : req.user_id, userStatus : req.user_status};

        var mProfile = new Profile(userInfo,langId);
        let params = {
            old_password : req.body.oldPAss,
            password : req.body.newPass,
            re_password : req.body.newRePass
        }
        var errors = mProfile.checkChangePassInfo(params);
        if(!errors.error){
            var newPass = sha1(params.password)
            var oldPass = sha1(params.old_password)
            mProfile.checkOldAndSetNewPassword(oldPass,newPass)
                .then(function (data) {
                    res.json(data)
                })
                .catch(function (error) {
                    res.json({error:true})
                })
        }else {
            res.json({error:true,data : errors})
            res.end();
        }
    }else{
        res.json({error:true,errorMsg : 'Authentication Failed'})
        res.end();
    }
});
router.post('/change-email',checkRequestUser.checkUserOrGuest, function(req, res, next) {
    if(req.user_type === 'user'){
        var langId = 2;
        if(typeof req.body.langId !== "undefined")
            langId = req.body.langId
        const userInfo = {userType :req.user_type,userId : req.user_id, userStatus : req.user_status};

        var mProfile = new Profile(userInfo,langId);
        var errors = mProfile.checkEmail(req.body.email);

        if(!errors.error){
            mProfile.setNewEmail(req.body.email)
                .then(function (data) {
                    res.json(data)
                })
                .catch(function (error) {
                    res.json({error:true, msg : '86456464'})
                })
        }else {
            res.json({error:true,data : errors})
            res.end();
        }
    }else{
        res.json({error:true,errorMsg : 'Authentication Failed'})
        res.end();
    }
});

router.post('/remove-account',checkRequestUser.checkUserOrGuest, function(req, res, next) {
    if(req.user_type === 'user'){
        var langId = 2;
        if(typeof req.body.langId !== "undefined")
            langId = req.body.langId
        const userInfo = {userType :req.user_type,userId : req.user_id, userStatus : req.user_status};

        var mProfile = new Profile(userInfo,langId);
        mProfile.deleteProfile()
            .then(function (data) {
                res.json(data)
            })
            .catch(function (error) {
                res.json({error:true})
            })
    }else{
        res.json({error:true,errorMsg : 'Authentication Failed'})
        res.end();
    }
});

module.exports = router;
