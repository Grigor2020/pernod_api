'use strict';
var log = require("../../logs/log");
const mailjet = require ('node-mailjet')
    .connect("5f276329359f13273bb3addf66262895", "c69215a0e7304bed93b5a24eb1b4b544")
let staticMethods = require('../../model/staticMethods');
let statics = require('../../static');

function sendMailAuth(user_mail,params) {

    return new Promise((resolve, reject) => {
        const request = mailjet
            .post("send", {'version': 'v3.1'})
            .request({
                "Messages":[{
                    "From": {
                        "Email": "noreply@pernod-ricard.am",
                        "Name": "Test test"
                    },
                    "To": [{
                        "Email": user_mail,
                        "Name": "Grigor Sadoyan"
                    }],
                    "Subject": "Pernod Ricard Armenia",
                    "TextPart": "",
                    "HTMLPart": "<h3>Dear passenger 1, welcome to <a href=\"https://www.mailjet.com/\">Mailjet</a>!</h3><br />May the delivery force be with you!"
                }]
            })
        request
            .then((result) => {
                resolve ({
                    error : false,
                    data : result.body
                })
            })
            .catch((err) => {
                resolve ({
                    error : true,
                    data : err.statusCode
                })
            })
    })
}
function sendMailForgotUrl(user_mail,params) {
    return new Promise((resolve, reject) => {
        const request = mailjet
            .post("send", {'version': 'v3.1'})
            .request({
                "Messages":[{
                    "From": {
                        "Email": "noreply@pernod-ricard.am",
                        "Name": "Pernod Ricard Armenia"
                    },
                    "To": [{
                        "Email": user_mail,
                        "Name": ""
                    }],
                    "Subject": "Pernod Ricard Armenia",
                    "TextPart": "",
                    "HTMLPart": forgotTemplate(user_mail,params.url)
                }]
            })
        request
            .then((result) => {
                resolve ({
                    error : false,
                    data : result.body
                })
            })
            .catch((err) => {
                resolve ({
                    error : true,
                    data : err.statusCode
                })
            })
    })
}

function sendVerificationEMail(user_mail,params) {
    return new Promise((resolve, reject) => {
        console.log('params',params)
        const request = mailjet
            .post("send", {'version': 'v3.1'})
            .request({
                "Messages":[{
                    "From": {
                        "Email": "noreply@pernod-ricard.am",
                        "Name": "Pernod Ricard Armenia"
                    },
                    "To": [{
                        "Email": user_mail,
                        "Name": ""
                    }],
                    "Subject": "Pernod Ricard Armenia",
                    "TextPart": "",
                    "HTMLPart": verificationTemplate(user_mail,params.url)
                }]
            })
        request
            .then((result) => {
                resolve ({
                    error : false,
                    data : result.body
                })
            })
            .catch((err) => {
                resolve ({
                    error : true,
                    data : err.statusCode
                })
            })
    })
}

function sendOrderEmail(products,orderDate,buyerInfo) {
    // console.log('........products',products)
    // console.log('........orderDate',orderDate)
    // console.log('........buyerInfo',buyerInfo)
    return new Promise((resolve, reject) => {
        const request = mailjet
            .post("send", {'version': 'v3.1'})
            .request({
                "Messages":[{
                    "From": {
                        "Email": "noreply@pernod-ricard.am",
                        "Name": "New Order In Pernod Ricard Armenia"
                    },
                    "To": [{
                        "Email": buyerInfo.email,
                        "Name": buyerInfo.name + " "+ buyerInfo.last_name
                    }],
                    "Subject": "New Order In Pernod Ricard Armenia",
                    "TextPart": "",
                    "HTMLPart": sendOrderEmailTemplate(products,orderDate,buyerInfo),
                    "Attachments": [
                        {
                            "Filename": "test.txt",
                            "ContentType": "text/plain",
                            "Base64Content": "VGhpcyBpcyB5b3VyIGF0dGFjaGVkIGZpbGUhISEK"
                        }
                    ]
                }]
            })
        request
            .then((result) => {
                resolve ({
                    error : false,
                    data : result.body
                })
            })
            .catch((err) => {
                resolve ({
                    error : true,
                    data : err.statusCode
                })
            })
    })
}

function sendEmailChangingEmail(newEmail,url) {
    // console.log('........products',products)
    // console.log('........orderDate',orderDate)
    // console.log('........buyerInfo',buyerInfo)
    return new Promise((resolve, reject) => {
        const request = mailjet
            .post("send", {'version': 'v3.1'})
            .request({
                "Messages":[{
                    "From": {
                        "Email": "noreply@pernod-ricard.am",
                        "Name": "New Order In Pernod Ricard Armenia"
                    },
                    "To": [{
                        "Email": newEmail,
                        "Name": "testName TestLastName "
                    }],
                    "Subject": "New Order In Pernod Ricard Armenia",
                    "TextPart": "",
                    "HTMLPart":  verificationNewMailTemplate(newEmail, url)
                }]
            })
        request
            .then((result) => {
                resolve ({
                    error : false,
                    data : result.body
                })
            })
            .catch((err) => {
                let errorData = {
                    "error : " : JSON.stringify(err),
                    "function : " : 'sendEmailChangingEmail',
                    "date : " : new Date(),
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/emails.txt")
                resolve ({
                    error : true,
                    data : err.statusCode
                })
            })
    })
}

function sendContactMessageToAdmin(admin_email,info) {
    return new Promise((resolve, reject) => {
        const request = mailjet
            .post("send", {'version': 'v3.1'})
            .request({
                "Messages":[{
                    "From": {
                        "Email": "noreply@pernod-ricard.am",
                        "Name": "New Contact Message In Pernod Ricard Armenia"
                    },
                    "To": [{
                        "Email": admin_email,
                        "Name": "Pernod Ricard Armenia"
                    }],
                    "Subject": info.subject,
                    "TextPart": "",
                    "HTMLPart": contactMsgToAdminTemplate(info)
                }]
            })
        request
            .then((result) => {
                resolve ({
                    error : false,
                    data : result.body
                })
            })
            .catch((err) => {
                resolve ({
                    error : true,
                    data : err.statusCode
                })
            })
    })
}

function sendContactMessageToUser(info) {
    return new Promise((resolve, reject) => {
        const request = mailjet
            .post("send", {'version': 'v3.1'})
            .request({
                "Messages":[{
                    "From": {
                        "Email": "noreply@pernod-ricard.am",
                        "Name": "Your Contact message send to Pernod Ricard Armenia"
                    },
                    "To": [{
                        "Email": info.email,
                        "Name": info.name
                    }],
                    "Subject": info.subject,
                    "TextPart": "",
                    "HTMLPart": contactMsgToUserTemplate()
                }]
            })
        request
            .then((result) => {
                resolve ({
                    error : false,
                    data : result.body
                })
            })
            .catch((err) => {
                resolve ({
                    error : true,
                    data : err.statusCode
                })
            })
    })
}

module.exports.sendMailAuth = sendMailAuth;
module.exports.sendMailForgotUrl = sendMailForgotUrl;
module.exports.sendVerificationEMail = sendVerificationEMail;
module.exports.sendOrderEmail = sendOrderEmail;
module.exports.sendContactMessageToAdmin = sendContactMessageToAdmin;
module.exports.sendContactMessageToUser = sendContactMessageToUser;
module.exports.sendEmailChangingEmail = sendEmailChangingEmail;



var forgotTemplate = function (email,url) {
    return `
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
            <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
                <meta content="width=device-width, initial-scale=1" name="viewport">
                <title>Reset your password</title>
                <style type="text/css">
                    *{-webkit-box-sizing: border-box;box-sizing: border-box;}
                    img{max-width:600px;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic}
                    a{text-decoration:none;border:0;outline:none;color:#bbb}
                    a img{border:none}
                    td,h1,h2,h3{font-family:Helvetica,Arial,sans-serif;font-weight:400}
                    td{text-align:center}
                    body{-webkit-font-smoothing:antialiased;-webkit-text-size-adjust:none;width:100%;height:100%;color:#37302d;background:#fff;font-size:16px}
                    table{border-collapse:collapse!important}
                </style>
                <style media="only screen and (max-width: 480px)" type="text/css">
                    /* Mobile styles */
                    @media only screen and (max-width: 480px) {
                        table[class="w320"] {
                            width: 320px !important;
                        }
                    }
                </style>
                <style type="text/css"></style>
            </head>
            <body bgcolor="#ffffff" class="body"
                  style="padding:0; margin:0; display:block; background:#ffffff; -webkit-text-size-adjust:none">
                <table align="center" cellpadding="0" cellspacing="0" height="100%" width="100%">
                    <tbody>
                        <tr>
                            <td align="center" bgcolor="#ffffff" class="" valign="top" width="100%">
                                <table cellpadding="0" cellspacing="0" class="w320" width="580" style="margin: 0 auto\`;border: 1px solid #D1D1D1;background-image: url(http://pernod-ricard.am/images/site/email-bg.png);background-repeat: no-repeat;background-position: center;background-size: 100% 100%">
                                    <tbody>
                                        <tr>
                                            <td align="center" class="" valign="top" style="">
                                                <img src="http://pernod-ricard.am/images/content/general/Logo.png" alt="" width="237" style="margin-top: 40px">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="" valign="top"  style="padding-left: 20px;padding-right: 20px;">
                                                <h1 style="margin-top: 35px;margin-bottom: 35px;font-size: 26px;line-height: 44px;text-align: center;text-transform: uppercase;color: #000000;">We've received a request
                                                    <br> to reset your password</h1>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="" valign="top"  style="padding-left: 20px;padding-right: 20px;">
                                                <p style="font-size: 20px;line-height: 34px;color: #474747;text-align: left;margin: 0px">We've received a request to reset your Pernod-Ricard Aremina password. Follow the link below to set a new password:</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="" valign="top"  style="padding-left: 20px;padding-right: 20px;">
                                                <a href="${url}" style="margin-top: 30px;display:inline-block;width: 100%;background: #005092;border: 1px solid #005092;padding: 28px;text-decoration: none;font-weight: normal;font-size: 14px;line-height: 16px;letter-spacing: 0.3em;text-transform: uppercase;color: #FFFFFF;margin-bottom: 38px">Reset my password</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="" valign="top"  style="padding-left: 20px;padding-right: 20px;">
                                                <p style="font-weight: normal;font-size: 24px;line-height: 34px;text-align: center;color: #7C7C7C;max-width: 369px;margin-top: 117px;margin-bottom: 30px;display: block;margin-left: auto;margin-right: auto">
                                                    If you did not send this request, please ignore this letter!
                                                </p>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </body>
            </html>`
}



var verificationTemplate = function (email,url) {
    return `
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
            <html xmlns="http://www.w3.org/1999/xhtml">
                <head>
                    <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
                    <meta content="width=device-width, initial-scale=1" name="viewport">
                    <title>Welcome Email</title>
                    <style type="text/css">
                        *{-webkit-box-sizing: border-box;box-sizing: border-box;}
                        img{max-width:600px;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic}
                        a{text-decoration:none;border:0;outline:none;color:#bbb}
                        a img{border:none}
                        td,h1,h2,h3{font-family:Helvetica,Arial,sans-serif;font-weight:400}
                        td{text-align:center}
                        body{-webkit-font-smoothing:antialiased;-webkit-text-size-adjust:none;width:100%;height:100%;color:#37302d;background:#fff;font-size:16px}
                        table{border-collapse:collapse!important}
                    </style>
                    <style media="only screen and (max-width: 480px)" type="text/css">
                        /* Mobile styles */
                        @media only screen and (max-width: 480px) {
                            table[class="w320"] {
                                width: 320px !important;
                            }
                        }
                    </style>
                    <style type="text/css"></style>
                </head>
                <body bgcolor="#ffffff" class="body"
                      style="padding:0; margin:0; display:block; background:#ffffff; -webkit-text-size-adjust:none">
                    <table align="center" cellpadding="0" cellspacing="0" height="100%" width="100%">
                        <tbody>
                            <tr>
                                <td align="center" bgcolor="#ffffff" class="" valign="top" width="100%">
                                    <table cellpadding="0" cellspacing="0" class="w320" width="580" style="border: 1px solid #D1D1D1;margin: 0 auto;background-image: url(http://pernod-ricard.am/images/site/email-bg.png);background-repeat: no-repeat;background-position: center;background-size: 100% 100%">
                                        <tbody>
                                            <tr>
                                                <td align="center" class="" valign="top" style="">
                                                    <img src="http://pernod-ricard.am/images/content/general/Logo.png" alt="" width="237" style="margin-top: 40px">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="" valign="top"  style="padding-left: 20px;padding-right: 20px;">
                                                    <h1 style="margin-top: 35px;margin-bottom: 35px;font-size: 26px;line-height: 44px;text-align: center;text-transform: uppercase;color: #000000;">You have successfully registered in
                                                        <a href="pernod-ricard.am" target="_blank" style="color: #023466;text-decoration: underline">pernod-ricard.am</a></h1>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="" valign="top"  style="padding-left: 20px;padding-right: 20px;">
                                                    <p style="font-size: 20px;line-height: 34px;color: #474747;text-align: left;margin: 0px">Thank you for your trust and interest! Login to enter your personal account:</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="" valign="top"  style="padding-left: 20px;padding-right: 20px;">
                                                    <p style="margin-top: 35px;font-size: 24px;line-height: 40px;color: #474747;text-align: left;margin-bottom: 35px;padding: 30px;background: #F5F8FB;">Your login: <span style="color: #023466;">${email}</span></p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="" valign="top"  style="padding-left: 20px;padding-right: 20px;">
                                                    <p style="font-weight: normal;font-size: 16px;line-height: 24px;color: #7C7C7C;text-align: left">
                                                        To get access to all the features of your personal account, please confirm your e-mail by clicking on the button below, by this you confirm your consent to receive mailings:
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="" valign="top"  style="padding-left: 20px;padding-right: 20px;">
                                                    <a href="${url}" target="_blank" style="display:inline-block;width: 100%;background: #005092;border: 1px solid #005092;padding: 28px;text-decoration: none;font-weight: normal;font-size: 14px;line-height: 16px;letter-spacing: 0.3em;text-transform: uppercase;color: #FFFFFF;margin-bottom: 38px">Confirm MY email address</a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </body>
            </html>`
}
var verificationNewMailTemplate = function (email,url) {
    return `
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
            <html xmlns="http://www.w3.org/1999/xhtml">
                <head>
                    <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
                    <meta content="width=device-width, initial-scale=1" name="viewport">
                    <title>Welcome Email</title>
                    <style type="text/css">
                        *{-webkit-box-sizing: border-box;box-sizing: border-box;}
                        img{max-width:600px;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic}
                        a{text-decoration:none;border:0;outline:none;color:#bbb}
                        a img{border:none}
                        td,h1,h2,h3{font-family:Helvetica,Arial,sans-serif;font-weight:400}
                        td{text-align:center}
                        body{-webkit-font-smoothing:antialiased;-webkit-text-size-adjust:none;width:100%;height:100%;color:#37302d;background:#fff;font-size:16px}
                        table{border-collapse:collapse!important}
                    </style>
                    <style media="only screen and (max-width: 480px)" type="text/css">
                        /* Mobile styles */
                        @media only screen and (max-width: 480px) {
                            table[class="w320"] {
                                width: 320px !important;
                            }
                        }
                    </style>
                    <style type="text/css"></style>
                </head>
                <body bgcolor="#ffffff" class="body"
                      style="padding:0; margin:0; display:block; background:#ffffff; -webkit-text-size-adjust:none">
                    <table align="center" cellpadding="0" cellspacing="0" height="100%" width="100%">
                        <tbody>
                            <tr>
                                <td align="center" bgcolor="#ffffff" class="" valign="top" width="100%">
                                    <table cellpadding="0" cellspacing="0" class="w320" width="580" style="border: 1px solid #D1D1D1;margin: 0 auto;background-image: url(http://pernod-ricard.am/images/site/email-bg.png);background-repeat: no-repeat;background-position: center;background-size: 100% 100%">
                                        <tbody>
                                            <tr>
                                                <td align="center" class="" valign="top" style="">
                                                    <img src="http://pernod-ricard.am/images/content/general/Logo.png" alt="" width="237" style="margin-top: 40px">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="" valign="top"  style="padding-left: 20px;padding-right: 20px;">
                                                    <h1 style="margin-top: 35px;margin-bottom: 35px;font-size: 26px;line-height: 44px;text-align: center;text-transform: uppercase;color: #000000;">Your mail has been successfully changed</h1>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="" valign="top"  style="padding-left: 20px;padding-right: 20px;">
                                                    <p style="font-size: 20px;line-height: 34px;color: #474747;text-align: left;margin: 0px">Thank you for your trust and interest! Login to enter your personal account:</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="" valign="top"  style="padding-left: 20px;padding-right: 20px;">
                                                    <p style="margin-top: 35px;font-size: 24px;line-height: 40px;color: #474747;text-align: left;margin-bottom: 35px;padding: 30px;background: #F5F8FB;">Your login: <span style="color: #023466;">${email}</span></p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="" valign="top"  style="padding-left: 20px;padding-right: 20px;">
                                                    <p style="font-weight: normal;font-size: 16px;line-height: 24px;color: #7C7C7C;text-align: left">
                                                        To get access to all the features of your personal account, please confirm your e-mail by clicking on the button below, by this you confirm your consent to receive mailings:
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="" valign="top"  style="padding-left: 20px;padding-right: 20px;">
                                                    <a href="${url}" target="_blank" style="display:inline-block;width: 100%;background: #005092;border: 1px solid #005092;padding: 28px;text-decoration: none;font-weight: normal;font-size: 14px;line-height: 16px;letter-spacing: 0.3em;text-transform: uppercase;color: #FFFFFF;margin-bottom: 38px">Confirm MY email address</a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </body>
            </html>`
}

var contactMsgToAdminTemplate = function (info) {
    return `
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
            <html xmlns="http://www.w3.org/1999/xhtml">
                <head>
                    <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
                    <meta content="width=device-width, initial-scale=1" name="viewport">
                    <title>Contact Message</title>
                    <style type="text/css">
                        *{-webkit-box-sizing: border-box;box-sizing: border-box;}
                        img{max-width:600px;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic}
                        a{text-decoration:none;border:0;outline:none;color:#bbb}
                        a img{border:none}
                        td,h1,h2,h3{font-family:Helvetica,Arial,sans-serif;font-weight:400}
                        td{text-align:center}
                        body{-webkit-font-smoothing:antialiased;-webkit-text-size-adjust:none;width:100%;height:100%;color:#37302d;background:#fff;font-size:16px}
                        table{border-collapse:collapse!important}
                    </style>
                    <style media="only screen and (max-width: 480px)" type="text/css">
                        /* Mobile styles */
                        @media only screen and (max-width: 480px) {
                            table[class="w320"] {
                                width: 320px !important;
                            }
                        }
                    </style>
                    <style type="text/css"></style>
                </head>
                <body bgcolor="#ffffff" class="body"
                      style="padding:0; margin:0; display:block; background:#ffffff; -webkit-text-size-adjust:none">
                    <table align="center" cellpadding="0" cellspacing="0" height="100%" width="100%">
                        <tbody>
                            <tr>
                                <td align="center" bgcolor="#ffffff" class="" valign="top" width="100%">
                                    <table cellpadding="0" cellspacing="0" class="w320" width="580" style="border: 1px solid #D1D1D1;margin: 0 auto;background-image: url(http://pernod-ricard.am/images/site/email-bg.png);background-repeat: no-repeat;background-position: centerbackground-size: 100% 100%">
                                        <tbody>
                                            <tr>
                                                <td align="center" class="" valign="top" style="">
                                                    <img src="http://pernod-ricard.am/images/content/general/Logo.png" alt="" width="237" style="margin-top: 40px">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="" valign="top"  style="padding-left: 20px;padding-right: 20px;">
                                                    <h1 style="margin-top: 35px;margin-bottom: 35px;font-size: 26px;line-height: 44px;text-align: center;text-transform: uppercase;color: #000000;">New Message from Contact page
                                                        <a href="pernod-ricard.am" target="_blank" style="color: #023466;text-decoration: underline">pernod-ricard.am</a></h1>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="" valign="top"  style="padding-left: 20px;padding-right: 20px;">
                                                    <p style="font-size: 20px;line-height: 34px;color: #474747;text-align: left;margin: 0px">Name: ${info.name}</p>
                                                    <p style="font-size: 20px;line-height: 34px;color: #474747;text-align: left;margin: 0px">E-mail: ${info.email}</p>
                                                    <p style="font-size: 20px;line-height: 34px;color: #474747;text-align: left;margin: 0px">Subject: ${info.subject}</p>
                                                    <p style="margin-bottom: 35px;font-size: 20px;line-height: 34px;color: #474747;text-align: left;margin: 0px">Message: ${info.message} </p>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </body>
            </html>`
}
var contactMsgToUserTemplate = function () {
    return `
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
            <html xmlns="http://www.w3.org/1999/xhtml">
                <head>
                    <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
                    <meta content="width=device-width, initial-scale=1" name="viewport">
                    <title>Contact Message</title>
                    <style type="text/css">
                        *{-webkit-box-sizing: border-box;box-sizing: border-box;}
                        img{max-width:600px;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic}
                        a{text-decoration:none;border:0;outline:none;color:#bbb}
                        a img{border:none}
                        td,h1,h2,h3{font-family:Helvetica,Arial,sans-serif;font-weight:400}
                        td{text-align:center}
                        body{-webkit-font-smoothing:antialiased;-webkit-text-size-adjust:none;width:100%;height:100%;color:#37302d;background:#fff;font-size:16px}
                        table{border-collapse:collapse!important}
                    </style>
                    <style media="only screen and (max-width: 480px)" type="text/css">
                        /* Mobile styles */
                        @media only screen and (max-width: 480px) {
                            table[class="w320"] {
                                width: 320px !important;
                            }
                        }
                    </style>
                    <style type="text/css"></style>
                </head>
                <body bgcolor="#ffffff" class="body"
                      style="padding:0; margin:0; display:block; background:#ffffff; -webkit-text-size-adjust:none">
                    <table align="center" cellpadding="0" cellspacing="0" height="100%" width="100%">
                        <tbody>
                            <tr>
                                <td align="center" bgcolor="#ffffff" class="" valign="top" width="100%">
                                    <table cellpadding="0" cellspacing="0" class="w320" width="580" style="border: 1px solid #D1D1D1;margin: 0 auto;background-image: url(http://pernod-ricard.am/images/site/email-bg.png);background-repeat: no-repeat;background-position: center;background-size: 100% 100%">
                                        <tbody>
                                            <tr>
                                                <td align="center" class="" valign="top" style="">
                                                    <img src="http://pernod-ricard.am/images/content/general/Logo.png" alt="" width="237" style="margin-top: 40px">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="" valign="top"  style="padding-left: 20px;padding-right: 20px;">
                                                    <h1 style="margin-top: 35px;margin-bottom: 35px;font-size: 26px;line-height: 44px;text-align: center;text-transform: uppercase;color: #000000;">New Message from Contact page
                                                        <a href="pernod-ricard.am" target="_blank" style="color: #023466;text-decoration: underline">pernod-ricard.am</a></h1>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="" valign="top"  style="padding-left: 20px;padding-right: 20px;">
                                                    <p style="margin-bottom: 35px;font-size: 20px;line-height: 34px;color: #474747;text-align: left;margin: 0px">Thank you, Please wait call from our contact manager </p>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </body>
            </html>`
}

var sendOrderEmailTemplate = function (products,orderDate,buyerInfo) {
    var prodContent = ``;
    var total = 0;
    for(var i = 0; i < products.length; i++){
        total+= parseInt(products[i].price.replace(/\s/g, ''))
        prodContent+= `
                        <tr>
                            <td align="center" class="" valign="top"  style="padding-top: 14px;padding-left: 20px;padding-right: 20px;border-bottom: 1px solid #F5F5F5;padding-bottom: 14px;">
                                <p style="margin-bottom: 0px;margin-top: 0px;text-align: center;">
                                    <img src="${products[i].image}" alt="" width="120" height="160">
                                </p>
                            </td>
                            <td align="center" class="" valign="top"  style="padding-top: 14px;padding-left: 20px;padding-right: 20px;border-bottom: 1px solid #F5F5F5;padding-bottom: 14px;">
                                <p style="margin-bottom: 0px;margin-top: 10px;text-align: left;width: 200px;">
                                    ${products[i].name} <br>
                                    Quantity: ${products[i].quantity}
                                </p>
                            </td>
                            <td align="center" class="" valign="top"  style="padding-top: 14px;padding-left: 20px;padding-right: 20px;border-bottom: 1px solid #F5F5F5;padding-bottom: 14px;">
                                <p style="margin-bottom: 0px;margin-top: 10px;text-align: right;">
                                    ֏  ${products[i].price}
                                </p>
                            </td>
                        </tr>`
    }
    var allTotal = staticMethods.formatMoney(total)
    // 1-cradit card, 2- idram, 3- cash, 4-bank transfer
    var payMethod = "";
    switch (orderDate.payment_type){
        case '1' :
            payMethod = 'Credit Card'
            break
        case '2' :
            payMethod = 'idram'
            break
        case '3' :
            payMethod = 'Cash'
            break
        case '4' :
            payMethod = 'Bank Transfer'
            break
        default :
            payMethod = 'Credit Card'
            break
    }


    return    `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
            <meta content="width=device-width, initial-scale=1" name="viewport">
                <title>Thank you</title>
                <style type="text/css">
                    *{-webkit-box-sizing: border-box;box-sizing: border-box;}
                    img{max-width:600px;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic}
                    a{text-decoration:none;border:0;outline:none;color:#bbb}
                    a img{border:none}
                    td,h1,h2,h3{font-family:Helvetica,Arial,sans-serif;font-weight:400}
                    td{text-align:center}
                    body{-webkit-font-smoothing:antialiased;-webkit-text-size-adjust:none;width:100%;height:100%;color:#37302d;background:#fff;font-size:16px}
                    table{border-collapse:collapse!important}
                </style>
                <style media="only screen and (max-width: 480px)" type="text/css">
                    /* Mobile styles */
                    @media only screen and (max-width: 480px) {
                    table[class="w320"] {
                    width: 320px !important;
                }
                }
                </style>
                <style type="text/css"></style>
    </head>
    <body bgcolor="#ffffff" class="body"
          style="padding:0; margin:0; display:block; background:#ffffff; -webkit-text-size-adjust:none">
    <table align="center" cellpadding="0" cellspacing="0" height="100%" width="100%">
        <tbody>
        <tr>
            <td align="center" bgcolor="#ffffff" class="" valign="top" width="100%">
                <table cellpadding="0" cellspacing="0" class="w320" width="580" style="margin: 0 auto;border: 1px solid #D1D1D1;background-image: url(http://pernod-ricard.am/images/site/email-bg.png);background-repeat: no-repeat;background-position: center;background-size: 100% 100%">
                    <tbody>
                    <tr>
                        <td colspan="3" align="center" class="" valign="top" style="" colspan="100%">
                            <img src="http://pernod-ricard.am/images/content/general/Logo.png" alt="" width="237" style="margin-top: 40px" >
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" align="center" class="" valign="top"  style="padding-left: 20px;padding-right: 20px;" colspan="100%">
                            <h1 style="margin-top: 27px;margin-bottom: 0px;font-size: 26px;line-height: 44px;text-align: center;text-transform: uppercase;color: #000000;">thank you for your order
                                <br>
                                    <span style="color: #023466;text-decoration: underline;text-transform: uppercase;">#${orderDate.public_id}</span>
                            </h1>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" align="center" class="" valign="top"  style="padding-left: 20px;padding-right: 20px;" colspan="100%">
                            <p style="text-align: left;margin-top: 27px;font-size: 16px;line-height: 26px;color: #000000;">
                                Hi ${buyerInfo.name},
                                <br>
                                    <br>
                                        We are delighted that you have found something you like!
                                        <br>
                                            As soon as your package is on its way, you will receive a delivery confirmation from us by email.
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" class="" valign="top"  style="padding-left: 20px;padding-right: 20px;" >
                            <p style="margin: 0px;font-weight: normal;font-size: 16px;line-height: 26px;color: #000000;text-align: left">
                                <span style="font-weight: bold;margin-bottom: 12px;text-transform: uppercase;">Delivery address</span>
                                <br>
                                    ${buyerInfo.name} ${buyerInfo.last_name} <br>
                                    ${buyerInfo.address} <br>
                                    ${buyerInfo.city} ${buyerInfo.phone}
                            </p>
                        </td>
                        <td align="center" class="" valign="top"  style="padding-left: 20px;padding-right: 20px;">
                            <p style="margin: 0px;font-weight: normal;font-size: 16px;line-height: 26px;color: #000000;text-align: left">
                                <span style="font-weight: bold;margin-bottom: 12px;text-transform: uppercase;">Order Date</span>
                                <br>
                                    ${orderDate.insert_data}
                            </p>
                        </td>
                        <td align="center" class="" valign="top"  style="padding-left: 20px;padding-right: 20px;">
                            <p style="margin: 0px;font-weight: normal;font-size: 16px;line-height: 26px;color: #000000;text-align: left">
                                <span style="font-weight: bold;margin-bottom: 12px;text-transform: uppercase;">Order number</span>
                                <br>
                                    <span style="text-decoration: underline;text-transform: uppercase;color: #023466;">#${orderDate.public_id}</span>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td  colspan="3" align="center" class="" valign="top"  style="padding-left: 20px;padding-right: 20px;" colspan="100%">
                            <p style="margin: 0px;font-weight: normal;margin-top: 27px;font-size: 16px;line-height: 26px;color: #000000;text-align: left;margin-bottom: 10px">
                                <span style="font-weight: bold;margin-bottom: 8px;text-transform: uppercase;">Your order item</span>
                                <br>
                            </p>
                        </td>
                    </tr>


                    <!-- Product / loop start -->
                    ${prodContent}
                    <!-- Product / loop end -->

                    <tr>
                        <td align="center" class="" valign="top"  style="padding-left: 20px;padding-right: 20px;"  colspan="1">
                            <p style="margin-top: 20px;margin-bottom:0px;font-weight: normal;font-size: 16px;line-height: 26px;color: #000000;text-align:left;">Payment method</p>
                        </td>
                        <td colspan="2" align="center" class="" valign="top"  style="padding-left: 20px;padding-right: 20px;"  colspan="2">
                            <p style="margin-top: 20px;margin-bottom:0px;font-weight: normal;font-size: 16px;line-height: 26px;color: #000000;text-align:right;">${payMethod}</p>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" class="" valign="top"  style="padding-left: 20px;padding-right: 20px;"  colspan="1">
                            <p style="margin-top: 8px;margin-bottom:0px;font-weight: normal;font-size: 16px;line-height: 26px;color: #000000;text-align:left;">Delivery</p>
                        </td>
                        <td  colspan="2" align="center" class="" valign="top"  style="padding-left: 20px;padding-right: 20px;"  colspan="2">
                            <p style="margin-top: 8px;margin-bottom:0px;font-weight: normal;font-size: 16px;line-height: 26px;color: #000000;text-align:right;">Free</p>
                        </td>
                    </tr>

                    <tr>
                        <td align="center" class="" valign="top"  style="padding-left: 20px;padding-right: 20px;"  colspan="1">
                            <p style="margin-top: 20px;margin-bottom:0px;font-weight: bold;font-size: 26px;line-height: 26px;color: #000000;text-align:left;">Total</p>
                        </td>
                        
                        <td  colspan="2"  align="center" class="" valign="top"  style="padding-left: 20px;padding-right: 20px;"  colspan="2">
                            <p style="margin-top: 20px;margin-bottom:0px;font-weight: bold;font-size: 26px;line-height: 26px;color: #000000;text-align:right;">֏ ${staticMethods.formatMoney(orderDate.total_price)}</p>
                        </td>

                    </tr>
                    <tr>
                        <td colspan="3"  align="center" className="" valign="top" style="padding-left: 20px;padding-right: 20px;" colSpan="100%">
                            <a target="_blank" href="${statics.MAIN_URL}/profile/order/get-invoice/${orderDate.public_id}"
                               style="margin-top: 27px;display:inline-block;width: 100%;background: #005092;border: 1px solid #005092;padding: 28px;text-decoration: none;font-weight: normal;font-size: 14px;line-height: 16px;letter-spacing: 0.3em;text-transform: uppercase;color: #FFFFFF;margin-bottom: 43px">Dowload
                                your order .pdf</a>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>
    </body>
    </html>`




}



