const db = require('../../connection');
var log = require("../../../logs/log");
const static = require('../../../static');
const staticMethods = require('../../../model/staticMethods');


var findCartItems = function(params,langId){
    // console.log('paginationData',paginationData)
    return new Promise(function (resolve, reject) {
        var prepareSql = "" +
            " SELECT `cart`.`id`,`cart`.`quantity`," +
            " `product`.`in_stock` " +
            " FROM `cart` " +
            " JOIN `product` ON `product`.`id` = `cart`.`product_id` " +
            " WHERE `cart`.`"+params.field+"`= "+db.escape(params.val)+" AND `cart`.`product_id` = "+db.escape(params.product_id)+"";
        // console.log('||||prepareSql',prepareSql)
        db.query(prepareSql, function (error, result) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findCartItems",
                        table:'cart',
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(findBrand),"./logs/v1/cart.txt")
                reject(new Error("Sql error findBrand"))
            }else{
                resolve(result)
            }
        })
    })
}


module.exports.findCartItems = findCartItems;