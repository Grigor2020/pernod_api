const db = require('../connection');
var log = require("../../logs/log");
const static = require('../../static');
const staticMethods = require('../staticMethods');

var findAllByFilds = function(table,filds,cb){
    var queryFild = "";
    if(filds == "*"){
        queryFild = filds;
    }else{
        queryFild = filds.join(' , ')
    }
    var prepareSql = 'SELECT ' + queryFild + ' FROM '+table+'';
    db.query(prepareSql, function (error, result, fields) {
        if (error){
            var errorData = {
                code:error.code,
                errno:error.errno,
                sqlMessage:error.sqlMessage,
                devData:{
                    queryFunction:"findAllByFilds",
                    table:table,
                    filds:filds,
                    date:new Date()
                }
            }
            log.insertLog(JSON.stringify(errorData),"./logs/query.txt")
            cb(true,errorData);
        }else{
            cb(false,result)
        }
    })
}

/**
 *
 * @param table
 * @param params {fild_name:value}
 * @param filds [fild1,fild2]
 * @param cb function
 */
var findByMultyName = function (table,params,filds,cb) {
    var whereParams = [];
    for(var key in params){
        if(params[key] !== "NULL") {
            whereParams.push("`" + key + "`=" + db.escape(params[key]));
        }else{
            whereParams.push("`" + key + "` IS NULL");
        }
    }
    var prepareSql = 'SELECT ' + filds.join(' , ') + ' FROM ' + table + ' WHERE '+whereParams.join(' AND ')+'';
    // console.log('prepareSql',prepareSql)
    db.query(prepareSql, function (error, rows, fields) {
        if (error){
            var errorData = {
                code:error.code,
                errno:error.errno,
                sqlMessage:error.sqlMessage,
                devData:{
                    queryFunction:"findByMultyName2v",
                    table:table,
                    params:params,
                    filds:filds,
                    date:new Date()
                }
            }
            log.insertLog(JSON.stringify(errorData),"./logs/query.txt")
            cb(true,errorData);
        }else{
            cb(false,rows)
        }
    });
}

var findAllByFildsPromise = function(table,filds){
    return new Promise(function (resolve, reject) {
        var queryFild = "";
        if(filds == "*"){
            queryFild = filds;
        }else{
            queryFild = filds.join(' , ')
        }
        var prepareSql = 'SELECT ' + queryFild + ' FROM '+table+'';
        db.query(prepareSql, function (error, result, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findAllByFilds",
                        table:table,
                        filds:filds,
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/query.txt")
                reject(new Error("Sql error findAllByFildsPromise"))
            }else{
                resolve(result)
            }
        })
    })
}

var findAllPromise = function(table){
    return new Promise(function (resolve, reject) {
        var prepareSql = 'SELECT * FROM '+table+'';
        db.query(prepareSql, function (error, result) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findAllByFilds",
                        table:table,
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/query.txt")
                reject(new Error("Sql error findAllPromise"))
            }else{
                resolve(result)
            }
        })
    })
}

var findAllG = function(table){
        var prepareSql = 'SELECT * FROM '+table+'';
        // console.log('prepareSql',prepareSql)
        db.query(prepareSql, function (error, result) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findAllG",
                        table:table,
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/query.txt")
                return {error:true,msg : "Sql error findAllPromise"}
            }else{
                // console.log('result,',result)
                return {error:false,data : result}
            }
        })

}

var findByMultyNamePromise = function(table,params,filds){
    return new Promise(function (resolve, reject) {
        var whereParams = [];
        for(var key in params){
            if(params[key] !== "NULL") {
                whereParams.push("`" + key + "`=" + db.escape(params[key]));
            }else{
                whereParams.push("`" + key + "` IS NULL");
            }
        }
        var prepareSql = 'SELECT ' + filds.join(' , ') + ' FROM ' + table + ' WHERE '+whereParams.join(' AND ')+'';
        // console.log('prepareSql',prepareSql)
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findByMultyNamePromise",
                        table:table,
                        params:params,
                        filds:filds,
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/query.txt")
                reject(new Error("Sql error findByMultyNamePromise"))
            }else{
                resolve(rows)
            }
        });
    })
}
var findFaq = function(lang_id){
    return new Promise(function (resolve, reject) {
        var prepareSql = 'SELECT ' +
            ' `faq_language`.`faq_id`,`faq_language`.`name`,`faq_language`.`answer`,`faq_language`.`question` ' +
            ' FROM `faq` ' +
            ' JOIN `faq_language` ON `faq_language`.`lang_id` = '+db.escape(lang_id)+' AND `faq_language`.`faq_id` = `faq`.`id` ' +
            ' ORDER by `faq`.`ord` ASC';
        // console.log('prepareSql',prepareSql)
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findFaq",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/query.txt")
                reject(new Error("Sql error findFaq"))
            }else{
                resolve(rows)
            }
        });
    })
}

var findNews = function(page,lang_id){
    return new Promise(function (resolve, reject) {
        var pagination = staticMethods.detectNewsPagination(page)
        var prepareSql = 'SELECT ' +
            ' `blog`.`slug`,' +
            ' `blog_language`.`title`,`blog_language`.`text`, ' +
            " CONCAT('"+static.API_URL+"','/images/blog/',`blog`.`image`) as image " +
            ' FROM `blog` ' +
            ' JOIN `blog_language` ON `blog_language`.`lang_id` = '+db.escape(lang_id)+' AND `blog_language`.`blog_id` = `blog`.`id` ' +
            ' ORDER by `blog`.`id` DESC LIMIT '+pagination.start+','+pagination.limit+'';
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findNews",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/query.txt")
                reject(new Error("Sql error findNews"))
            }else{
                resolve(rows)
            }
        });
    })
}

var findLatestNews = function(lang_id){
    return new Promise(function (resolve, reject) {
        var prepareSql = 'SELECT ' +
            ' `blog`.`slug`,' +
            ' `blog_language`.`title`,`blog_language`.`text`, ' +
            " CONCAT('"+static.API_URL+"','/images/blog/',`blog`.`image`) as image " +
            ' FROM `blog` ' +
            ' JOIN `blog_language` ON `blog_language`.`lang_id` = '+db.escape(lang_id)+' AND `blog_language`.`blog_id` = `blog`.`id` ' +
            ' ORDER by `blog`.`id` DESC LIMIT 0,4';
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findLatestNews",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/query.txt")
                reject(new Error("Sql error findNews"))
            }else{
                resolve(rows)
            }
        });
    })
}

var findNewsById = function(slug,lang_id){
    return new Promise(function (resolve, reject) {
        var prepareSql = 'SELECT ' +
            ' `blog`.`slug`,`blog_language`.`text`,' +
            ' `blog_language`.`title`,`blog_language`.`text`, ' +
            " CONCAT('"+static.API_URL+"','/images/blog/',`blog`.`image`) as image " +
            ' FROM `blog` ' +
            ' JOIN `blog_language` ON `blog_language`.`lang_id` = '+db.escape(lang_id)+' AND `blog_language`.`blog_id` = `blog`.`id` ' +
            ' WHERE `blog`.`slug` = '+db.escape(slug)+'';
        // console.log('prepareSql',prepareSql)
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findNewsById",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/query.txt")
                reject(new Error("Sql error findNews"))
            }else{
                resolve(rows)
            }
        });
    })
}

var findUserInfo = function(user_id){
    return new Promise(function (resolve, reject) {
        var prepareSql = 'SELECT ' +
            ' `users_db`.`name`,`users_db`.`last_name`,' +
            ' `users_db`.`status`,`users_db`.`token`,' +
            ' `users_db`.`phone`,`users_db`.`email`,`users_db`.`email_verify`,' +
            ' `user_change_email`.`email` as changing_email ' +
            ' FROM `users_db` ' +
            ' LEFT JOIN `user_change_email` ON `user_change_email`.`user_id` = '+db.escape(user_id)+' '+
            ' WHERE `users_db`.`id` = '+db.escape(user_id)+'';
        // console.log('prepareSql',prepareSql)
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findUserInfo",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/query.txt")
                reject(new Error("Sql error findNews"))
            }else{
                resolve(rows)
            }
        });
    })
}
var findAllNewsCount = function(slug,lang_id){
    return new Promise(function (resolve, reject) {
        var prepareSql = 'SELECT COUNT(`blog`.`id` ) as blogCount ' +
            ' FROM `blog` ';
        // console.log('prepareSql',prepareSql)
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findAllNewsCount",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/query.txt")
                reject(new Error("Sql error findAllNewsCount"))
            }else{
                resolve(rows[0].blogCount)
            }
        });
    })
}

function insert2vPromise(table,post){
    return new Promise(function (resolve, reject) {
        var prepareSql = 'INSERT INTO '+table+' SET ?';
        var query = db.query(prepareSql, post, function (error, results, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"insert2vPromise",
                        table:table,
                        params:post,
                        date:new Date()
                    }
                }
                // console.log('errorData',errorData)
                log.insertLog(JSON.stringify(errorData),"./logs/v1/query.txt")
                reject(new Error("Sql error insert2vPromise"))
            }else{
                resolve(results)
            }
        });
    })
}

var insertLoopPromise = function(table,filds,post){
    return new Promise(function (resolve, reject) {
        var prepareSql = 'INSERT INTO ' + table + ' (' + filds.join(',') + ') VALUES  ?';
        var query = db.query(prepareSql, [post], function (error, results, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"insertLoopPromise",
                        table:table,
                        filds:filds,
                        params:post,
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/query.txt")
                reject(new Error("Sql error insertLoopPromise"))
            }else{
                resolve(results)
            }
        });
    })
}

var updateLastLogin = function(user_id){
    return new Promise(function (resolve, reject) {
        var prepareSql = "UPDATE `users_db` SET `last_login_date` =  NOW()  WHERE `users_db`.`id` = "+user_id+"";
        var query = db.query(prepareSql, function (error, results, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"updateLastLogin",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/query.txt")
                reject(new Error("Sql error updateLastLogin"))
            }else{
                resolve(results)
            }
        });
    })
}

function deletePromise(table,params) {
    return new Promise(function (resolve, reject) {
        var where = [];
        for (var i = 0; i < params.length; i++) {
            where.push("`" + params[i]['key'] + "`=" + db.escape(params[i]['value']));
        }
        var prepareSql = "DELETE FROM  " + table + " WHERE " + where.join(" OR ") + " ";
        var query = db.query(prepareSql, function (error, results, fields) {
            if (error) {
                var errorData = {
                    code: error.code,
                    errno: error.errno,
                    sqlMessage: error.sqlMessage,
                    devData: {
                        queryFunction: "deletePromise",
                        table: table,
                        params: params,
                        date: new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData), "./logs/query.txt");
                reject(new Error("Sql error deletePromise"))
            } else {
                resolve({error: false})
            }
        });
    })
}

function findByToken(token){
    return new Promise(function (resolve, reject) {
        var prepareSql = 'SELECT `id` FROM  `users_db` WHERE `token`=' + db.escape(token) + ' LIMIT 1';
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findByToken",
                        table:'users_db',
                        params:{token:token},
                        filds:"*",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/checkUser.txt")
                reject(new Error("Sql error "+error.sqlMessage+""))
            }else{
                resolve(rows)
            }
        })
    })
}

function findByTokenMulty(table,token){
    return new Promise(function (resolve, reject) {
        var prepareSql = "SELECT `id` FROM  " + table + " WHERE `token`=" + db.escape(token);
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findByToken",
                        table:'users_db',
                        params:{token:token},
                        filds:"*",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/checkUser.txt")
                reject(new Error("Sql error "+error.sqlMessage+""))
            }else{
                resolve(rows)
            }
        })
    })
}

function getUserOrderInfo(order_id){
    return new Promise(function (resolve, reject) {
        var prepareSql = "" +
            " SELECT `order_info`.`email`,`order_info`.`name`,`order_info`.`last_name`,`order_info`.`phone`,`order_info`.`address`,`cities_lang`.`name` as city " +
            " FROM  `order_info`" +
            " JOIN  `cities_lang` ON `cities_lang`.`cities_id` = `order_info`.`cities_id` AND `cities_lang`.`lang_id` = 1 " +
            " WHERE `order_info`.`order_id` = "+db.escape(order_id);
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"getUserOrderInfo",
                        table:'order_info',
                        filds:"*",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/checkUser.txt")
                reject(new Error("Sql error "+error.sqlMessage+""))
            }else{
                resolve(rows)
            }
        })
    })
}

function findByGuestToken(token){
    return new Promise(function (resolve, reject) {
        var prepareSql = 'SELECT `id` FROM  `guest` WHERE `token`=' + db.escape(token) + ' LIMIT 1';
        db.query(prepareSql, function (error, rows, fields) {
            if (error) {
                var errorData = {
                    code: error.code,
                    errno: error.errno,
                    sqlMessage: error.sqlMessage,
                    devData: {
                        queryFunction: "findByGuestToken",
                        table: 'guest',
                        params: {token:token},
                        filds: "*",
                        date: new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/checkUser.txt")
                reject(new Error("Sql error findByGuestToken"))
            } else {
                resolve(rows)
            }
        })
    })
}

function updatePromise(table,post){
    return new Promise(function (resolve, reject) {
        var val = [];
        for(var key in post.values){
            val.push("`"+key +"`="+db.escape(post.values[key]));
        }
        var whereVal = [];
        for(var key in post.where){
            whereVal.push("`"+key+"`" +"="+db.escape(post.where[key]));
        }
        var prepareSql = "UPDATE "+ table + " SET "+val.toString()+" WHERE "+whereVal.join(' AND ')+"";
        // console.log('updatePromise',prepareSql)
        var query = db.query(prepareSql, post, function (error, results, fields) {
            if (error){
                var errorData = {
                    code: error.code,
                    errno: error.errno,
                    sqlMessage: error.sqlMessage,
                    devData: {
                        queryFunction: "updatePromise",
                        table: table,
                        params: post,
                        date: new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData), "./logs/query.txt");
                reject(new Error("Sql error updatePromise"));
            }else{
                resolve(results)
            }
        });
    })
}

var update2v = function(table,post,cb){
    var val = [];
    for(var key in post.values){
        val.push("`"+key +"`="+db.escape(post.values[key]));
    }
    var whereVal = [];
    for(var key in post.where){
        whereVal.push("`"+key+"`" +"="+db.escape(post.where[key]));
    }
    var prepareSql = "UPDATE "+ table + " SET "+val.toString()+" WHERE "+whereVal.join(' AND ')+"";
    var query = db.query(prepareSql, post, function (error, results, fields) {
        if (error){
            var errorData = {
                code: error.code,
                errno: error.errno,
                sqlMessage: error.sqlMessage,
                devData: {
                    queryFunction: "update2v",
                    table: table,
                    params: post,
                    date: new Date()
                }
            }
            log.insertLog(JSON.stringify(errorData),"./logs/query.txt")
            cb(true,errorData);
        }else{
            cb(false,results)
        }
    });
}

var updateMultiple = function(table,post,cb){

    var prepareSql = [];

    for(var i = 0; i < post.length; i++){
        prepareSql.push("UPDATE  `products`  SET  `status_buy` = "+db.escape(post[i].set.status_buy)+" "+
            " WHERE `id`  =  "+ db.escape(post[i].where.id) +"");
    }
    var query = db.query(prepareSql.join(";"), function (error, results, fields) {
        if (error){
            var errorData = {
                code: error.code,
                errno: error.errno,
                sqlMessage: error.sqlMessage,
                devData: {
                    queryFunction: "update2v",
                    table: table,
                    date: new Date()
                }
            }
            log.insertLog(JSON.stringify(errorData),"./logs/query.txt")
            cb(true,errorData);
        }else{
            cb(false,results)
        }
    });
}
var findAllTranslate = function(lang_id){
    return new Promise(function (resolve, reject) {
        var prepareSql = "" +
            " SELECT `translates`.`key_name` as word_key, `translates_language`.`name` as word" +
            " FROM `translates_language`" +
            " JOIN `translates` ON `translates_language`.`translates_id` = `translates`.`id` " +
            " WHERE `translates_language`.`lang_id` = "+db.escape(lang_id)+"";
        db.query(prepareSql, function (error, result) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findAllByFilds",
                        table:'findAllTranslate',
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/query.txt")
                reject(new Error("Sql error findAllPromise"))
            }else{
                resolve(result)
            }
        })
    })
}
var findAllCategories = function(lang_id,API_URL){
    return new Promise(function (resolve, reject) {
        var prepareSql = "" +
            " SELECT `categories_language`.`name` as name,`categories_language`.`id` as catId," +
            " CONCAT('"+API_URL+"','/images/categories/',`categories`.`image`) as image, " +
            " CONCAT('"+API_URL+"','/images/categories/',`categories`.`effect_image`) as effect_image " +
            " FROM `categories`" +
            " JOIN `categories_language` ON `categories_language`.`cat_id` = `categories`.`id`  AND `categories_language`.`lang_id` = "+lang_id+"";

        db.query(prepareSql, function (error, result) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findAllCategories",
                        table:'findAllCategories',
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/query.txt")
                reject(new Error("Sql error findAllPromise"))
            }else{
                resolve(result)
            }
        })
    })
}
var findAllCategoriesWithBrands = function(lang_id,API_URL){
    return new Promise(function (resolve, reject) {
        var prepareSql = "" +
            " SELECT " +
            " `categories`.`id` as catId,`categories`.`slug` as catSlug," +
            " CONCAT('"+API_URL+"','/images/categories/',`categories`.`image`) as catImage, " +
            " CONCAT('"+API_URL+"','/images/categories/',`categories`.`effect_image`) as catEffectImage, " +
            " `categories`.`ord` as catOrder,`categories_language`.`name` as catName,`brand`.`cat_id` as brandCatId," +
            " GROUP_CONCAT(DISTINCT CONCAT('"+API_URL+"','/images/brands/',`brand`.`image`) ORDER BY `brand`.`id` ASC SEPARATOR '||') AS brandImage," +
            " GROUP_CONCAT(DISTINCT CONCAT('"+API_URL+"','/images/brands/',`brand`.`brand_logo`)  ORDER BY `brand`.`id` ASC SEPARATOR '||') AS brandLogo,"+
            " `brand`.`slug` as brandSlug,`brand`.`ord` as brandOrd," +
            " GROUP_CONCAT(DISTINCT `brand`.`slug`  ORDER BY `brand`.`id` ASC SEPARATOR '||') AS brandSlug, " +
            " GROUP_CONCAT(DISTINCT `brand_language`.`name` ORDER BY `brand`.`id` ASC SEPARATOR '||' ) AS brandName, " +
            " GROUP_CONCAT(DISTINCT `brand_language`.`title` ORDER BY `brand`.`id` ASC SEPARATOR '||' ) AS brandTitle " +
            " FROM `categories`" +
            " JOIN `categories_language` ON `categories_language`.`cat_id` = `categories`.`id`  AND `categories_language`.`lang_id` = "+db.escape(lang_id)+""+
            " LEFT JOIN `brand` ON `brand`.`cat_id` = `categories`.`id` " +
            " LEFT JOIN `brand_language` ON `brand_language`.`brand_id` = `brand`.`id`  AND `brand_language`.`lang_id` = "+db.escape(lang_id)+"" +
            " GROUP BY `categories`.`id` ORDER BY `categories`.`ord` ASC";
        // console.log('prepareSql',prepareSql)
        db.query(prepareSql, function (error, result) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findAllCategoriesWithBrands",
                        table:'findAllCategoriesWithBrands',
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/query.txt")
                reject(new Error("Sql error findAllCategoriesWithBrands"))
            }else{
                resolve(result)
            }
        })
    })
}
var findAllLitrages = function(){
    return new Promise(function (resolve, reject) {
        var prepareSql = "" +
            " SELECT " +
            " `litrages`.`id` as id,`litrages`.`name` as name " +
            " FROM `litrages`" +
            " ORDER BY `litrages`.`ord` ASC";
        // console.log('prepareSql',prepareSql)
        db.query(prepareSql, function (error, result) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findAllLitrages",
                        table:'findAllLitrages',
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/query.txt")
                reject(new Error("Sql error findAllLitrages"))
            }else{
                resolve(result)
            }
        })
    })
}
var findAllSlide = function(lang_id,API_URL){
    return new Promise(function (resolve, reject) {
        var prepareSql = "" +
            " SELECT `home_slide`.`url` as buttonUrl, " +
            " `home_slide_language`.`title` as buttonTitle,`home_slide_language`.`button_text` as buttonText," +
            " CONCAT('"+API_URL+"','/images/home/slide/',`home_slide`.`image`) as image " +
            " FROM `home_slide`" +
            " JOIN `home_slide_language` ON `home_slide_language`.`home_slide_id` = `home_slide`.`id`  AND `home_slide_language`.`lang_id` = "+lang_id+"";
        db.query(prepareSql, function (error, result) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findAllSlide",
                        table:'home_slide',
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/query.txt")
                reject(new Error("Sql error findAllSlide"))
            }else{
                resolve(result)
            }
        })
    })
}
var findAllLoginSlide = function(lang_id,API_URL){
    return new Promise(function (resolve, reject) {
        var prepareSql = "" +
            " SELECT  " +
            " `login_slide_language`.`name` as name," +
            " CONCAT('"+API_URL+"','/images/home/slide/',`login_slide`.`image`) as image " +
            " FROM `login_slide`" +
            " JOIN `login_slide_language` ON `login_slide_language`.`login_slide_id` = `login_slide`.`id`  AND `login_slide_language`.`lang_id` = "+lang_id+"";
        // console.log('prepareSql', prepareSql)
        db.query(prepareSql, function (error, result) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findAllLoginSlide",
                        table:'findAllTranslate',
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/query.txt")
                reject(new Error("Sql error findAllPromise"))
            }else{
                resolve(result)
            }
        })
    })
}
/**
 *
 * @param table
 * @param post {fildname:value}
 * @param cb function(){}
 */
var deletes = function(table,post,cb){
    var val = [];
    for(var key in post){
        if(post[key] !== "NULL") {
            val.push("`" + key + "`=" + db.escape(post[key]));
        }else{
            val.push("`" + key + "` IS NULL");
        }
    }
    var prepareSql = 'DELETE FROM ' + table + ' WHERE '+val.join(' AND ')+'';
    db.query(prepareSql,post,function(error,result,fields){
        if (error){
            var errorData = {
                code: error.code,
                errno: error.errno,
                sqlMessage: error.sqlMessage,
                devData: {
                    queryFunction: "deletes",
                    table: table,
                    params: post,
                    date: new Date()
                }
            }
            log.insertLog(JSON.stringify(errorData),"./logs/query.txt")
            cb(true,errorData);
        }else{
            cb(false,result)
        }
    });
}

var deletesPromise = function(table,post){
    return new Promise(function (resolve, reject) {
        var val = [];
        for (var key in post) {
            if (post[key] !== "NULL") {
                val.push("`" + key + "`=" + db.escape(post[key]));
            } else {
                val.push("`" + key + "` IS NULL");
            }
        }
        var prepareSql = 'DELETE FROM ' + table + ' WHERE ' + val.join(' AND ') + '';
        db.query(prepareSql, post, function (error, result, fields) {
            if (error) {
                var errorData = {
                    code: error.code,
                    errno: error.errno,
                    sqlMessage: error.sqlMessage,
                    devData: {
                        queryFunction: "deletesPromise",
                        table: table,
                        params: post,
                        date: new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData), "./logs/query.txt")
                resolve(errorData);
            } else {
                resolve(result)
            }
        });
    })
}

var deletesMultyRowPromise = function(table,filed,values){
    return new Promise(function (resolve, reject) {
        var prepareSql = 'DELETE FROM ' + table + ' WHERE '+filed+' IN(' + values.join(' , ') + ')';
        // console.log('prepareSql',prepareSql)
        db.query(prepareSql, function (error, result, fields) {
            if (error) {
                var errorData = {
                    code: error.code,
                    errno: error.errno,
                    sqlMessage: error.sqlMessage,
                    devData: {
                        queryFunction: "deletesPromise",
                        table: table,
                        filed: filed,
                        values: values,
                        date: new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData), "./logs/query.txt")
                resolve(errorData);
            } else {
                resolve(result)
            }
        });
    })
}

var getCartInfoByUserOrGuest = function(selectInfo,langId){
    return new Promise(function (resolve, reject) {
        var prepareSql = 'SELECT `cart`.`quantity`,`product`.`id`,`product`.`in_stock`,`product`.`price`,`product`.`in_stock`,`product`.`price_before_sale`,`product_language`.`name`,`categories_language`.`name` as catName, ' +
            " CONCAT('"+static.API_URL+"','/images/product/',`product`.`image`) as image " +
            ' FROM `cart` ' +
            ' JOIN  `product` ON `product`.`id` = `cart`.`product_id` ' +
            ' JOIN `product_language` ON `product_language`.`product_id` = `product`.`id` AND `product_language`.`lang_id` = '+db.escape(langId)+'' +
            ' JOIN `brand` ON `brand`.`id` = `product`.`brand_id` ' +
            ' JOIN `categories` ON `categories`.`id` = `brand`.`cat_id` ' +
            ' JOIN `categories_language` ON `categories_language`.`cat_id` = `categories`.`id` AND `categories_language`.`lang_id` = '+db.escape(langId)+'' +
            ' WHERE `cart`.`'+selectInfo.field+'` = "'+db.escape(selectInfo.id)+'" '
        // console.log('prepareSql',prepareSql)
        db.query(prepareSql, function (error, result, fields) {
            if (error) {
                var errorData = {
                    code: error.code,
                    errno: error.errno,
                    sqlMessage: error.sqlMessage,
                    devData: {
                        queryFunction: "getCartInfoByUserOrGuest",
                        date: new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData), "./logs/query.txt")
                resolve(errorData);
            } else {
                resolve(result)
            }
        });
    })
}

var getCartCheckoutInfoByUserOrGuest = function(selectInfo,langId){
    return new Promise(function (resolve, reject) {
        var prepareSql = 'SELECT `cart`.`quantity`,`product`.`id`,`product`.`in_stock`,`product`.`price`,`product`.`in_stock`,`product`.`price_before_sale`,`product_language`.`name`,`categories_language`.`name` as catName, ' +
            " CONCAT('"+static.API_URL+"','/images/product/',`product`.`image`) as image " +
            ' FROM `cart` ' +
            ' JOIN  `product` ON `product`.`id` = `cart`.`product_id` AND `product`.`in_stock` = "0"' +
            ' JOIN `product_language` ON `product_language`.`product_id` = `product`.`id` AND `product_language`.`lang_id` = '+db.escape(langId)+'' +
            ' JOIN `brand` ON `brand`.`id` = `product`.`brand_id` ' +
            ' JOIN `categories` ON `categories`.`id` = `brand`.`cat_id` ' +
            ' JOIN `categories_language` ON `categories_language`.`cat_id` = `categories`.`id` AND `categories_language`.`lang_id` = '+db.escape(langId)+'' +
            ' WHERE `cart`.`'+selectInfo.field+'` = "'+db.escape(selectInfo.id)+'" '
        // console.log('prepareSql',prepareSql)
        db.query(prepareSql, function (error, result, fields) {
            if (error) {
                var errorData = {
                    code: error.code,
                    errno: error.errno,
                    sqlMessage: error.sqlMessage,
                    devData: {
                        queryFunction: "getCartInfoByUserOrGuest",
                        date: new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData), "./logs/query.txt")
                resolve(errorData);
            } else {
                resolve(result)
            }
        });
    })
}

var getFavouriteCount = function(selectInfo){
    return new Promise(function (resolve, reject) {
        var prepareSql = 'SELECT COUNT(`favorite`.`id`) as fav_count' +
            ' FROM `favorite` ' +
            ' WHERE `favorite`.`'+selectInfo.field+'` = "'+db.escape(selectInfo.id)+'" '
        // console.log('prepareSql',prepareSql)
        db.query(prepareSql, function (error, result, fields) {
            if (error) {
                var errorData = {
                    code: error.code,
                    errno: error.errno,
                    sqlMessage: error.sqlMessage,
                    devData: {
                        queryFunction: "getFavouriteCount",
                        date: new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData), "./logs/query.txt")
                resolve(errorData);
            } else {
                resolve(result)
            }
        });
    })
}

var findFavourite = function(field,vals,langId){
    return new Promise(function (resolve, reject) {
        var prepareSql = 'SELECT ' +
            ' `product`.`price`,`product`.`in_stock`,`product`.`slug`,`product`.`price_before_sale`,`product`.`sale_percent`,`product`.`id`,`product`.`price`,`product_language`.`name`,`product_language`.`description`,' +
            " CONCAT('"+static.API_URL+"','/images/product/',`product`.`image`) as image," +
            " CONCAT('"+static.API_URL+"','/images/product/',`product`.`hover_image`) as hoverImage" +
            ' FROM `favorite` ' +
            ' JOIN `product` ON `product`.`id` = `favorite`.`product_id` ' +
            ' JOIN `product_language` ON `product_language`.`product_id` = `product`.`id` AND `product_language`.`lang_id` = '+db.escape(langId)+' ' +
            ' WHERE `favorite`.`'+field+'` = "'+db.escape(vals)+'" '
        // console.log('prepareSql',prepareSql)
        db.query(prepareSql, function (error, result, fields) {
            if (error) {
                var errorData = {
                    code: error.code,
                    errno: error.errno,
                    sqlMessage: error.sqlMessage,
                    devData: {
                        queryFunction: "findFavourite",
                        date: new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData), "./logs/query.txt")
                resolve(errorData);
            } else {
                result.forEach(function (item) {
                    item.price = staticMethods.formatMoney(item.price);
                    item.price_before_sale = staticMethods.formatMoney(item.price_before_sale);
                })
                resolve(result)
            }
        });
    })
}

var findAllCountries = function(langId){
    return new Promise(function (resolve, reject) {
        var prepareSql = 'SELECT ' +
            ' `countries_lang`.`name`,`countries`.`alcohol_age`,`countries`.`id`' +
            ' FROM `countries` ' +
            ' JOIN `countries_lang` ON `countries_lang`.`countries_id` = `countries`.`id` AND `countries_lang`.`lang_id` = '+db.escape(langId);
        // console.log('prepareSql',prepareSql)
        db.query(prepareSql, function (error, result, fields) {
            if (error) {
                var errorData = {
                    code: error.code,
                    errno: error.errno,
                    sqlMessage: error.sqlMessage,
                    devData: {
                        queryFunction: "findAllCountries",
                        date: new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData), "./logs/query.txt")
                resolve(errorData);
            } else {
                resolve(result)
            }
        });
    })
}
var findAllStates = function(countryId,langId){
    return new Promise(function (resolve, reject) {
        var prepareSql = 'SELECT ' +
            ' `states_lang`.`name`,`states`.`id`' +
            ' FROM `states` ' +
            ' JOIN `states_lang` ON `states_lang`.`states_id` = `states`.`id` AND `states_lang`.`lang_id` = '+db.escape(langId)+'' +
            ' WHERE `states`.`country_id` = '+db.escape(countryId)+'';
        // console.log('prepareSql',prepareSql)
        db.query(prepareSql, function (error, result, fields) {
            if (error) {
                var errorData = {
                    code: error.code,
                    errno: error.errno,
                    sqlMessage: error.sqlMessage,
                    devData: {
                        queryFunction: "findAllStates",
                        date: new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData), "./logs/query.txt")
                resolve(errorData);
            } else {
                resolve(result)
            }
        });
    })
}

var findAllCities = function(countryId,langId){
    return new Promise(function (resolve, reject) {
        var prepareSql = 'SELECT ' +
            ' `cities_lang`.`name`,`cities`.`id`' +
            ' FROM `cities` ' +
            ' JOIN `cities_lang` ON `cities_lang`.`cities_id` = `cities`.`id` AND `cities_lang`.`lang_id` = '+db.escape(langId)+'' +
            ' WHERE `cities`.`state_id` = '+db.escape(countryId)+'';
        // console.log('prepareSql',prepareSql)
        db.query(prepareSql, function (error, result, fields) {
            if (error) {
                var errorData = {
                    code: error.code,
                    errno: error.errno,
                    sqlMessage: error.sqlMessage,
                    devData: {
                        queryFunction: "findAllCities",
                        date: new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData), "./logs/query.txt")
                resolve(errorData);
            } else {
                resolve(result)
            }
        });
    })
}


module.exports.findAllCountries = findAllCountries;
module.exports.findAllCities = findAllCities;
module.exports.findAllStates = findAllStates;
module.exports.findFavourite = findFavourite;
module.exports.getFavouriteCount = getFavouriteCount;
module.exports.deletesMultyRowPromise = deletesMultyRowPromise;
module.exports.getCartInfoByUserOrGuest = getCartInfoByUserOrGuest;
module.exports.findAllCategories = findAllCategories;
module.exports.findAllCategoriesWithBrands = findAllCategoriesWithBrands;
module.exports.findAllLitrages = findAllLitrages;
module.exports.findAllSlide = findAllSlide;
module.exports.findAllTranslate = findAllTranslate;
module.exports.findAllByFilds = findAllByFilds;
module.exports.findAllByFildsPromise = findAllByFildsPromise;
module.exports.findAllPromise = findAllPromise;
module.exports.findByMultyName = findByMultyName;
module.exports.findByMultyNamePromise = findByMultyNamePromise;
module.exports.update2v = update2v;
module.exports.updatePromise = updatePromise;
module.exports.deletes = deletes;
module.exports.deletesPromise = deletesPromise;
module.exports.insert2vPromise = insert2vPromise;
module.exports.insertLoopPromise = insertLoopPromise;
module.exports.deletePromise = deletePromise;
module.exports.findByToken = findByToken;
module.exports.findByGuestToken = findByGuestToken;
module.exports.findByTokenMulty = findByTokenMulty;
module.exports.getUserOrderInfo = getUserOrderInfo;
module.exports.updateMultiple = updateMultiple;
module.exports.findAllG = findAllG;
module.exports.updateLastLogin = updateLastLogin;
module.exports.findAllLoginSlide = findAllLoginSlide;
module.exports.findFaq = findFaq;
module.exports.findNews = findNews;
module.exports.findLatestNews = findLatestNews;
module.exports.findNewsById = findNewsById;
module.exports.findAllNewsCount = findAllNewsCount;
module.exports.getCartCheckoutInfoByUserOrGuest = getCartCheckoutInfoByUserOrGuest;
module.exports.findUserInfo = findUserInfo;
