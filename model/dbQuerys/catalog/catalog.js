const db = require('../../connection');
var log = require("../../../logs/log");
const statics = require('../../../static');
const staticMethods = require('../../../model/staticMethods');

getCatalogProducts = (langId,products) => {

}

findCatalog2V = (page,categories,brand,litrage,filters,price,langId,userType) => {
    var paginationData = staticMethods.detectPagination(page);
    // console.log('paginationData',paginationData)
    return new Promise((resolve, reject) => {
        var prepareSql = "";
        var catJoin = '';
        var brandJoin = '';
        //var litrageJoin = '';
        var litrageWhere = '';
        if(categories.length > 0){
            catJoin += " AND `categories`.`id` IN ("+categories.join(',')+")"
        }
        if(brand.length > 0){
            var brandStr = "";
            if(brand.length == 1){
                brandStr += "'"+brand[0]+"'"
            }else{
                for(var i = 0; i < brand.length; i++){
                    if(i == 0){
                        brandStr += "'"+brand[i]+"',"
                    }else if(i == brand.length-1){
                        brandStr += "'"+brand[i]+"'"
                    }else{
                        brandStr += "'"+brand[i]+"',"
                    }
                }
            }

            brandJoin += " AND `brand`.`slug` IN ("+brandStr+")"
        }
        if(litrage.length > 0){
        //     litrageJoin += " AND `litrages`.`id` IN ("+litrage.join(',')+")"
            litrageWhere = "AND `product`.`litrage_id` IN("+litrage.join(',')+")";
        }


        var priceWhere = " AND `product`.`price` BETWEEN "+price.min+" and "+price.max+" ";
        var filtersWhereMain = " WHERE `product`.`price` BETWEEN "+price.min+" and "+price.max+" AND ";
        let filterJoin = [];
        let filterWhere = [];
        for (let i = 0 ;  i < filters.length; i++){
            if(i === 0){
                filterWhere.push(" `t1`.`filter_prop_id` IN ("+filters[i].join(',')+")");
            }else {
                filterJoin.push("JOIN `filter_product` `k"+i+"` ON `t1`.`product_id` = `k"+i+"`.`product_id` ");
                filterWhere.push(" `k"+i+"`.`filter_prop_id` IN ("+filters[i].join(',')+")");
            }
        }
        filtersWhereMain+=""+filterWhere.join(' AND ');
        if(filters.length === 0){
            filtersWhereMain = "";
            priceWhere = " WHERE `product`.`price` BETWEEN "+price.min+" and "+price.max+" ";
        }

        prepareSql += "" +
            " SELECT `t1`.`product_id`,`favorite`.`id` as favId, " +
            "  `product`.`price`,`product`.`slug`,`product`.`in_stock`,`product`.`price_before_sale`,`product`.`sale_percent`,`product`.`price`,`product_language`.`name`,`product_language`.`description`," +
            " CONCAT('"+statics.API_URL+"','/images/product/',`product`.`image`) as image," +
            " CONCAT('"+statics.API_URL+"','/images/product/',`product`.`hover_image`) as hoverImage" +
            " FROM `filter_product` `t1`" +
            " "+
            " "+filterJoin.join('')+
            " RIGHT JOIN `product` ON `product`.`id` = `t1`.`product_id`" +
            " LEFT JOIN `favorite` ON `favorite`.`product_id` = `product`.`id` AND `favorite`.`"+userType.field+"` = "+db.escape(userType.val)+" " +
           // " RIGHT JOIN `litrages` ON `litrages`.`id` = `product`.`litrage_id` "+litrageJoin +
            " RIGHT JOIN `brand` ON `brand`.`id` = `product`.`brand_id` "+brandJoin +
            " RIGHT JOIN `categories` ON `categories`.`id` = `brand`.`cat_id` "+catJoin+
            " JOIN `product_language` ON `product_language`.`product_id` = `t1`.`product_id` AND `product_language`.`lang_id` = "+ db.escape(langId) +""+
            filtersWhereMain+
            priceWhere+
            litrageWhere+
            " GROUP BY `t1`.`product_id`"+
            " LIMIT "+paginationData.start+","+paginationData.limit+"";
        console.log('..................................prepareSql........................',prepareSql)

        db.query(prepareSql, function (error, result) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"filter_product",
                        table:'product',
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/catalog.txt")
                reject(new Error("Sql error findCatalog2V"))
            }else{
                result.forEach(function (item) {
                    item.price = staticMethods.formatMoney(item.price);
                    if(item.price_before_sale !== null){
                        item.price_before_sale = staticMethods.formatMoney(item.price_before_sale);
                    }
                })
                console.log('..................................result........................',result)
                resolve(result)
            }
        })
    })

}


var findCatalog = (categories,brand,filters,price,langId) => {

    var categoriesSELECT = "";
    var categoriesJOIN = "";

    let brandSELECTFields = [];
    var brandSELECT = brandSELECTFields.length > 0 ? brandSELECTFields.join(',') : ""
    var brandJOIN = "";

    let filtersSELECTFields = [];
    var filtersSELECT = filtersSELECTFields.length > 0 ? filtersSELECTFields.join(',') : ""
    var filtersJOIN = "";



    // console.log('categories',categories)
    // console.log('brand',brand)
    // console.log('filters',filters)
    // console.log('price',price)

    for(let i = 0; i < categories.length; i++){

    }
    if(brand.length > 0){
        var brandIds = [];

        for(let i = 0; i < brand.length; i++){
            let splitItem = brand[i].split('-');
            brandIds.push(splitItem[splitItem.length - 1])
        }
        brandJOIN += " JOIN `brand` ON  (`brand`.`id` IN ("+brandIds.join(',')+")) AND `brand`.`id` = `product`.`brand_id`  ";
    }

    if(filters.length > 0){
        filtersJOIN += " JOIN `filter_product` ON  (`filter_product`.`filter_prop_id` IN ("+filters.join(',')+")) AND `filter_product`.`product_id` = `product`.`id` " +
            "JOIN `brand_language` ON `brand_language`.`brand_id` = `brand`.`id` AND `brand_language`.`lang_id` = "+db.escape(langId)+" ";
    }

    var whereQuery = "";
    if(typeof price.min !== "undefined" && typeof price.max !== "undefined"){
        whereQuery = "WHERE `product`.`price` > "+price.min+" AND `product`.`price` < "+price.max+""
    }

    // console.log('brandSELECT',brandSELECT)
    // console.log('brandJOIN',brandJOIN)
    // console.log('filtersJOIN',filtersJOIN)


    return new Promise(function (resolve, reject) {
        var prepareSql = "" +
            " SELECT " +
            " `product_language`.`name` as productName " +
            " CONCAT('"+statics.API_URL+"','/public/images/product/',`product`.`image`) as productImage, " +
            brandSELECT +
            filtersSELECT +
            " FROM `product`" +
            brandJOIN +
            filtersJOIN +
            " JOIN `filter_prop_language` ON `filter_prop_language`.`filter_prop_id` = `filter_product`.`filter_prop_id` AND `filter_prop_language`.`lang_id` = "+db.escape(langId)+"" +

            " " +
            " JOIN `product_language` ON `product_language`.`product_id` = `product`.`id` AND `product_language`.`lang_id` = "+db.escape(langId)+" " +
            whereQuery +
            "GROUP BY `product`.`id`";
        // console.log('prepareSql',prepareSql)
        // db.query(prepareSql, function (error, result) {
        //     if (error){
        //         var errorData = {
        //             code:error.code,
        //             errno:error.errno,
        //             sqlMessage:error.sqlMessage,
        //             devData:{
        //                 queryFunction:"getAllFilters",
        //                 table:'filter',
        //                 date:new Date()
        //             }
        //         }
        //         log.insertLog(JSON.stringify(errorData),"./logs/v1/filter.txt")
        //         reject(new Error("Sql error getAllFilters"))
        //     }else{
        //         resolve(result)
        //     }
        // })
    })
}


var findCatalogWithoutFilters = (page,cat,brand,litrage,price,langId,userType) => {
    var litrageWhere = "";
    var catJOIN = "";
    var brandJOIN = "";

    if(cat.length > 0){
        var catStr = "";
        if(cat.length == 1){
            catStr += "'"+cat[0]+"'"
        }else{
            for(var i = 0; i < cat.length; i++){
                if(i == 0){
                    catStr += "'"+cat[i]+"',"
                }else if(i == cat.length-1){
                    catStr += "'"+cat[i]+"'"
                }else{
                    catStr += "'"+cat[i]+"',"
                }
            }
        }
        catJOIN += " JOIN `categories` ON `categories`.`slug` IN ("+catStr+")"
        catJOIN += " AND `categories`.`id` = `product`.`cat_id`  ";
    }

    if(brand.length > 0){
        var brandStr = "";
        if(brand.length == 1){
            brandStr += "'"+brand[0]+"'"
        }else{
            for(var i = 0; i < brand.length; i++){
                if(i == 0){
                    brandStr += "'"+brand[i]+"',"
                }else if(i == brand.length-1){
                    brandStr += "'"+brand[i]+"'"
                }else{
                    brandStr += "'"+brand[i]+"',"
                }
            }
        }
        brandJOIN += " JOIN `brand` ON `brand`.`slug` IN ("+brandStr+")"
        brandJOIN += " AND `brand`.`id` = `product`.`brand_id`  ";
    }

    var whereQuery = "";
    if(typeof price.min !== "undefined" && typeof price.max !== "undefined"){
        whereQuery = " WHERE `product`.`price` BETWEEN "+price.min+" and "+price.max+" "
    }
    if(litrage.length > 0){
        if(whereQuery === ""){
            litrageWhere = " WHERE `product`.`litrage_id` IN(" + litrage.join(',') + ") ";
        }else {
            litrageWhere = " AND `product`.`litrage_id` IN(" + litrage.join(',') + ") ";
        }
    }

    return new Promise(function (resolve, reject) {
        var prepareSql = "" +
            " SELECT `favorite`.`id` as favId," +
            "  `product`.`price`,`product`.`slug`,`product`.`in_stock`,`product`.`price_before_sale`,`product`.`sale_percent`,`product`.`price`,`product_language`.`name`,`product_language`.`description`," +
            " CONCAT('"+statics.API_URL+"','/images/product/',`product`.`image`) as image," +
            " CONCAT('"+statics.API_URL+"','/images/product/',`product`.`hover_image`) as hoverImage" +
            " FROM `product`" +
            catJOIN +
            brandJOIN +
            " " +
            " JOIN `product_language` ON `product_language`.`product_id` = `product`.`id` AND `product_language`.`lang_id` = "+db.escape(langId)+" " +
            " LEFT JOIN `favorite` ON `favorite`.`product_id` = `product`.`id` AND `favorite`.`"+userType.field+"` = "+db.escape(userType.val)+" " +
            whereQuery + litrageWhere +
            "GROUP BY `product`.`id`";
        console.log(':::prepareSql',prepareSql)
        db.query(prepareSql, function (error, result) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findCatalogWithoutFilters",
                        table:'product',
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/product.txt")
                reject(new Error("Sql error findCatalogWithoutFilters"))
            }else{
                resolve(result)
            }
        })
    })
}



var getAllCatalog = function(price,langId,page,userType){
    var paginationData = staticMethods.detectPagination(page);

    var where = '';
    if(price.min !== null && price.max !== null){
        where = " WHERE `product`.`price` BETWEEN "+price.min+" and "+price.max+" "
    }

    return new Promise(function (resolve, reject) {
        var prepareSql = "" +
            " SELECT `favorite`.`id` as favId,`product`.`slug`,`product`.`id` as product_id,`product`.`price`,`product`.`in_stock`,`product`.`price_before_sale`,`product_language`.`name`,`product_language`.`description`," +
            " CONCAT('"+statics.API_URL+"','/images/product/',`product`.`image`) as image," +
            " CONCAT('"+statics.API_URL+"','/images/product/',`product`.`hover_image`) as hoverImage" +
            " FROM `product` " +
            " LEFT JOIN `favorite` ON `favorite`.`product_id` = `product`.`id` AND `favorite`.`"+userType.field+"` = "+db.escape(userType.val)+" " +
            " JOIN `product_language` ON `product_language`.`product_id` = `product`.`id` AND `product_language`.`lang_id` = "+db.escape(langId)+"" +
            where+" LIMIT "+paginationData.start+","+paginationData.limit+"";
        console.log('prepareSql',prepareSql)
        db.query(prepareSql, function (error, result) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"getAllCatalog",
                        table:'product',
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/catalog.txt")
                reject(new Error("Sql error getCatalog"))
            }else{
                result.forEach(function (item) {
                    item.price = staticMethods.formatMoney(item.price);
                    if(item.price_before_sale !== null){
                        item.price_before_sale = staticMethods.formatMoney(item.price_before_sale);
                    }
                })
                resolve(result)
            }
        })
    })
}


module.exports.findCatalog = findCatalog;
module.exports.getAllCatalog = getAllCatalog;
module.exports.findCatalog2V = findCatalog2V;
module.exports.getCatalogProducts = getCatalogProducts;
module.exports.findCatalogWithoutFilters = findCatalogWithoutFilters;