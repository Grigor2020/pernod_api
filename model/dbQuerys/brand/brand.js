const db = require('../../connection');
var log = require("../../../logs/log");
const static = require('../../../static');
const staticMethods = require('../../../model/staticMethods');



var findBrand = function(brandId,langId){
    // console.log('paginationData',paginationData)
    return new Promise(function (resolve, reject) {
        var prepareSql = "" +
            " SELECT `brand`.`slug`,`brand`.`id`,`brand_language`.`name`,`brand_language`.`title`,`brand_language`.`text`," +
            " CONCAT('"+static.API_URL+"','/images/brands/',`brand`.`image`) as coverImage," +
            " CONCAT('"+static.API_URL+"','/images/brands/',`brand`.`brand_logo`) as logo" +
            " FROM `brand` " +
            " JOIN `brand_language` ON `brand_language`.`brand_id` = `brand`.`id` AND `brand_language`.`lang_id` = "+db.escape(langId)+"" +
            " WHERE `brand`.`slug`= "+db.escape(brandId)+"";
        // console.log('prepareSql',prepareSql)
        db.query(prepareSql, function (error, result) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findBrand",
                        table:'product',
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(findBrand),"./logs/v1/brand.txt")
                reject(new Error("Sql error findBrand"))
            }else{
                resolve(result)
            }
        })
    })
}
var findBrandIdByslug = function(slug){
    // console.log('paginationData',paginationData)
    return new Promise(function (resolve, reject) {
        var prepareSql = "" +
            " SELECT `brand`.`id`"+
            " FROM `brand` " +
            " WHERE `brand`.`slug`= "+db.escape(slug)+"";
        // console.log('prepareSql',prepareSql)
        db.query(prepareSql, function (error, result) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findBrand",
                        table:'product',
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(findBrand),"./logs/v1/brand.txt")
                reject(new Error("Sql error findBrand"))
            }else{
                resolve(result)
            }
        })
    })
}


module.exports.findBrand = findBrand;
module.exports.findBrandIdByslug = findBrandIdByslug;