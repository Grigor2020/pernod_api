const db = require('../../connection');
var log = require("../../../logs/log");
const static = require('../../../static');
const staticMethods = require('../../../model/staticMethods');

var getAllProductCount = function(){
    return new Promise(function (resolve, reject) {
        var prepareSql = "" +
            " SELECT  COUNT(`product`.`id`) as productRow " +
            " FROM `product`";
        db.query(prepareSql, function (error, result) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"getAllProductCount",
                        table:'product',
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/product.txt")
                reject(new Error("Sql error getAllFilters"))
            }else{
                resolve(result[0].productRow)
            }
        })
    })
}
var getBroductByBrandId = function(langId,brandId,userType){
    return new Promise(function (resolve, reject) {
        var prepareSql = "" +
            " SELECT  `favorite`.`id` as favId,`product`.`id`, `product`.`slug`,`product`.`price`,`product`.`in_stock`,`product`.`price_before_sale`,`product_language`.`name`,`product_language`.`description`, " +
            " CONCAT('"+static.API_URL+"','/images/product/',`product`.`image`) as image," +
            " CONCAT('"+static.API_URL+"','/images/product/',`product`.`hover_image`) as hoverImage" +
            " FROM `product` " +
            " LEFT JOIN `favorite` ON `favorite`.`product_id` = `product`.`id` AND `favorite`.`"+userType.field+"` = "+db.escape(userType.val)+" " +
            " JOIN `product_language` ON `product_language`.`product_id` = `product`.`id` AND `product_language`.`lang_id` = "+db.escape(langId)+" " +
            " WHERE `product`.`brand_id` = "+db.escape(brandId)+"";
        // console.log('prepareSql',prepareSql)
        db.query(prepareSql, function (error, result) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"getBroductByBrandId",
                        table:'product',
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/product.txt")
                reject(new Error("Sql error getBroductByBrandId"))
            }else{
                result.forEach(function (item) {
                    if(item.price_before_sale !== null){
                        item.price_before_sale = staticMethods.formatMoney(item.price_before_sale);
                    }
                    item.price = staticMethods.formatMoney(item.price);
                })
                resolve(result)
            }
        })
    })
}

var findMinMaxPrice = () => {
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            'SELECT MAX(`product`.`price`) AS max_price ' +
            'FROM `product` ';
        db.query(prepareSql, function (error, result, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findMinMaxPrice",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/product.txt")
                reject(new Error("Sql error findMinMaxPrice: "+error.sqlMessage+""))
            }else{
                resolve(result[0])
            }
        })
    })
}

var getOneProduct = (prodId,langId,userType) => {
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            ' SELECT  `categories`.`id` as catId,' +
            ' `favorite`.`id` as favId,' +
            ' `product`.`id`,`product`.`image_top`,`product`.`in_stock`,`product`.`price`,`product`.`price_before_sale`,`product`.`sale_percent`,' +
            ' `product_language`.`name`,`product_language`.`description`,`product_language`.`seo_title`,`product_language`.`seo_desc`,' +
            ' `brand`.`slug` as brandSlug,`brand`.`slug` as brandSlug, ' +
            ' `brand_language`.`name` as brandName,' +
            " CONCAT('"+static.MAIN_URL+"','/product?id=',`product`.`slug`) as publicLink," +
            " CONCAT('"+static.API_URL+"','/images/brands/',`brand`.`brand_logo`) as brandLogo," +
            " CONCAT('"+static.API_URL+"','/images/product/',`product`.`image`) as image," +
            " CONCAT('"+static.API_URL+"','/images/product/',`product`.`cover_image`) as coverImage" +
            ' FROM `product` '+
            " JOIN `product_language` ON `product_language`.`product_id` = `product`.`id` AND `product_language`.`lang_id` = "+db.escape(langId)+" " +
            " JOIN `brand` ON `brand`.`id` = `product`.`brand_id` " +
            " JOIN `categories` ON `brand`.`cat_id` = `categories`.`id` " +
            " LEFT JOIN `favorite` ON `favorite`.`product_id` = `product`.`id` AND `favorite`.`"+userType.field+"` = "+db.escape(userType.val)+" " +
            " JOIN `brand_language` ON `brand_language`.`brand_id` = `brand`.`id` AND `brand_language`.`lang_id` = "+db.escape(langId)+" " +
            " WHERE `product`.`id` = "+db.escape(prodId)+"";
            db.query(prepareSql, function (error, result, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findMinMaxPrice",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/product.txt")
                reject(new Error("Sql error findMinMaxPrice: "+error.sqlMessage+""))
            }else{

                result.forEach(function (item) {
                    item.price = staticMethods.formatMoney(item.price);
                })
                resolve(result[0])
            }
        })
    })
}

var getRelativeProducts = (catId,prodId,langId,userType) => {
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            ' SELECT  `favorite`.`id` as favId,`product`.`id`,`product`.`image_top`,`product`.`in_stock`,`product`.`price`,`product`.`price_before_sale`,`product`.`sale_percent`,`product_language`.`name`,`product_language`.`description`,' +
            ' `product`.`slug` as slug,`brand`.`cat_id` as cat_id, ' +
            ' `brand_language`.`name` as brandName,' +
            " CONCAT('"+static.MAIN_URL+"','/product?id=',`product`.`slug`) as publicLink," +
            " CONCAT('"+static.API_URL+"','/images/brands/',`brand`.`brand_logo`) as brandLogo," +
            " CONCAT('"+static.API_URL+"','/images/product/',`product`.`image`) as image," +
            " CONCAT('"+static.API_URL+"','/images/product/',`product`.`hover_image`) as hoverImage, " +
            " CONCAT('"+static.API_URL+"','/images/product/',`product`.`cover_image`) as coverImage" +
            ' FROM `product` '+
            " JOIN `product_language` ON `product_language`.`product_id` = `product`.`id` AND `product_language`.`lang_id` = "+db.escape(langId)+" " +
            " JOIN `brand` ON `brand`.`id` = `product`.`brand_id` AND `brand`.`cat_id` = "+db.escape(catId)+"" +
            " LEFT JOIN `favorite` ON `favorite`.`product_id` = `product`.`id` AND `favorite`.`"+userType.field+"` = "+db.escape(userType.val)+" " +
            " JOIN `brand_language` ON `brand_language`.`brand_id` = `brand`.`id` AND `brand_language`.`lang_id` = "+db.escape(langId)+" " +
            " WHERE `product`.`id` <> "+db.escape(prodId)+" AND `product`.`brand_id` = `brand`.`id` LIMIT 5";

            // console.log('prepareSql',prepareSql)

            db.query(prepareSql, function (error, result, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"getRelativeProducts",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/product.txt")
                reject(new Error("Sql error getRelativeProducts: "+error.sqlMessage+""))
            }else{

                result.forEach(function (item) {
                    item.price = staticMethods.formatMoney(item.price);
                })
                resolve(result)
            }
        })
    })
}

var getAlternativeRelativeProducts = (langId,limit,ids,userType) => {
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            ' SELECT  `favorite`.`id` as favId,`product`.`id`,`product`.`image_top`,`product`.`in_stock`,`product`.`price`,`product`.`price_before_sale`,`product`.`sale_percent`,`product_language`.`name`,`product_language`.`description`,' +
            ' `product`.`slug` as slug,`brand`.`cat_id` as cat_id, ' +
            ' `brand_language`.`name` as brandName,' +
            " CONCAT('"+static.MAIN_URL+"','/product?id=',`product`.`slug`) as publicLink," +
            " CONCAT('"+static.API_URL+"','/images/brands/',`brand`.`brand_logo`) as brandLogo," +
            " CONCAT('"+static.API_URL+"','/images/product/',`product`.`image`) as image," +
            " CONCAT('"+static.API_URL+"','/images/product/',`product`.`hover_image`) as hoverImage, " +
            " CONCAT('"+static.API_URL+"','/images/product/',`product`.`cover_image`) as coverImage" +
            ' FROM `product` '+
            " JOIN `product_language` ON `product_language`.`product_id` = `product`.`id` AND `product_language`.`lang_id` = "+db.escape(langId)+" " +
            " JOIN `brand` ON `brand`.`id` = `product`.`brand_id`" +
            " LEFT JOIN `favorite` ON `favorite`.`product_id` = `product`.`id` AND `favorite`.`"+userType.field+"` = "+db.escape(userType.val)+" " +
            " JOIN `brand_language` ON `brand_language`.`brand_id` = `brand`.`id` AND `brand_language`.`lang_id` = "+db.escape(langId)+" " +
            " WHERE `product`.`id` NOT IN("+ids.join(',')+") ORDER BY RAND() LIMIT "+limit+" ";

            // console.log('prepareSql',prepareSql)

            db.query(prepareSql, function (error, result, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"getAlternativeRelativeProducts",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/product.txt")
                reject(new Error("Sql error getAlternativeRelativeProducts: "+error.sqlMessage+""))
            }else{

                result.forEach(function (item) {
                    item.price = staticMethods.formatMoney(item.price);
                })
                resolve(result)
            }
        })
    })
}

var getMultipleProducts = (page,langId,products) => {
    // console.log('page',page)
    // console.log('langId',langId)
    // console.log('products',products)
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            ' SELECT  `product`.`price`,`product`.`slug`,`product`.`price_before_sale`,`product`.`sale_percent`,`product`.`price`,`product_language`.`name`,`product_language`.`description`,' +
            ' `brand`.`slug` as brandSlug,`brand`.`slug` as brandSlug, ' +
            ' `brand_language`.`name` as brandName,' +
            " CONCAT('"+static.API_URL+"','/images/brands/',`brand`.`brand_logo`) as brandLogo," +
            " CONCAT('"+static.API_URL+"','/images/product/',`product`.`image`) as image," +
            " CONCAT('"+static.API_URL+"','/images/product/',`product`.`hover_image`) as hoverImage" +
            ' FROM `product` '+
            " JOIN `product_language` ON `product_language`.`product_id` = `product`.`id` AND `product_language`.`lang_id` = "+db.escape(langId)+" " +
            " JOIN `brand` ON `brand`.`id` = `product`.`brand_id` " +
            " JOIN `brand_language` ON `brand_language`.`brand_id` = `brand`.`id` AND `brand_language`.`lang_id` = "+db.escape(langId)+" " +
            " WHERE `product`.`id` IN( "+products.join(',')+")";
        // console.log('prepareSql',prepareSql)
            db.query(prepareSql, function (error, result, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"getMultipleProducts",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/product.txt")
                reject(new Error("Sql error getMultipleProducts: "+error.sqlMessage+""))
            }else{
                result.forEach(function (item) {
                    item.price = staticMethods.formatMoney(item.price);
                })
                resolve(result)
            }
        })
    })
}

var getOneProductFilters = (prodId,langId) => {
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            ' SELECT  `filter_prop_language`.`name` as filName,`filter_language`.`name` as valName' +
            ' FROM `filter_product` '+
            " JOIN `filter_prop_language` ON `filter_prop_language`.`filter_prop_id` = `filter_product`.`filter_prop_id` AND `filter_prop_language`.`lang_id` = "+db.escape(langId)+" " +
            " JOIN `filter_prop` ON `filter_prop`.`id` = `filter_prop_language`.`filter_prop_id` " +
            " JOIN `filter_language` ON `filter_language`.`filter_id` = `filter_prop`.`filter_id` AND `filter_language`.`lang_id` = "+db.escape(langId)+" " +
            " WHERE `filter_product`.`product_id` = "+db.escape(prodId)+"";
        // console.log('prepareSql',prepareSql)
            db.query(prepareSql, function (error, result, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"getOneProductFilters",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/product.txt")
                reject(new Error("Sql error getOneProductFilters: "+error.sqlMessage+""))
            }else{
                result.forEach(function (item) {
                    item.price = staticMethods.formatMoney(item.price);
                })
                resolve(result)
            }
        })
    })
}

var getSearch = (vals,userType) => {
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            " SELECT  `favorite`.`id` as favId,`product_language`.`name`,`product`.`price`,`product`.`in_stock`,`product`.`price_before_sale`,`product`.`slug`,`brand_language`.`name` as brandName," +
            " CONCAT('"+static.API_URL+"','/images/product/',`product`.`image`) as image, " +
            " CONCAT('"+static.API_URL+"','/images/product/',`product`.`hover_image`) as hoverImage " +
            " FROM `product_language` "+
            " JOIN `product` ON `product`.`id` = `product_language`.`product_id` " +
            " LEFT JOIN `favorite` ON `favorite`.`product_id` = `product`.`id` AND `favorite`.`"+userType.field+"` = "+db.escape(userType.val)+" " +
            " JOIN `brand_language` ON `brand_language`.`brand_id` = `product`.`brand_id` AND `brand_language`.`lang_id` = `product_language`.`lang_id` " +
            " WHERE `product_language`.`name` LIKE '%"+vals+"%' GROUP BY `product_language`.`product_id`";
            // console.log('prepareSql',prepareSql)
            db.query(prepareSql, function (error, result, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"getSearch",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/product.txt")
                reject(new Error("Sql error getSearch: "+error.sqlMessage+""))
            }else{
                resolve(result)
            }
        })
    })
}

var getOneProductAddFilters = (prodId,langId) => {
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            ' SELECT  `additional_filters_language`.`name` as valName,`additional_filters_language`.`val` as filName' +
            ' FROM `additional_filters_language` '+
            " WHERE `additional_filters_language`.`product_id` = "+db.escape(prodId)+" AND `additional_filters_language`.`lang_id` = "+db.escape(langId)+"";
        // console.log('prepareSql',prepareSql)
            db.query(prepareSql, function (error, result, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"getProductAdditionalFilters",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/product.txt")
                reject(new Error("Sql error getProductAdditionalFilters: "+error.sqlMessage+""))
            }else{
                resolve(result)
            }
        })
    })
}

var getPopularProduct = (langId,userType) => {

    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            ' SELECT  `favorite`.`id` as favId,`product`.`id`,`product`.`price`,`product`.`in_stock`,`product`.`price_before_sale`,`product`.`slug`,`product_language`.`name`,`product_language`.`description`, ' +
            " CONCAT('"+static.API_URL+"','/images/product/',`product`.`image`) as image," +
            " CONCAT('"+static.API_URL+"','/images/product/',`product`.`hover_image`) as hoverImage " +
            " FROM `popular_products` "+
            " JOIN `product` ON `product`.`id` = `popular_products`.`product_id` " +
            " LEFT JOIN `favorite` ON `favorite`.`product_id` = `product`.`id` AND `favorite`.`"+userType.field+"` = "+db.escape(userType.val)+" " +
            " JOIN `product_language` ON `product_language`.`product_id` = `product`.`id` AND `product_language`.`lang_id` = "+db.escape(langId);
        // console.log('prepareSql',prepareSql)
            db.query(prepareSql, function (error, result, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"getProductAdditionalFilters",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/product.txt")
                reject(new Error("Sql error getProductAdditionalFilters: "+error.sqlMessage+""))
            }else{
                result.forEach(function (item) {
                    if(item.price_before_sale !== null){
                        item.price_before_sale = staticMethods.formatMoney(item.price_before_sale);
                    }
                    item.price = staticMethods.formatMoney(item.price);
                })
                resolve(result)
            }
        })
    })
}

var getProductInShopSection = (langId) => {
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            ' SELECT  `product`.`id`,`product`.`price`,`product`.`in_stock`,`product`.`slug`,`product_language`.`name`, ' +
            " CONCAT('"+static.API_URL+"','/images/product/',`product`.`image`) as image " +
            " FROM `popular_products` "+
            " JOIN `product` ON `product`.`id` = `popular_products`.`product_id` AND `product`.`in_stock` = '0'" +
            " JOIN `product_language` ON `product_language`.`product_id` = `product`.`id` AND `product_language`.`lang_id` = "+db.escape(langId)+" " +
            " ORDER BY RAND() LIMIT 1 ";
            db.query(prepareSql, function (error, result, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"getProductInShopSection",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/product.txt")
                reject(new Error("Sql error getProductInShopSection: "+error.sqlMessage+""))
            }else{
                result.forEach(function (item) {
                    item.price = staticMethods.formatMoney(item.price);
                })
                resolve(result)
            }
        })
    })
}

var getSpecialOffers = (langId,userType) => {

    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            ' SELECT  `favorite`.`id` as favId,`product`.`id`,`product`.`price`,`product`.`in_stock`,`product`.`price_before_sale`,`product`.`slug`,`product_language`.`name`,`product_language`.`description`, ' +
            " CONCAT('"+static.API_URL+"','/images/product/',`product`.`image`) as image," +
            " CONCAT('"+static.API_URL+"','/images/product/',`product`.`hover_image`) as hoverImage " +
            " FROM `product` " +
            " JOIN `product_language` ON `product_language`.`product_id` = `product`.`id` AND `product_language`.`lang_id` = "+db.escape(langId)+""+
            " LEFT JOIN `favorite` ON `favorite`.`product_id` = `product`.`id` AND `favorite`.`"+userType.field+"` = "+db.escape(userType.val)+" " +
            " WHERE `product`.`price_before_sale` IS NOT NULL";
        // console.log('prepareSql',prepareSql)
            db.query(prepareSql, function (error, result, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"getSpecialOffers",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/product.txt")
                reject(new Error("Sql error getSpecialOffers: "+error.sqlMessage+""))
            }else{
                result.forEach(function (item) {
                    item.price = staticMethods.formatMoney(item.price);
                })
                resolve(result)
            }
        })
    })
}


var getProductForOrderEmail = (orderId) => {

    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            ' SELECT  `product`.`id`,`product`.`price`,`product_language`.`name`,`order_product`.`quantity`, ' +
            " CONCAT('"+static.API_URL+"','/images/product/',`product`.`image`) as image " +
            " FROM `order_product` " +
            " JOIN `product` ON `product`.`id` = `order_product`.`product_id` "+
            " JOIN `product_language` ON `product_language`.`product_id` = `product`.`id` AND `product_language`.`lang_id` = 1"+
            " WHERE `order_product`.`order_id` = "+db.escape(orderId)+" ";
            db.query(prepareSql, function (error, result, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"getSpecialOffers",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/product.txt")
                reject(new Error("Sql error getSpecialOffers: "+error.sqlMessage+""))
            }else{
                result.forEach(function (item) {
                    item.price = staticMethods.formatMoney(item.price);
                })
                resolve(result)
            }
        })
    })
}

var getProductsByIds = (langId,ids) => {

    return new Promise(function (resolve, reject) {
        var prepareSql = "" +
            ' SELECT  `product`.`id`, `product`.`image` as productImageName,`product`.`image_top`,`product`.`price`,`product`.`price_before_sale`,`product`.`in_stock`,`product`.`sale_percent`,`product`.`litrage_id`,' +
            '`product_language`.`name`,`product_language`.`description`,' +
            ' `brand`.`slug` as brandSlug,`brand`.`slug` as brandSlug, ' +
            ' `brand_language`.`name` as brandName,' +
            " CONCAT('"+static.MAIN_URL+"','/product?id=',`product`.`slug`) as publicLink," +
            " CONCAT('"+static.API_URL+"','/images/product/',`product`.`image`) as image" +
            ' FROM `product` '+
            " JOIN `product_language` ON `product_language`.`product_id` = `product`.`id` AND `product_language`.`lang_id` = "+db.escape(langId)+" " +
            " JOIN `brand` ON `brand`.`id` = `product`.`brand_id` " +
            " JOIN `brand_language` ON `brand_language`.`brand_id` = `brand`.`id` AND `brand_language`.`lang_id` = "+db.escape(langId)+" " +
            " WHERE `product`.`id` IN ("+ids.join(',')+") ";
        // console.log('prepareSql',prepareSql)
            db.query(prepareSql, function (error, result, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"getSpecialOffers",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/product.txt")
                reject(new Error("Sql error getSpecialOffers: "+error.sqlMessage+""))
            }else{
                result.forEach(function (item) {
                    item.price = staticMethods.formatMoney(item.price);
                    item.price_before_sale = item.price_before_sale !== null ? staticMethods.formatMoney(item.price_before_sale) : item.price_before_sale
                })
                resolve(result)
            }
        })
    })
}


var checkProductInStock = (id) => {

    return new Promise(function (resolve, reject) {
        var prepareSql = "" +
            ' SELECT  `product`.`id`,`product`.`in_stock` ' +
            ' FROM `product` '+
            " WHERE `product`.`id` = "+id+" ";
        // console.log('prepareSql',prepareSql)
            db.query(prepareSql, function (error, result, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"checkProductInStock",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/product.txt")
                reject(new Error("Sql error getSpecialOffers: "+error.sqlMessage+""))
            }else{
                resolve(result)
            }
        })
    })
}


module.exports.findMinMaxPrice = findMinMaxPrice;
module.exports.getAllProductCount = getAllProductCount;
module.exports.getBroductByBrandId = getBroductByBrandId;
module.exports.getOneProductFilters = getOneProductFilters;
module.exports.getOneProduct = getOneProduct;
module.exports.getOneProductAddFilters = getOneProductAddFilters;
module.exports.getPopularProduct = getPopularProduct;
module.exports.getMultipleProducts = getMultipleProducts;
module.exports.getSearch = getSearch;
module.exports.getSpecialOffers = getSpecialOffers;
module.exports.getProductForOrderEmail = getProductForOrderEmail;
module.exports.getProductsByIds = getProductsByIds;
module.exports.checkProductInStock = checkProductInStock;
module.exports.getProductInShopSection = getProductInShopSection;
module.exports.getRelativeProducts = getRelativeProducts;
module.exports.getAlternativeRelativeProducts = getAlternativeRelativeProducts;