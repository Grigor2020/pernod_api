const db = require('../../connection');
var log = require("../../../logs/log");
const static = require('../../../static');
const staticMethods = require('../../../model/staticMethods');

var getUserOrders = (count,user_id) => {
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            " SELECT  `orders`.`id` as order_id, `orders`.`public_id` as publicId,`orders`.`insert_data`,`orders`.`total_price`,`order_payment`.`id` as order_payment_id " +
            " FROM `orders` "+
            " JOIN `order_payment` ON `order_payment`.`order_id` = `orders`.`id`"+
            " WHERE `orders`.`user_id` = "+db.escape(user_id)+" LIMIT 0,"+db.escape(count)+""
        // console.log('prepareSql',prepareSql)
        db.query(prepareSql, function (error, result, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"getUserOrders",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/profile.txt")
                reject(new Error("Sql error getUserOrders: "+error.sqlMessage+""))
            }else{
                resolve(result)
            }
        })
    })
}

var getUserAllOrders = (user_id) => {
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            " SELECT  `orders`.`id` as order_id, `orders`.`public_id` as publicId,`orders`.`insert_data`,`orders`.`total_price`,`orders`.`order_status`,`orders`.`delivery_status`," +
            "`order_payment`.`status` as payment_status,`order_payment`.`id` as order_payment_id " +
            " FROM `order_payment` " +
            " JOIN `orders` ON `orders`.`id` = `order_payment`.`order_id` AND `orders`.`private` = 0"+
            " WHERE `orders`.`user_id` = "+db.escape(user_id)+" ORDER BY `order_payment`.`id` DESC";
        // console.log('prepareSql',prepareSql)
        db.query(prepareSql, function (error, result, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"getUserAllOrders",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/profile.txt")
                reject(new Error("Sql error getUserAllOrders: "+error.sqlMessage+""))
            }else{
                resolve(result)
            }
        })
    })
}
var getUserOrderById = (field,val,orderPaymentId) => {
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            " SELECT  `orders`.`id` as order_id,`orders`.`insert_data`,`orders`.`total_price`,`orders`.`comment`,`orders`.`delivery_date`,`orders`.`delivery_time`," +
            " `order_payment`.`status`, `order_payment`.`id` as order_payment_id,`order_payment`.`paymentDetailResponse`," +
            " `order_info`.`name`,`order_info`.`last_name`,`order_info`.`email`,`order_info`.`phone`,`order_info`.`address`, " +
            " `cities_lang`.`name` as city" +
            " FROM `order_payment` " +
            " JOIN `orders` ON `orders`.`id` = `order_payment`.`order_id` AND`orders`.`"+field+"` = "+db.escape(val)+"  AND `orders`.`private` = 0 " +
            " JOIN `order_info` ON `order_info`.`order_id` = `order_payment`.`order_id` " +
            " JOIN `cities_lang` ON `cities_lang`.`cities_id` = `order_info`.`cities_id` AND `cities_lang`.`lang_id` = 1" +
            " WHERE `order_payment`.`id` = "+db.escape(orderPaymentId)+"";
        // console.log('getUserOrderById prepareSql',prepareSql)
        db.query(prepareSql, function (error, result, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"getUserOrderById",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/profile.txt")
                reject(new Error("Sql error getUserOrderById: "+error.sqlMessage+""))
            }else{
                resolve(result)
            }
        })
    })
}

var getUserOrderByPublicId = (field,val,publicId) => {
    return new Promise(function (resolve, reject) {
        // console.log('publicId',publicId)
        var prepareSql = '' +
            " SELECT  `orders`.`id` as order_id,`orders`.`total_price`,`orders`.`user_id`,`orders`.`public_id` as public_id,`orders`.`payment_type`,`orders`.`insert_data`,`orders`.`total_price`,`orders`.`comment`,`orders`.`delivery_date`,`orders`.`delivery_time`," +
            " `order_payment`.`status`, `order_payment`.`id` as order_payment_id,`order_payment`.`paymentDetailResponse`," +
            " `order_info`.`name`,`order_info`.`last_name`,`order_info`.`email`,`order_info`.`phone`,`order_info`.`address`, " +
            " `cities_lang`.`name` as city" +
            " FROM `orders` " +
            " JOIN `order_payment` ON `order_payment`.`order_id` = `orders`.`id`  " +
            " JOIN `order_info` ON `order_info`.`order_id` = `order_payment`.`order_id` " +
            " JOIN `cities_lang` ON `cities_lang`.`cities_id` = `order_info`.`cities_id` AND `cities_lang`.`lang_id` = 1" +
            " WHERE `orders`.`public_id` = "+db.escape(publicId)+" AND `orders`.`"+field+"` = "+db.escape(val)+"  AND `orders`.`private` = 0";
        db.query(prepareSql, function (error, result, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"getUserOrderByPublicId",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/profile.txt")
                reject(new Error("Sql error getUserOrderByPublicId: "+error.sqlMessage+""))
            }else{
                resolve(result)
            }
        })
    })
}

var getOrderProducts = (orderId) => {
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            " SELECT  `order_product`.`product_id`,`order_product`.`quantity` " +
            " FROM `order_product` " +
            " WHERE `order_product`.`order_id` = "+db.escape(orderId)+"";
        // console.log('prepareSql',prepareSql)
        db.query(prepareSql, function (error, result, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"getOrderPaymentProducts",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/profile.txt")
                reject(new Error("Sql error getOrderProducts: "+error.sqlMessage+""))
            }else{
                resolve(result)
            }
        })
    })
}

var getAllLoyalityStatuses = (langId) => {
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            " SELECT  `loyality_status`.`from_money` as start_from,`loyality_status`.`id`,`loyality_status`.`color_1`,`loyality_status`.`color_2`,`loyality_status_language`.`name` " +
            " FROM `loyality_status` "+
            " JOIN `loyality_status_language` ON `loyality_status_language`.`lang_id` = '"+ langId +"'  AND `loyality_status_language`.`loyality_status_id` = `loyality_status`.`id` ORDER BY `loyality_status`.`from_money` ASC";
        // console.log('prepareSql',prepareSql)
        db.query(prepareSql, function (error, result, fields) {
            // console.log('error',error)
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"getAllLoyalityStatuses",
                        date:new Date()
                    }
                }
                log.insertLog(prepareSql,"./logs/v1/profile.txt")
                reject(new Error("Sql error getAllLoyalityStatuses: "+error.sqlMessage+""))
            }else{
                resolve(result)
            }
        })
    })
}
var getUserLoyality = (userId) => {
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            " SELECT  `user_loyality`.`loyality_status_id`,`user_loyality`.`money` as spent_money" +
            " FROM `user_loyality` " +
            " WHERE `user_loyality`.`user_id` = "+db.escape(userId)+"";
        // console.log('prepareSql',prepareSql)
        db.query(prepareSql, function (error, result, fields) {
            if (error){
                // console.log('error',error)
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"getUserLoyality",
                        date:new Date()
                    }
                }
                log.insertLog(prepareSql,"./logs/v1/profile.txt")
                reject(new Error("Sql error getUserLoyality: "+error.sqlMessage+""))
            }else{
                resolve(result)
            }
        })
    })
}


var getUserAddresses = (user_id) => {
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            " SELECT  `user_address`.`text` as address,`user_address`.`def` as default_address,`user_address`.`id` as address_id," +
            " `user_address`.`name` as name,`user_address`.`last_name` as last_name, `user_address`.`phone` as phone, `user_address`.`email` as email," +
            "  `cities_lang`.`name` as city,`cities_lang`.`cities_id` as city_id," +
            "  `states_lang`.`name` as state,`states_lang`.`states_id` as state_id," +
            "  `countries_lang`.`name` as country,`countries_lang`.`countries_id` as countrie_id" +
            " FROM `user_address` " +
            " JOIN `cities` ON `cities`.`id` = `user_address`.`city_id`  " +
            " JOIN `cities_lang` ON `cities_lang`.`cities_id` = `cities`.`id` AND `cities_lang`.`lang_id` = '1' " +
            " JOIN `states` ON `states`.`id` = `cities`.`state_id`  " +
            " JOIN `states_lang` ON `states_lang`.`states_id` = `states`.`id` AND `states_lang`.`lang_id` = '1' " +
            " JOIN `countries` ON `countries`.`id` = `states`.`country_id`  " +
            " JOIN `countries_lang` ON `countries_lang`.`countries_id` = `countries`.`id` AND `countries_lang`.`lang_id` = '1' " +
            " WHERE `user_address`.`user_id` = "+db.escape(user_id)+" ";
        // console.log('prepareSql',prepareSql)
        db.query(prepareSql, function (error, result, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"getUserAddresses",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/profile.txt")
                reject(new Error("Sql error getUserAddresses: "+error.sqlMessage+""))
            }else{
                resolve(result)
            }
        })
    })
}
var getUserAddressesLimit = (limit,user_id) => {
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            " SELECT  `user_address`.`text` as address,`user_address`.`def` as default_address,`user_address`.`id` as address_id," +
            " `user_address`.`name` as name, `user_address`.`phone` as phone, `user_address`.`email` as email," +
            "  `cities_lang`.`name` as city,`cities_lang`.`cities_id` as city_id," +
            "  `states_lang`.`name` as state,`states_lang`.`states_id` as state_id," +
            "  `countries_lang`.`name` as country,`countries_lang`.`countries_id` as countrie_id" +
            " FROM `user_address` " +
            " JOIN `cities` ON `cities`.`id` = `user_address`.`city_id`  " +
            " JOIN `cities_lang` ON `cities_lang`.`cities_id` = `cities`.`id` AND `cities_lang`.`lang_id` = '1' " +
            " JOIN `states` ON `states`.`id` = `cities`.`state_id`  " +
            " JOIN `states_lang` ON `states_lang`.`states_id` = `states`.`id` AND `states_lang`.`lang_id` = '1' " +
            " JOIN `countries` ON `countries`.`id` = `states`.`country_id`  " +
            " JOIN `countries_lang` ON `countries_lang`.`countries_id` = `countries`.`id` AND `countries_lang`.`lang_id` = '1' " +
            " WHERE `user_address`.`user_id` = "+db.escape(user_id)+"  LIMIT 0,"+db.escape(limit)+"";
        // console.log('prepareSql',prepareSql)
        db.query(prepareSql, function (error, result, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"getUserAddresses",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/profile.txt")
                reject(new Error("Sql error getUserAddresses: "+error.sqlMessage+""))
            }else{
                resolve(result)
            }
        })
    })
}

var getUserAddressById = (address_id) => {
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            " SELECT  `user_address`.`text` as address,`user_address`.`def` as default_address,`user_address`.`id` as address_id," +
            " `user_address`.`name` as name,`user_address`.`last_name` as last_name, `user_address`.`phone` as phone, `user_address`.`email` as email," +
            "  `cities_lang`.`name` as city,`cities_lang`.`cities_id` as city_id," +
            "  `states_lang`.`name` as state,`states_lang`.`states_id` as state_id," +
            "  `countries_lang`.`name` as country,`countries_lang`.`countries_id` as countrie_id" +
            " FROM `user_address` " +
            " JOIN `cities` ON `cities`.`id` = `user_address`.`city_id`  " +
            " JOIN `cities_lang` ON `cities_lang`.`cities_id` = `cities`.`id` AND `cities_lang`.`lang_id` = '1' " +
            " JOIN `states` ON `states`.`id` = `cities`.`state_id`  " +
            " JOIN `states_lang` ON `states_lang`.`states_id` = `states`.`id` AND `states_lang`.`lang_id` = '1' " +
            " JOIN `countries` ON `countries`.`id` = `states`.`country_id`  " +
            " JOIN `countries_lang` ON `countries_lang`.`countries_id` = `countries`.`id` AND `countries_lang`.`lang_id` = '1' " +
            " WHERE  `user_address`.`id` = "+db.escape(address_id)+"";
        // console.log('prepareSql',prepareSql)
        db.query(prepareSql, function (error, result, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"getUserAddressById",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/profile.txt")
                reject(new Error("Sql error getUserAddressById: "+error.sqlMessage+""))
            }else{
                resolve(result)
            }
        })
    })
}



module.exports.getUserOrders = getUserOrders;
module.exports.getAllLoyalityStatuses = getAllLoyalityStatuses;
module.exports.getUserLoyality = getUserLoyality;
module.exports.getUserAddresses = getUserAddresses;
module.exports.getUserAllOrders = getUserAllOrders;
module.exports.getUserAddressById = getUserAddressById;
module.exports.getUserOrderById = getUserOrderById;
module.exports.getUserOrderByPublicId = getUserOrderByPublicId;
module.exports.getOrderProducts = getOrderProducts;
module.exports.getUserAddressesLimit = getUserAddressesLimit;