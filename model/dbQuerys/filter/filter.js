const db = require('../../connection');
var log = require("../../../logs/log");
const static = require('../../../static');

var getAllFilters = function(langId){

    return new Promise(function (resolve, reject) {
        var prepareSql = "" +
            " SELECT `filter_language`.`name` as filterName, " +
            " GROUP_CONCAT(DISTINCT `filter_prop`.`id` SEPARATOR '||') AS filterPropId, " +
            " GROUP_CONCAT(DISTINCT `filter_prop_language`.`name` SEPARATOR '||') AS filterPropName " +
            " FROM `filter`" +
            " JOIN `filter_language` ON `filter_language`.`filter_id` = `filter`.`id` AND `filter_language`.`lang_id` = "+db.escape(langId)+""+
            " JOIN `filter_prop` ON `filter_prop`.`filter_id` = `filter`.`id` "+
            " JOIN `filter_prop_language` ON `filter_prop_language`.`filter_prop_id` = `filter_prop`.`id` AND `filter_prop_language`.`lang_id` = "+db.escape(langId)+"" +
            " GROUP BY `filter`.`id`";
        // console.log('sss/////prepareSql',prepareSql)
        db.query(prepareSql, function (error, result) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"getAllFilters",
                        table:'filter_prop_language',
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/filter.txt")
                reject(new Error("Sql error getAllFilters"))
            }else{
                resolve(result)
            }
        })
    })
}

var getAllFiltersByProdIds = function(selectedProducts,langId){
    return new Promise(function (resolve, reject) {
        // console.log('selectedProducts',selectedProducts)
        var prepareSql = "" +
            " SELECT `product`.`id`, " +
            " GROUP_CONCAT(DISTINCT `filter_product`.`filter_prop_id` SEPARATOR '||') AS filterPropId, " +
            " GROUP_CONCAT(DISTINCT `brand`.`slug` SEPARATOR '||') AS brandSlug " +
            " FROM `product` " +
            " JOIN `filter_product` ON `filter_product`.`product_id` = `product`.`id` "+
            " JOIN `brand` ON `brand`.`id` = `product`.`brand_id` "+
            " WHERE `product`.`id` IN ("+selectedProducts.join(',')+") ";
        // console.log(' getAllFiltersByProdIds prepareSql',prepareSql)
        db.query(prepareSql, function (error, result) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"getAllFiltersByProdIds",
                        table:'filter_prop_language',
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/filter.txt")
                reject(new Error("Sql error getAllFiltersByProdIds"))
            }else{
                resolve(result)
            }
        })
    })
}
var getGrouped = function(filters){
    return new Promise(function (resolve, reject) {
        // console.log('selectedProducts',selectedProducts)
        var prepareSql = "SELECT GROUP_CONCAT(DISTINCT `filter_prop`.`id` SEPARATOR '||') as fGroup"+
        " FROM `filter_prop`"+
        " WHERE `filter_prop`.`id` IN ("+filters.join(',')+")"+
        " GROUP BY `filter_prop`.`filter_id`";
        // console.log(' getAllFiltersByProdIds prepareSql',prepareSql)
        db.query(prepareSql, function (error, result) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"getGrouped",
                        table:'filter_prop',
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/filter.txt")
                reject(new Error("Sql error getGrouped"))
            }else{
                resolve(result)
            }
        })
    })
}
var getAllFiltersByPropIds = function(avFilterProps,langId){
    return new Promise(function (resolve, reject) {
        // var prepareSql = "" +
        //     " SELECT  `filter_prop_language`.`name` as propName,`filter_language`.`name` as filterName" +
        //     " FROM `filter_prop`" +
        //     " JOIN `filter_prop_language` ON `filter_prop_language`.`filter_prop_id` = `filter_prop`.`id` AND `filter_prop_language`.`lang_id` = "+db.escape(langId)+""+
        //     " JOIN `filter_language` ON `filter_language`.`filter_id` = `filter_prop`.`filter_id`  AND  `filter_language`.`lang_id` = "+db.escape(langId)+""+
        //     " WHERE `filter_prop`.`id` IN ("+avFilterProps.join(',')+") ";
        var prepareSql = "" +
            " SELECT `filter_language`.`name` as filterName, " +
            " GROUP_CONCAT(DISTINCT `filter_prop`.`id` SEPARATOR '||') AS filterPropId, " +
            " GROUP_CONCAT(DISTINCT `filter_prop_language`.`name` SEPARATOR '||') AS filterPropName " +
            " FROM `filter`" +
            " JOIN `filter_language` ON `filter_language`.`filter_id` = `filter`.`id` AND `filter_language`.`lang_id` = "+db.escape(langId)+""+
            " JOIN `filter_prop` ON `filter_prop`.`filter_id` = `filter`.`id` AND  `filter_prop`.`id` IN ("+avFilterProps.join(',')+")  "+
            " JOIN `filter_prop_language` ON `filter_prop_language`.`filter_prop_id` = `filter_prop`.`id` AND `filter_prop_language`.`lang_id` = "+db.escape(langId)+"" +
            " GROUP BY `filter`.`id`";
        // console.log(' getAllFiltersByPropIds prepareSql',prepareSql)
        db.query(prepareSql, function (error, result) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"getAllFiltersByPropIds",
                        table:'filter_prop',
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/filter.txt")
                reject(new Error("Sql error getAllFiltersByPropIds"))
            }else{
                resolve(result)
            }
        })
    })
}


module.exports.getAllFilters = getAllFilters;
module.exports.getAllFiltersByProdIds = getAllFiltersByProdIds;
module.exports.getAllFiltersByPropIds = getAllFiltersByPropIds;
module.exports.getGrouped = getGrouped;