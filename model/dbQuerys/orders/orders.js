const db = require('../../connection');
var log = require("../../../logs/log");
const static = require('../../../static');
const staticMethods = require('../../../model/staticMethods');

var getAllProductCount = function(){
    return new Promise(function (resolve, reject) {
        var prepareSql = "" +
            " SELECT  COUNT(`product`.`id`) as productRow " +
            " FROM `product`";
        db.query(prepareSql, function (error, result) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"getAllProductCount",
                        table:'product',
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/product.txt")
                reject(new Error("Sql error getAllFilters"))
            }else{
                resolve(result[0].productRow)
            }
        })
    })
}

var getOrderByPublicId = function(public_id,defineQueryUserType){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            ' SELECT  `orders`.`insert_data`,`orders`.`id` as order_id,`product`.`id`,`product`.`price`,`product_language`.`name`,`order_product`.`quantity`, ' +
            " CONCAT('"+static.API_URL+"','/images/product/',`product`.`image`) as image " +
            " FROM `orders` " +
            " JOIN `order_product` ON `order_product`.`order_id` = `orders`.`id` "+
            " JOIN `product` ON `product`.`id` = `order_product`.`product_id` "+
            " JOIN `product_language` ON `product_language`.`product_id` = `product`.`id` AND `product_language`.`lang_id` = 1"+
            " WHERE `orders`.`public_id` = "+db.escape(public_id)+" AND `orders`.`"+defineQueryUserType.field+"` = "+db.escape(defineQueryUserType.val)+"";
        // console.log('getOrderByPublicId prepareSql',prepareSql)
        db.query(prepareSql, function (error, result) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"getAllProductCount",
                        table:'product',
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/product.txt")
                reject(new Error("Sql error getOrderByPublicId"))
            }else{
                resolve(result)
            }
        })
    })
}

var getOrderInfoByPublicId = function(public_id,defineQueryUserType){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            ' SELECT  `orders`.`id` as order_id,`orders`.`total_price`,`orders`.`comment`,`orders`.`delivery_date`,' +
            ' `orders`.`delivery_time`,`orders`.`insert_data`,`orders`.`payment_type`,' +
            ' `order_info`.`name`,`order_info`.`last_name`,`order_info`.`email`,`order_info`.`phone`,' +
            ' `order_info`.`address`,`cities_lang`.`name` as city ' +
            " FROM `orders` " +
            " JOIN `order_info` ON `order_info`.`order_id` = `orders`.`id` "+
            " JOIN `cities_lang` ON `cities_lang`.`cities_id` = `order_info`.`cities_id` AND `cities_lang`.`lang_id` = 1 " +
            " WHERE `orders`.`public_id` = "+db.escape(public_id)+" AND `orders`.`"+defineQueryUserType.field+"` = "+db.escape(defineQueryUserType.val)+"";
        // console.log('getOrderInfoByPublicId prepareSql',prepareSql)
        db.query(prepareSql, function (error, result) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"getAllProductCount",
                        table:'product',
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v1/product.txt")
                reject(new Error("Sql error getOrderInfoByPublicId"))
            }else{
                resolve(result)
            }
        })
    })
}

module.exports.getAllProductCount = getAllProductCount;
module.exports.getOrderByPublicId = getOrderByPublicId;
module.exports.getOrderInfoByPublicId = getOrderInfoByPublicId;